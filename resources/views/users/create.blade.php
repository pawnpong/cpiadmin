@extends('layouts.master')

@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">                  
        <div class="main-body">
            <div class="page-wrapper">
                <!-- Page-header start -->
                <div class="page-header">
                    <div class="page-header-title">
                        <h4>เพิ่มข่าวประชาสัมพันธ์</h4>
                        <span></span>
                    </div>
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{url('/')}}">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{route('admin.newstopic.index')}}">ข่าวประชาสัมพันธ์</a>
                            <li class="breadcrumb-item"><a href="#">เพิ่มข่าวประชาสัมพันธ์</a>  
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5>เพิ่ม</h5>
                    </div>
                    <div class="card-block">
                        {!! Form::open(array('route' => 'admin.users.store' , 'method' => 'post')) !!}
                        
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">ชื่อ</label>
                            <div class="col-sm-10">
                                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'ชื่อ']);!!}
                        
                                @if ($errors->has('name'))
                                <span class="messages"><p class="text-danger error">{{ $errors->first('name')}}</p></span>
                                @endif
                            </div>  
                        </div>

                        <div class="form-group row">
                            <label for="exampleTextarea" class="col-sm-2 col-form-label">E-Mail Address</label>
                            <div class="col-10">
                                {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'กรุณากรอก E-Mail Address']);!!}
                                @if ($errors->has('email'))
                                <span class="messages"><p class="text-danger error">{{ $errors->first('email')}}</p></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="exampleTextarea" class="col-sm-2 col-form-label">Password</label>
                            <div class="col-10">
                                <input type="password" name="password" class="form-control" placeholder="กรุณากรอก Password">
                                {{-- {!! Form::text('password',null,['class'=>'form-control','placeholder'=>'กรุณากรอก Password']);!!} --}}
                                @if ($errors->has('password'))
                                <span class="messages"><p class="text-danger error">{{ $errors->first('password')}}</p></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="exampleTextarea" class="col-sm-2 col-form-label">Confirm Password</label>
                            <div class="col-10">
                                    <input type="password" name="password_confirmation" class="form-control" placeholder="กรุณากรอก Confirm Password">
                                {{-- {!! Form::text('password_confirmation',null,['class'=>'form-control','placeholder'=>'กรุณากรอก Confirm Password']);!!} --}}
                                @if ($errors->has('password_confirmation'))
                                <span class="messages"><p class="text-danger error">{{ $errors->first('password_confirmation')}}</p></span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-sm-2"></label>
                            <div class="col-sm-10 edit-right ">
                                <button data-key="submit" type="submit" class="btn btn-primary" > <i class="ion-archive"></i>บันทึก</button>
                                <a href="{{ route('admin.users.index') }}" class="btn btn-default">
                                    <i class="ion-reply"></i>ยกเลิก
                                </a>
                            </div>
                        </div> 
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('cssheader')
<!-- -->
@endsection

@section('jsfooter')
    <script>
        //    $(document).ready(function() {
        //     document.getElementById('main_news').classList.add('active');
        //     document.getElementById('main_news').classList.add('pcoded-trigger');
        //     document.getElementById('news_active').classList.add('active');
    });
        //image headline Show
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imgInp").change(function() {readURL(this);});
    </script>
@endsection
