@extends('layouts.master') 
@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <!-- Main-body start -->
        <div class="main-body">
            <div class="page-wrapper">
                <!-- Page-header start -->
                <div class="page-header">
                    <div class="page-header-title">
                        <h4>หน้าหลัก</h4>
                        <span> ระบบจัดการข้อมูลเว็ปไซต์ CPI CHANNEL </span>
                    </div>
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{url('/admin')}}">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="page-body">
                    <div class="row">
                        <!-- Documents card start <-->
                        <div class="col-md-6 col-xl-6">
                            <div class="card client-blocks">
                                <div class="card-block">
                                    <a href="{{route('admin.categorynews.index')}}">
                                        <h5>ระบบจัดการหมวดหมู่ประชาสัมพันธ์</h5>
                                        <ul>
                                            <li>
                                                <i class="icofont icofont-news"></i>
                                            </li>
                                            <li class="text-right success-border">
                                                {{$categorynews_count}}
                                            </li>
                                        </ul>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-6">
                            <div class="card client-blocks">
                                <div class="card-block">
                                    <a href="{{route('admin.newstopic.index')}}">
                                        <h5>ระบบจัดการข่าวประชาสัมพันธ์</h5>
                                        <ul>
                                            <li>
                                                <i class="icofont icofont-news"></i>
                                            </li>
                                            <li class="text-right success-border">
                                                {{$news_count}}
                                            </li>
                                        </ul>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-6">
                            <div class="card client-blocks success-border">
                                <div class="card-block">
                                    <a href="{{route('admin.categoryinfographics.index')}}">
                                        <h5>ระบบจัดการหมวดหมู่กิจกรรม</h5>
                                        <ul>
                                            <li>
                                                <i class="icofont icofont-social-photobucket text-success"></i>
                                            </li>
                                            <li class="text-right text-success">
                                                {{$categoryinfographic_count}}
                                            </li>
                                        </ul>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-6">
                            <div class="card client-blocks success-border">
                                <div class="card-block">
                                    <a href="{{route('admin.infographics.index')}}">
                                        <h5>ระบบจัดการกิจกรรม</h5>
                                        <ul>
                                            <li>
                                                <i class="icofont icofont-social-photobucket text-success"></i>
                                            </li>

                                            <li class="text-right text-success">
                                                {{$infographic_count}}
                                            </li>
                                        </ul>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-6">
                            <div class="card client-blocks warning-border">
                                <div class="card-block">
                                    <a href="{{route('admin.category.index')}}">
                                        <h5>ระบบจัดการหมวดหมู่วีดีโอ</h5>
                                        <ul>
                                            <li>
                                                <i class="icofont icofont-brand-youtube text-warning"></i>
                                            </li>
                                            <li class="text-right text-warning">
                                                {{$category_count}}
                                            </li>
                                        </ul>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-6">
                            <div class="card client-blocks warning-border">
                                <div class="card-block">
                                    <a href="{{route('admin.youtube.index')}}">
                                        <h5>ระบบจัดการวีดีโอ</h5>
                                        <ul>
                                            <li>
                                                <i class="icofont icofont-brand-youtube text-warning"></i>
                                            </li>
                                            <li class="text-right text-warning">
                                                {{$youtube_count}}
                                            </li>
                                        </ul> 
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- New files card end -->
                        <!-- Open Project card start -->
                    </div>
                </div>
                <!-- Round card end -->
            </div>
        </div>
        <!-- Main-body end -->
        <div id="styleSelector">
        </div>
    </div>
</div>
@endsection
 @section('jsfooter')
<script type="text/javascript" src="assets/js/animation.js"></script> 
@endsection