@extends('layouts.frontend') 
    @section('content')
<!--breadcrumbs-->
<section id="breadcrumb">
    <div class="row">
        <div class="large-12 columns">
            <nav aria-label="You are here:" role="navigation">
                <ul class="breadcrumbs">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="{{url('/')}}">หน้าหลัก</a>
                    </li>
                    <li>
                        <span class="show-for-sr">Current: </span>ข่าวประชาสพัมพันธ์
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</section>
<!--end breadcrumbs-->
<br>

<section class="category-content">
    <div class="row">
        <!-- left side content area -->
        <div class="large-8 columns">
            <section class="content content-with-sidebar">
                <!-- newest video -->
                <div class="main-heading removeMargin">
                    <div class="row secBg padding-14 removeBorderBottom">
                        <div class="medium-8 small-8 columns">
                            <div class="head-title">
                                <i class="fa fa-newspaper-o"></i>
                                @if($id_category == 0) 
                                {{$category_type}}
                                @else
                                {{$category_type->name}} 
                                @endif
                            </div>
                        </div>
                        <div class="medium-4 small-4 columns">
                            <ul class="tabs text-right pull-right" data-tabs id="newVideos">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row secBg">
                    <div class="large-12 columns">
                        <div class="row column head-text clearfix">
                            <p class="pull-left">@if($id_category == 0) All News :
                                <span> {{$news->count()}} News posted</span>
                            </p>
                            @else All News :
                            <span> {{$category->count()}} News posted</span>
                            </p>
                            @endif </span>
                            </p>
                            <div class="grid-system pull-right show-for-large">
                                <a class="secondary-button current grid-medium" href="#">
                                    <i class="fa fa-th-large"></i>
                                </a>
                            </div>
                        </div>
                        <div class="tabs-content" data-tabs-content="newVideos">
                            <div class="tabs-panel is-active" id="new-all">
                                <div class="row list-group">
                                    @if($id_category == 0) 
                                    @foreach($news as $item)
                                    <div class="item large-4 end columns grid-medium">
                                        <div class="post thumb-border">
                                            <div class="post-thumb">
                                                <img src="{{asset('news/'.$item->headlines)}}" alt="{{$item->title}}">
                                                <a href="#" onclick="updateview({{$item->id}});" class="hover-posts">
                                                    <span>
                                                    <i class="fa fa-play"></i>ดูรายละเอียด</span>
                                                </a>
                                            </div>
                                            <div class="post-des">
                                                <h6>{{ str_limit($item->title, 50,'...') }}</h6>
                                                <div class="post-stats clearfix">
                                                    <p class="pull-left">
                                                        <i class="fa fa-clock-o"></i>
                                                        <small>{{DateTime::createFromFormat('Y-m-d H:i:s',$item->created_at)->format('d-m-Y')}}</small>
                                                    </p>
                                                    <p class="pull-left">
                                                        <i class="fa fa-eye"></i>
                                                        <small>{{$item->view}}</small>
                                                    </p>
                                                </div>
                                                <div class="post-button">
                                                    <a href="single-video-v2.html" class="secondary-button">
                                                        <i class="fa fa-play-circle"></i>watch video</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach 
                                    @elseif($category->count() == 0)
                                    @foreach(range(1, 2) as $i)
                                    <div class="item large-4 columns end grid-medium">
                                        <figure class="premium-img">
                                            <img src="http://placehold.it/370x220" alt="new video">
                                            <figcaption>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    @endforeach 
                                    @else
                                    @foreach($category as $item)
                                    <div class="item large-4 end columns grid-medium">
                                        <div class="post thumb-border">
                                            <div class="post-thumb">
                                                <img src="{{asset('news/'.$item->headlines)}}" alt="{{$item->title}}">
                                                <a href="#" onclick="updateview({{$item->id}});" class="hover-posts">
                                                    <span> <i class="fa fa-play"></i>ดูรายละเอียด</span>
                                                </a>
                                            </div>
                                            <div class="post-des">
                                                <h6>{{ str_limit($item->title, 50,'...') }}</h6>
                                                <div class="post-stats clearfix">
                                                    <p class="pull-left">
                                                        <i class="fa fa-clock-o"></i>
                                                        <small>{{DateTime::createFromFormat('Y-m-d H:i:s',$item->created_at)->format('d-m-Y')}}</small>
                                                    </p>
                                                    <p class="pull-left">
                                                        <i class="fa fa-eye"></i>
                                                        <small>{{$item->view}}</small>
                                                    </p>
                                                </div>
                                                <div class="post-summary">
                                                </div>
                                                <div class="post-button">
                                                    <a href="single-video-v2.html" class="secondary-button">
                                                        <i class="fa fa-play-circle"></i>ดูรายละเอียด</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach @endif
                                </div>
                            </div>
                        </div>
                        @include('pagination.default', ['paginator' => $news])
                    </div>
            </section>
            </div>
            <!-- end left side content area -->
            <!-- sidebar -->
            <div class="large-4 columns">
                <aside class="secBg sidebar">
                    <div class="row">
                        <div class="large-12 medium-7 medium-centered columns">
                            <div class="widgetBox">
                                <div class="widgetTitle">
                                    <h5>Category</h5>
                                </div>
                                <div class="widgetContent">
                                    <ul class="accordion" data-accordion>
                                        <li class="accordion-item is-active" data-accordion-item>
                                            <a href="#" class="accordion-title">หมวดหมู่ข่าวประชาสัมพันธ์</a>
                                            <div class="accordion-content" data-tab-content>
                                                <ul>
                                                    @php $news_count = \App\News::where('is_public', 1)->count(); @endphp
                                                    <li
                                                        class="clearfix">
                                                        <i class="fa fa-newspaper-o"></i>
                                                        <a href="{{ route('News',['id' => 0 ]) }}">ทั้งหมด
                                                            <span>({{$news_count}})</span>
                                                        </a>
                                                    </li>
                                                    @foreach($category_select as $item) @php $news_count = \App\News::where('categorynews_id','=',$item->id)->where('is_public',
                                                    1)->count(); @endphp
                                                    <li class="clearfix">
                                                        <i class="fa fa-newspaper-o"></i>
                                                        <a href="{{ route('News',['id' => $item->id ]) }}">{{$item->name}}
                                                            <span>({{$news_count}})</span>
                                                        </a>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- social Fans Widget -->
                        @include('layouts.social')
                    </div>
                </aside>
            </div>
            <!-- end sidebar -->
        </div>
</section>
<!-- End Category Content-->
<!-- footer -->


</div>
<!--end off canvas content-->
</div>
<!--end off canvas wrapper inner-->
</div>
<!--end off canvas wrapper-->
@endsection
<!-- script files -->

@section('footer')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> {{-- @section('footer') --}}
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
    function updateview($id) {
        //alert($id);
        var token = "<?php echo csrf_token(); ?>";
        $.ajax({
            url: '{{ route("ajaxupdateviewnews") }}',
            datatType: 'json',
            type: 'POST',
            data: {
                '_token': token,
                'id': $id,
            },
            success: function (response) {
                //alert(response.id);
                //redirect to Playyoutube
                window.location = response.url

            }
        });//end ajax
    }
</script> 
@endsection