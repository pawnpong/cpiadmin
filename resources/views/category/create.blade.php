@extends('layouts.master')

@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header">
                        <div class="page-header-title">
                            <h4>เพิ่มหมวดหมู่วีดีโอ</h4>
                            <span></span>
                        </div>
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="{{url('/')}}">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('admin.category.index')}}">หมวดหมู่วีดีโอ</a>
                                <li class="breadcrumb-item"><a href="#">เพิ่มหมวดหมู่วีดีโอ</a>
                            </ul>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h5>เพิ่มข้อมูล</h5>
                        </div>
                        <div class="card-block">
                            {!! Form::open(array('route' => 'admin.category.store' , 'method' => 'post' , 'files' => true )) !!}
                                {{ csrf_field() }}

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">ชื่อ</label>
                                    <div class="col-sm-10">
                                        <?= Form::text('name',null,['class'=>'form-control','placeholder'=>'ชื่อ']);?>
                                        <span class="messages"></span>
                                        @if ($errors->has('name'))
                                        <span class="messages"><p class="text-danger error">{{ $errors->first('name')}}</p></span>
                                        @endif
                                    </div>
                                </div>

                                {{-- <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">รูปภาพปก</label>
                                    <div class="col-sm-10">
                                            <div class="col-lg-3 col-md-6 col-sm-12">
                                                <a onclick="$('#imgInp').click();">      
                                                <img id="blah" style="cursor:pointer;" src="{{asset('images/nopic.png')}}" alt="รูปภาพปก"  class="img-fluid" /></a>
                                            </div>
                                        <div style="margin-button:20px"></div><br>
                                        <input type="file" id="imgInp" name="image" style="display:none;cursor:pointer;" accept="image/jpg, image/jpeg, image/png" >   
                                        <span class="messages"></span>
                                        @if ($errors->has('image'))
                                        <span class="messages"><p class="text-danger error">{{ $errors->first('image')}}</p></span>
                                        @endif         
                                    </div>        
                                </div> --}}

                                <div class="form-group row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button data-key="submit" type="submit" class="btn btn-primary" > <i class="ion-archive"></i>บันทึก</button>
                                        <a href="{{ route('admin.category.index') }}" class="btn btn-default">
                                            <i class="ion-reply"></i>ยกเลิก
                                        </a>
                                    </div>
                                </div>
                            {!! Form::close() !!}    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('jsfooter')
<script>
     $(document).ready(function() {
            document.getElementById('main_youtube').classList.add('active');
            document.getElementById('main_youtube').classList.add('pcoded-trigger');
            document.getElementById('category_active').classList.add('active');
    });
    //image headline Show
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#blah').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
        $("#imgInp").change(function() {readURL(this);});
</script>
@endsection