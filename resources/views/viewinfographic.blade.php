 @extends('layouts.frontend')

     @section('content')
<!--breadcrumbs-->
<section id="breadcrumb" class="breadcrumb-video-2">
    <div class="row">
        <div class="large-12 columns">
            <nav aria-label="You are here:" role="navigation">
                <ul class="breadcrumbs">
                    <li><i class="fa fa-home"></i><a href="{{url('/')}}">หน้าหลัก</a></li>
                    <li><a href="">กิจกรรม</a></li>
                </ul>
            </nav>
        </div>
    </div>
</section>
<!--end breadcrumbs-->
<div class="row">
    <!-- left side content area -->
    <div class="large-8 columns">
        <section class="content content-with-sidebar">
            <div class="row secBg">
                <div class="large-12 columns">
                    <div class="main-heading borderBottom">
                        <div class="row padding-14">
                            <div class="medium-12 small-12 columns">
                                <div class="head-title">
                                    @if($id_category == 0) 
                                    <h4><i class="fa fa-film"></i>{{$category_type}} </h4> 
                                    @else 
                                    <h4><i class="fa fa-film"></i>{{$category_type->name}}</h4> 
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row list-group">
                        @if($id_category == 0)
                        @foreach($Infographic as $item)
                        <div class="item large-4 columns end group-item-grid-default">
                            <div class="post thumb-border">
                                <div class="post-thumb">
                                    <img src="{{asset('fileinfographic/'.$item->filename)}}" alt="carousel">
                                    <a href="{{asset('fileinfographic/'.$item->filename)}}" onclick="updateview({{$item->id}});" class="hover-posts" data-lightbox="lightboxOverlay" data-title="{{$item->department}}">
                                        <span><i class="">{{$item->name}}</i></span>
                                    </a>
                                </div>
                                <div class="post-des">
                                    <h6>
                                        <a href="#">{{$item->department}}</a></h6>

                                    <div class="post-stats clearfix">
                                        <p class="pull-left">
                                            <i class="fa fa-clock-o"></i>
                                            <small>{{DateTime::createFromFormat('Y-m-d H:i:s',$item->created_at)->format('d-m-Y')}}</small>
                                        </p>
                                        <p class="pull-left">
                                            <i class="fa fa-eye"></i>
                                            <small>{{$item->view}}</small>
                                        </p>
                                       
                                    </div>
                                    <div>  
                                            <p>    
                                                <div class="a2a_kit a2a_kit_size_28 a2a_default_style" data-a2a-url="{{ URL::current() }}">
                                                        <a class="a2a_button_facebook"></a>
                                                        <a class="a2a_button_twitter"></a>
                                                        <a class="a2a_button_google_plus"></a>
                                                        <a class="a2a_button_line"></a>
                                                </div>
                                                <script async src="https://static.addtoany.com/menu/page.js"></script>
                                            </p>
                                        </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @elseif($category->count() == 0) 
                        @foreach(range(1, 2) as $i)
                        <div class="item large-4 columns end grid-medium">
                            <figure class="premium-img">
                                <img src="http://placehold.it/370x220" alt="new video">
                                <figcaption>
                                </figcaption>
                            </figure>
                        </div>
                        @endforeach
                        @else
                        @foreach($category as $item)
                        <div class="item large-4 columns end group-item-grid-default">
                            <div class="post thumb-border">
                                <div class="post-thumb">
                                    <img src="{{asset('fileinfographic/'.$item->filename)}}" alt="carousel">
                                    <a href="{{asset('fileinfographic/'.$item->filename)}}" onclick="updateview({{$item->id}});" class="hover-posts" data-lightbox="lightboxOverlay" data-title="{{$item->department}}">
                                        <span><i class="">{{$item->name}}</i></span>
                                    </a>
                                </div>
                                <div class="post-des">
                                        <h6><a href="#">  {{$item->department}}  </a></h6>
                                        <div class="post-stats clearfix">
                                            <p class="pull-left">
                                                <i class="fa fa-clock-o"></i>
                                                <small>{{DateTime::createFromFormat('Y-m-d H:i:s',$item->created_at)->format('d-m-Y')}}</small>
                                            </p>
                                            <p class="pull-left">
                                                <i class="fa fa-eye"></i>
                                                <small>{{$item->view}}</small>
                                            </p>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        @endforeach 
                        @endif
                    </div>
                    @include('pagination.default', ['paginator' => $Infographic])

        </section>
        </div><!-- end left side content area -->

        <!-- sidebar -->
        <div class="large-4 columns">
            <aside class="secBg sidebar">
                <div class="row">
                    <div class="large-12 medium-7 medium-centered columns">
                        <div class="widgetBox">
                            <div class="widgetTitle">
                                <h5>Category</h5>
                            </div>
                            <div class="widgetContent">
                                <ul class="accordion" data-accordion>
                                    <li class="accordion-item is-active" data-accordion-item>
                                        <a href="#" class="accordion-title">หมวดหมู่กิจกรรม</a>
                                        <div class="accordion-content" data-tab-content>
                                            <ul>
                                                @php $infographic_count = \App\InfographicTable::where('is_public', 1)->count(); @endphp
                                                <li
                                                    class="clearfix">
                                                    <i class="fa fa-angle-right"></i>
                                                    <a href="{{ route('Infographic',['id' => 0 ]) }}">ทั้งหมด <span>({{$infographic_count}})</span></a>
                                                </li>
                                                @foreach($category_select as $item)
                                                 @php $infographic_count = \App\InfographicTable::where('CategoryInfographic_id','=',$item->id)->where('is_public',1)->count(); @endphp
                                                <li class="clearfix">
                                                    <i class="fa fa-angle-right"></i>
                                                    <a href="{{ route('Infographic',['id' => $item->id ]) }}">{{$item->name}}
                                                    <span>({{$infographic_count}})</span></a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @include('layouts.social')

                </div>
            </aside>
        </div><!-- end sidebar -->
        </div>
    </div>
    <!--end off canvas content-->
</div>
<!--end off canvas wrapper inner-->
</div>
<!--end off canvas wrapper-->
<!-- script files -->
@endsection



@section('footer')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> {{-- @section('footer') --}}
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
    function updateview($id) {
        //alert($id);
        var token = "<?php echo csrf_token(); ?>";
        $.ajax({
            url: '{{ route("ajaxupdateinfographic") }}',
            datatType: 'json',
            type: 'POST',
            data: {
                '_token': token,
                'id': $id,
            },
            success: function (response) {
                //alert(response.id);
                //redirect to Playyoutube
                //window.location = response.url

            }
        });//end ajax
    }
</script> 
@endsection
