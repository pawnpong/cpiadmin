@extends('layouts.frontend')
     @section('content')
<!--breadcrumbs-->
<section id="breadcrumb">
    <div class="row">
        <div class="large-12 columns">
            <nav aria-label="You are here:" role="navigation">
                <ul class="breadcrumbs">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="{{url('/')}}">หน้าหลัก</a>
                    </li>
                    <li>
                        <span class="show-for-sr">Current: </span> วีดีโอ
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</section>
<!--end breadcrumbs-->
<!-- Premium Videos -->
<br>
<!-- End Premium Videos -->
<section class="category-content">
    <div class="row">
        <!-- left side content area -->
        <div class="large-8 columns">
            <section class="content content-with-sidebar">
                <!-- newest video -->
                <div class="main-heading removeMargin">
                    <div class="row secBg padding-14 removeBorderBottom">
                        <div class="medium-8 small-8 columns">
                            <div class="head-title">
                                <i class="fa fa-film"></i>
                                <h4>
                                    เพลย์ลิส
                                </h4>
                            </div>
                        </div>
                        <div class="medium-4 small-4 columns">
                            <ul class="tabs text-right pull-right" data-tabs id="newVideos">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row secBg">
                    <div class="large-12 columns">
                        <div class="row column head-text clearfix">
                            <p class="pull-left">
                              เพลย์ลิสทั้งหมด</p>
                            <div class="grid-system pull-right show-for-large">
                                <a class="secondary-button current grid-medium" href="#"><i class="fa fa-th-large"></i></a>
                                <a class="secondary-button list" href="#"><i class="fa fa-th-list"></i></a>
                            </div>
                        </div>
                        <div class="tabs-content" data-tabs-content="newVideos">
                            <div class="tabs-panel is-active" id="new-all">
                                <div class="row list-group">
                                    @foreach($playlists['results'] as $item)
                                    <div class="item large-4 columns end grid-medium">
                                        <div class="post thumb-border">
                                            <div class="post-thumb">
                                            <embed width="370" height="220" src="https://www.youtube.com/embed/?listType=playlist&list={{$item->id}}">
                                                
                                                <a href="https://www.youtube.com/watch?v=73ZKUbTiTxs&list={{$item->id}}" class="hover-posts"> 
                                                 <span><i class="fa fa-play"></i>Watch Video</span>
                                                </a>
                                            </div>
                                            <div class="post-des">
                                                <h6><a href="single-video-v2.html">{{$item->snippet->title}}</a></h6>
                                                <div class="post-stats clearfix">
                                                    {{-- <p class="pull-left">
                                                        <i class="fa fa-user"></i>
                                                        <span><a href="#">12</a></span>
                                                    </p> --}}
                                                    <p class="pull-left">
                                                        <i class="fa fa-clock-o"></i>
                                                        <span>{{$item->snippet->publishedAt}}</span>
                                                    </p>
                                                    {{-- <p class="pull-left">
                                                        <i class="fa fa-eye"></i>
                                                        <span>5678910</span>
                                                    </p> --}}
                                                </div>
                                                <div class="post-summary">
                                                    {{--
                                                    <p>{{$item->description}}</p> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="main-heading removeMargin">
                            <div class="row secBg padding-14 removeBorderBottom">
                                <div class="medium-8 small-8 columns">
                                    <div class="head-title">
                                        <i class="fa fa-film"></i>
                                        <h4>
                                            ทั้งหมด
                                        </h4>
                                    </div>
                                </div>
                                <div class="medium-4 small-4 columns">
                                    <ul class="tabs text-right pull-right" data-tabs id="newVideos">
                                        {{--
                                        <li class="tabs-title is-active"><a href="#new-all">all</a></li>
                                        <li class="tabs-title"><a href="#new-hd">HD</a></li> --}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row secBg">
                            <div class="large-12 columns">
                        <div class="row column head-text clearfix">
                            <p class="pull-left">
                                All Videos :
                                <span> Videos posted</span></p>
                            <div class="grid-system pull-right show-for-large">
                                <a class="secondary-button current grid-medium" href="#"><i class="fa fa-th-large"></i></a>
                                <a class="secondary-button list" href="#"><i class="fa fa-th-list"></i></a>
                            </div>
                        </div>
                        <div class="tabs-content" data-tabs-content="newVideos">
                            <div class="tabs-panel is-active" id="new-all">
                                <div class="row list-group">
                                    @foreach($videoList as $item)
                                    <div class="item large-4 columns end grid-medium">
                                        <div class="post thumb-border">
                                            <div class="post-thumb">
                                            <embed width="370" height="220" src="https://www.youtube.com/embed/{{$item->id->videoId}}">
                                                <a href="https://www.youtube.com/watch?v={{$item->id->videoId}}" class="hover-posts">
                                                 <span><i class="fa fa-play"></i>Watch Video</span>
                                                </a>
                                                <div class="video-stats clearfix">
                                                </div>
                                            </div>
                                            <div class="post-des">
                                                <h5>{{str_limit($item->snippet->title , 30 , '...')}}</h5>
                                                <div class="post-stats clearfix">
                                                    <p class="pull-left">
                                                        <i class="fa fa-clock-o"></i>
                                                        <span>{{$item->snippet->publishedAt}}</span>
                                                    </p>
                                                </div>
                                                <div class="post-summary">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                </div>
                
            </section>
           
        </div><!-- end left side content area -->
        
        <!-- sidebar -->
        <div class="large-4 columns">
            <aside class="secBg sidebar">
                <div class="row">
                    <!-- categories -->
                    {{-- <div class="large-12 medium-7 medium-centered columns">
                        <div class="widgetBox">
                            <div class="widgetTitle">
                                <h5>categories</h5>
                            </div>
                            <div class="widgetContent">
                                <ul class="accordion" data-accordion>
                                    <li class="accordion-item is-active" data-accordion-item>
                                        <a href="#" class="accordion-title">Entertainment</a>
                                        <div class="accordion-content" data-tab-content>
                                            <ul>
                                                @php $youtube_count = \App\Youtube::where('is_public', 1)->count(); @endphp
                                                <li
                                                    class="clearfix">
                                                    <i class="fa fa-play-circle-o"></i>
                                                    <a href="{{ route('Category',['id' => 0 ]) }}">ทั้งหมด <span>({{$youtube_count}})</span></a>
                                                </li>
                                                @foreach($category_select as $item) @php $youtube_count = \App\Youtube::where('category_id','=',$item->id)->where('is_public',
                                                1)->count(); @endphp
                                                <li class="clearfix">
                                                    <i class="fa fa-play-circle-o"></i>
                                                    <a href="{{ route('Category',['id' => $item->id ]) }}">{{$item->name}}
                                                        <span>({{$youtube_count}})</span></a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div> --}}
                    <!-- social Fans Widget -->
                   
                    @include('layouts.social')
                     
                  <!-- End social Fans Widget -->
                </div>
            </aside>
        </div><!-- end sidebar -->
    </div>
</section><!-- End Category Content-->
<!-- footer -->

</div>
<!--end off canvas content-->
</div>
<!--end off canvas wrapper inner-->
</div>
<!--end off canvas wrapper-->
<!-- script files -->
@endsection

