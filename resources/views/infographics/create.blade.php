@extends('layouts.master') 
@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <!-- Page-header start -->
                <div class="page-header">
                    <div class="page-header-title">
                        <h4>เพิ่มกิจกรรม</h4>
                        <span></span>
                    </div>
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{url('/')}}">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{route('admin.infographics.index')}}">กิจกรรม</a>
                                <li class="breadcrumb-item">
                                    <a href="">เพิ่มกิจกรรม</a>
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5>เพิ่มข้อมูล</h5>
                    </div>
                    <div class="card-block">
                        {!! Form::open(array('route' => 'admin.infographics.store' , 'method' => 'post' , 'files' => true )) !!} {{ csrf_field()
                        }}
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Category</label>
                            <div class="col-sm-10">
                                {!! Form::select('CategoryInfographic_id', App\CategoryInfographicTable::where('is_public' , 1)->orderby('name','asc')->pluck('name','id'),
                                null, ['placeholder' => 'กรุณาเลือกหมวดหมู่','class' => 'form-control']); !!} @if ($errors->has('CategoryInfographic_id'))
                                <span class="messages">
                                    <p class="text-danger error">{{ $errors->first('CategoryInfographic_id')}}</p>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">ชื่อโครงการ</label>
                            <div class="col-sm-10">
                                <?= Form::text('name',null,['class'=>'form-control','placeholder'=>'โครงการ']);?>
                                    <span class="messages"></span>
                                    @if ($errors->has('name'))
                                    <span class="messages">
                                        <p class="text-danger error">{{ $errors->first('name')}}</p>
                                    </span>
                                    @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">ชื่อหน่วยงาน</label>
                            <div class="col-sm-10">
                                <?= Form::text('department',null,['class'=>'form-control','placeholder'=>'หน่วยงาน']);?>
                                    <span class="messages"></span>
                                    @if ($errors->has('name'))
                                    <span class="messages">
                                        <p class="text-danger error">{{ $errors->first('name')}}</p>
                                    </span>
                                    @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">รูปภาพ</label>
                            <div class="col-sm-10">
                                <a onclick="$('#imgInp').click();">
                                    <img id="blah" style="cursor:pointer;" src="{{asset('images/nopic.png')}}" alt="รูปภาพ" class="img-fluid" />
                                </a>
                                <div style="margin-button:20px"></div>
                                <br>
                                <input type="file" id="imgInp" name="file" style="display:none;cursor:pointer;" accept="image/jpg, image/jpeg, image/png,image/gif">
                                <span class="messages"></span>
                                @if ($errors->has('file'))
                                <span class="messages">
                                    <p class="text-danger error">{{ $errors->first('file')}}</p>
                                </span>
                                @endif
                            </div>
                        </div>
                            <div class="form-group row">
                                    <label for="exampleTextarea" class="col-sm-2 col-form-label">วันที่เริ่ม</label>
                                    <div class="col-10">
                                            {!! Form::date('startdate',null,['class'=>'form-control']);!!}
                                        @if ($errors->has('startdate'))
                                        <span class="messages"><p class="text-danger error">{{ $errors->first('startdate')}}</p></span>
                                        @endif
                                    </div>
                                </div>
                            <div class="form-group row">
                                    <label for="exampleTextarea" class="col-sm-2 col-form-label">วันที่สิ้นสุด</label>
                                    <div class="col-10">
                                            {!! Form::date('enddate',null,['class'=>'form-control']);!!}
                                        @if ($errors->has('enddate'))
                                        <span class="messages"><p class="text-danger error">{{ $errors->first('enddate')}}</p></span>
                                        @endif
                                    </div>
                                </div>
                            {{--
                            <input type="file" name="file" class="form-control"> --}}
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2"></label>
                            <div class="col-sm-10">
                                <button data-key="submit" type="submit" class="btn btn-primary">
                                    <i class="ion-archive"></i>บันทึก</button>
                                <a href="{{ route('admin.infographics.index') }}" class="btn btn-default">
                                    <i class="ion-reply"></i>ยกเลิก
                                </a>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 
@section('jsfooter')
<script>
     $(document).ready(function() {
            document.getElementById('main_infographic').classList.add('active');
            document.getElementById('main_infographic').classList.add('pcoded-trigger');
            document.getElementById('infographic_active').classList.add('active');
    });
    //image headline Show
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imgInp").change(function () {
        readURL(this);
    });
</script>
@endsection


