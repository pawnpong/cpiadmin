@extends('layouts.master')
@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">   
        <div class="main-body">
            <div class="page-wrapper">
                @if(session('feedback'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Success!</strong> {{ session('feedback') }}
                    </div>
                @endif
                @if(session('feedbackdelete'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Success!</strong> {{ session('feedbackdelete') }}
                    </div>
                @endif
                <!-- Page-header start -->
                <div class="page-header">
                    <div class="page-header-title">
                        <h4>วีดีโอ</h4>
                        <span></span>
                    </div>
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{url('/')}}">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="">วีดีโอ</a>
                            
                        </ul>
                    </div>
                </div>
                <!-- Hover table card start -->
                <div class="card">
                    <div class="card-header">
                            <a href="{{route('admin.youtube.create')}}">
                        <h5><button class="btn btn-info"><i class="ion-plus"></i>เพิ่มข้อมูล</button></h5>
                        <span></span></a>
                        <div class="card-header-right">
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="dt-responsive table-responsive">
                            <table id="simpletable" class="table table-striped table-bordered nowrap">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>หมวดหมู่</th>
                                        <th>ชื่อวีดีโอ</th>
                                        <th>Link</th>
                                        <th>รายละเอียด</th>
                                        <th>view</th>
                                        <th>create_by</th>
                                        <th>วันที่เริ่ม</th>
                                        <th>วันที่สิ้นสุด</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($youtube as $i=>$row)
                                    <tr>
                                        <td>{{$i+1}}</td>
                                        <td>{{$row->category->name}}</td>
                                        <td>{{$row->title}}</td>
                                        <td>{{$row->link}}</td>
                                        <td> 
                                            <button type="button" class="btn btn-inverse waves-effect " data-toggle="modal" data-target="#description{{$row->id}}"><i class="ion-ios-search"></i></button>
                                        </td>
                                        <td>{{$row->view}}</td>
                                        <td>{{$row->create_by}}</td>
                                        <td>{{DateTime::createFromFormat('Y-m-d', $row->startdate)->format('d-m-Y') }}</td>
                                        <td>{{DateTime::createFromFormat('Y-m-d', $row->enddate)->format('d-m-Y') }}</td>
                                        <td>
                                            <a href="{{ route('admin.youtube.edit',$row->id) }}" class="btn   btn-warning waves-effect waves-light"><i class="fa fa-pencil"></i> แก้ไข</a>
                                            <a href="" class=" btn btn-danger  waves-effect waves-light delBtn" data-id="{{$row->id}}"><i class="fa fa-times" ></i> ลบ</a>
                                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                        </td>
                                    </tr>      
                                    <!-- Modal description-->
                                    <div class="modal fade" id="description{{$row->id}}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">{{$row->title}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true" style="font-size: 1.50em;cursor: pointer;"><i class="ion-close-circled"></i></span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                @php echo $row->description @endphp
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Modal description-->
                                    @php $i++ @endphp
                                    @endforeach
                                </tbody>  
                            </table>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>

@endsection
@section('jsfooter')
<script>
       $(document).ready(function() {
            document.getElementById('main_youtube').classList.add('active');
            document.getElementById('main_youtube').classList.add('pcoded-trigger');
            document.getElementById('youtube_active').classList.add('active');
    });
		window.setTimeout(function(){
            $('.alert').fadeTo(500,0).slideUp(500,function(){
                $(this).remove();
            })
        }, 3000);

$(document).on('click', '.delBtn', function (e) {
            
            e.preventDefault();
            var id = $(this).data('id');
            // alert(id);
            swal({
                title: "คุณต้องการลบ?",
                text: "หากคุณทำการลบข้อมูล จะไม่สามารถทำการกู้คืนได้อีก",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    // alert(id);
                    $.ajax({
                            method: "DELETE",
                            url: '{{ url('admin/youtube')}}/'+id,
                            data: {ids:id,_token: $('#_token').val(),},
                            success: function (data) {
                                if(data.success =="1"){
                                    swal("ทำการลบข้อมูลสำเร็จ", {
                                        icon: "success",
                                    }).then(()=>{ location.reload(); });     
                                }else{
                                    swal({
                                        title:    "พบข้อผิดพลาด",
                                        text: "กรุณาติดต่อผู้ดูแลระบบ",
                                        icon: "warning",
                                        dangerMode: true,
        
                                    });
                                }
                            }         
                    });
                } else {
                    //swal("ยกเลิกการลบข้อมูล");
                }
            });
        });
    </script>
@endsection
