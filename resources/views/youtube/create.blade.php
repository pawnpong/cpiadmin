@extends('layouts.master') 
@section('content')

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <!-- Page-header start -->
                <div class="page-header">
                    <div class="page-header-title">
                        <h4>เพิ่มวีดีโอ</h4>
                        <span></span>
                    </div>
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{url('/')}}">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{route('admin.youtube.index')}}">วีดีโอ</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">เพิ่มวีดีโอ</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5>เพิ่มข้อมูล</h5>
                        <span></span>
                    </div>
                    <div class="card-block">
                        {!! Form::open(array('route' => 'admin.youtube.store' , 'method' => 'post' )) !!} {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">หมวดหมู่วีดีโอ</label>
                            <div class="col-sm-10">
                                {!! Form::select('Category_id', App\CategoryTable::where('is_public' , 1)->orderby('name','asc')->pluck('name','id'), null,
                                ['placeholder' => 'กรุณาเลือกหมวดหมู่','class' => 'form-control']); !!} @if ($errors->has('Category_id'))
                                <span class="messages">
                                    <p class="text-danger error">{{ $errors->first('Category_id')}}</p>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">ชื่อวีดีโอ</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="title" id="title" placeholder="กรุณากรอกชื่อวีดีโอ"> @if ($errors->has('title'))
                                <span class="messages">
                                    <p class="text-danger error">{{ $errors->first('title')}}</p>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="exampleTextarea" class="col-2 col-form-label">Link</label>
                            <div class="col-10">
                                {{--
                                <textarea class="form-control" id="link" name="link" rows="3" placeholder="กรุณากรอก Link"></textarea> --}}
                                <?= Form::text('link',null,['class'=>'form-control','placeholder'=>'กรุณาใส่ Link']);?>
                                    @if ($errors->has('link'))
                                    <span class="messages">
                                        <p class="text-danger error">{{ $errors->first('link')}}</p>
                                    </span>
                                    @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">รายละเอียด</label>
                            <div class="col-sm-10">
                                {!! Form::textarea('description',null,['class'=>'form-control','placeholder'=>'กรุณากรอกรายละเอียด']); !!} @if ($errors->has('description'))
                                <span class="messages">
                                    <p class="text-danger error">{{ $errors->first('description')}}</p>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="exampleTextarea" class="col-sm-2 col-form-label">วันที่เริ่ม</label>
                            <div class="col-10">
                                    {!! Form::date('startdate',null,['class'=>'form-control']);!!}
                                @if ($errors->has('startdate'))
                                <span class="messages"><p class="text-danger error">{{ $errors->first('startdate')}}</p></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="exampleTextarea" class="col-sm-2 col-form-label">วันที่สิ้นสุด</label>
                            <div class="col-10">
                                    {!! Form::date('enddate',null,['class'=>'form-control']);!!}
                                @if ($errors->has('enddate'))
                                <span class="messages"><p class="text-danger error">{{ $errors->first('enddate')}}</p></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2"></label>
                            <div class="col-sm-10">
                                <button data-key="submit" type="submit" class="btn btn-primary">
                                    <i class="ion-archive"></i>บันทึก</button>
                                <a href="{{ route('admin.youtube.index') }}" class="btn btn-default">
                                    <i class="ion-reply"></i>ยกเลิก
                                </a>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('jsfooter')
<script>
        $(document).ready(function() {
             document.getElementById('main_youtube').classList.add('active');
             document.getElementById('main_youtube').classList.add('pcoded-trigger');
             document.getElementById('youtube_active').classList.add('active');
     });
</script>
@endsection