
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>สลด คลิปนาทีสึนามิ ถล่มอินโดฯ คนร้องบอกให้วิ่งหนีตาย ก่อนคลื่นยักษ์ซัดเละกับตา</title>
<link rel="canonical" href="https://hilight.kapook.com/view/178501">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="Description" content="สึนามิอินโดนีเซีย เปิดคลิปนาทีวิ่งหนีเอาชีวิตรอด ก่อนคลื่นยักษ์ สึนามิ ถล่มริมชายฝั่ง เผยเสียงคนถ่ายตะโกนเตือน สึนามิอินโดนีเซีย ก่อนจบลงด้วยภาพของความสูญเสีย">
<meta name="Keywords" content="สึนามิอินโดนีเซีย, สึนามิ, สึนามิ อินโดนีเซีย, แผ่นดินไหวอินโดนีเซีย, แผ่นดินไหว">
<link rel="image_src" type="image/jpeg" href="https://hilight.kapook.com/image_fb/36/178501-new-148859.jpg">
<meta property="fb:app_id" content="306795119462">
<meta property="fb:admins" content="1592216870">
<meta property="og:type" content="website">
<meta property="og:title" content="สลด ! คลิปนาทีสึนามิ ถล่มอินโดฯ คนร้องบอกให้วิ่งหนีตาย ก่อนคลื่นยักษ์ซัดเละ">
<meta property="og:description" content="จบลงด้วยภาพของความสูญเสีย และเสียงสะอื้นของผู้ถ่ายคลิป...">
<meta property="og:image" content="https://hilight.kapook.com/image_fb/36/178501-new-148859.jpg">
<meta property="og:url" content="https://hilight.kapook.com/view/178501">
<meta property="og:site_name" content="kapook.com">
<meta property="article:published_time" content="2018-10-01T09:22:16+07:00">
<meta property="article:modified_time" content="2018-10-01T10:50:05+07:00">
<meta property="og:updated_time" content="2018-10-01T10:50:05+07:00">
<meta name="publish-date" content="2018-10-01T09:22:16+07:00">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@kapookdotcom">
<meta name="twitter:creator" content="@kapookdotcom">
<meta name="twitter:title" content="สลด ! คลิปนาทีสึนามิ ถล่มอินโดฯ คนร้องบอกให้วิ่งหนีตาย ก่อนคลื่นยักษ์ซัดเละ">
<meta name="twitter:description" content="จบลงด้วยภาพของความสูญเสีย และเสียงสะอื้นของผู้ถ่ายคลิป...">
<meta name="twitter:image:src" content="https://hilight.kapook.com/image_fb/36/178501-new-148859.jpg">
<meta name="twitter:domain" content="kapook.com">
<meta name="robots" content="index, follow">
<meta name="cXenseParse:pageclass" content="article">
<meta name="cXenseParse:articleid" content="178501">
                            <meta name="cXenseParse:taxonomy" content="hilight/ข่าวต่างประเทศ">
                            <meta name="cXenseParse:category" content="hilight">
                            <meta name="cXenseParse:category" content="ข่าวต่างประเทศ">
                

<link rel="shortcut icon" href="//www.kapook.com/favicon.ico">
<link rel="dns-prefetch" href="//www.google-analytics.com">
<link rel="dns-prefetch" href="//googleads.g.doubleclick.net">
<link rel="dns-prefetch" href="//connect.facebook.net">
<link rel="dns-prefetch" href="//fonts.googleapis.com">
<link rel="dns-prefetch" href="//my.kapook.com">

<link rel="stylesheet" href="https://my.kapook.com/fonts/kittithada_roman/fontface.css">
<link rel="stylesheet" href="https://my.kapook.com/fonts/chatthai/fontface.css">
<link href="https://fonts.googleapis.com/css?family=Kanit:700" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all" href="//my.kapook.com/css/portal/theme.css">
<link rel="stylesheet" type="text/css" media="all" href="//my.kapook.com/signin_2017/css/main.css">


<style>
  .nav ul>li>a:before { content:""; display:inline; float:left; margin: 0 2px 0 0; width:30px; height:40px; background:url(//my.kapook.com/svg-portal/ico-hilight.svg) 0 0 no-repeat; background-size:24px;}
  .nav ul>li.active>a:before, .nav ul>li:hover>a:before { background:url(//my.kapook.com/svg-portal/ico-hilight2.svg) 0 0 no-repeat; background-size:24px;}
  .nav ul>li:first-child>a:before { background-position: 0 0}
  .nav ul>li:nth-child(2)>a:before { background-position: 0 -40px}
  .nav ul>li:nth-child(3)>a:before { background-position: 0 -80px}
  .nav ul>li:nth-child(4)>a:before { background-position: 0 -120px}
  .nav ul>li:nth-child(5)>a:before { background-position: 0 -160px}
  .nav ul>li:nth-child(6)>a:before { background-position: 0 -200px}
  .nav ul>li:nth-child(7)>a:before { background-position: 0 -240px}
  .nav ul>li:nth-child(8)>a:before { background-position: 0 -280px}
  .nav ul>li:nth-child(9)>a:before { background-position: 0 -320px}

  .cx-related { margin:10px 0; width:100%; clear:both; overflow:hidden}
  .cx-related .header-related strong {font-family:'kittithada_roman'; display: block; font-weight: normal; font-size:34px; padding: 0 5px 5px;}
  .cx-related .cx-item { overflow:hidden}
  .cx-related .cx-item a { display:block; width:100%;}
  .cx-related .cx-item img { width:100%; height:auto;}
  .cx-related .cx-item h3 { font-family:'CS ChatThaiUI'; font-size: 16px; line-height: 24px; font-weight: normal; font-style: normal; color: #000; padding:0 5px 9px; margin: 0;}
  .cx-related .cx-item a:hover h3 { color:rgb(128, 0, 0); }

  .hidden {display: none !important;}
  .btn-subscribe { background:#22b573; outline: 0; display:block; cursor:pointer; padding: 6px 20px; margin: 10px auto; font-family: 'kittithada_roman', Tahoma, sans-serif; font-size: 34px; font-weight: normal; border: 0; color: #FFF; border-radius:10px; box-shadow:0 3px 3px rgba(0,0,0,0.2)}
  .btn-subscribe:hover { background:#dc4f1b; box-shadow:0 1px 3px rgba(0,0,0,0.2); margin: 12px auto 8px;}
  .show-when-unsubscribed { position:fixed; top: 76px; left: 50%; box-shadow: 0 2px 4px rgba(0,0,0,0.2); transform: translateX(-50%); z-index:99; border: 4px solid #ccc; text-align:center; border-radius: 6px; padding: 30px 10px 20px; background: #FFF; margin: 10px 0;}
  .show-when-unsubscribed strong { color:#000; font-size: 24px; font-weight: normal;}
  .show-when-unsubscribed p { color:#000; font-size:16px; padding:10px 20px;}
  .closepop { position:absolute; top:10px; right:10px; width:16px; height:16px; cursor:pointer;}
  .closepop:before { content:"X"; display:block; position:absolute; top:0; left:0; width:16px; height:16px;}
</style>
<script src="//my.kapook.com/jquery/jquery-1.9.1.min.js"></script>
    <meta name="category" content="hilight_world">

<link href="//my.kapook.com/css-template2017/content_template2017.css" rel="stylesheet" type="text/css" media="all">

  
  <!-- pubmatic > dfp -->
  <script type="text/javascript">
            var PWT={}; //Initialize Namespace
            var googletag = googletag || {};
            googletag.cmd = googletag.cmd || [];
            PWT.jsLoaded = function(){ //PubMatic pwt.js on load callback is used to load GPT
                (function() {
                    var gads = document.createElement('script');
                    var useSSL = 'https:' == document.location.protocol; 
                    gads.src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
                    var node = document.getElementsByTagName('script')[0];
                    node.parentNode.insertBefore(gads, node);
                })();
            };
            (function() {
                var purl = window.location.href;
                var url = '//ads.pubmatic.com/AdServer/js/pwt/156743/740';
                var profileVersionId = '';
                if(purl.indexOf('pwtv=')>0){
                    var regexp = /pwtv=(.*?)(&|$)/g;
                    var matches = regexp.exec(purl);
                    if(matches.length >= 2 && matches[1].length > 0){
                        profileVersionId = '/'+matches[1];
                    }
                }
                var wtads = document.createElement('script');
                wtads.async = true;
                wtads.type = 'text/javascript';
                wtads.src = url+profileVersionId+'/pwt.js';
                var node = document.getElementsByTagName('script')[0];
                node.parentNode.insertBefore(wtads, node);
            })();
  </script>
  
        
  <script>
    var slot1;
    var cX = cX || {}; cX.callQueue = cX.callQueue || [];
    cX.callQueue.push(['invoke', function() {
      googletag.cmd.push(function() {
                            googletag.defineSlot('/16357739/Bloomblock_Hilight_300x250_ATF_(18)', [[1, 1], [300, 250]], 'div-gpt-ad-1530788081041-0').addService(googletag.pubads());
                                      googletag.defineSlot('/16357739/BoomBlock_Hilight_300x250_ATF', [[300, 600], [1, 1]], 'div-gpt-ad-1530788081041-1').addService(googletag.pubads());
                                      googletag.defineSlot('/16357739/hilight_news_728_pc', [[728, 90], [1, 1]], 'div-gpt-ad-1530788081041-2').addService(googletag.pubads());
                                      googletag.defineSlot('/16357739/hilight_news_970_pc', [970, 250], 'div-gpt-ad-1530788081041-3').addService(googletag.pubads());
                                                        googletag.defineSlot('/16357739/mob', [1500, 900], 'div-gpt-ad-1536326235215-0').addService(googletag.pubads());
                          
                  slot1 = googletag.defineSlot('/16357739/gallery_desktop_728', [[728, 90], [1, 1]], 'div-gpt-ad-1535441830629-0').addService(googletag.pubads());
                googletag.pubads().setTargeting("CxSegments", cX.getUserSegmentIds({persistedQueryId:'7c24d158794e4cc5fc76ef93093f129cc9e98ea2'}));
        googletag.pubads().enableSingleRequest();
        googletag.pubads().collapseEmptyDivs(true);
        googletag.enableServices();
      });
    }]);
  </script>
<!-- Cxense -->
  <script type="text/javascript">
        var cX = cX || {}; cX.callQueue = cX.callQueue || [];
        cX.callQueue.push(['setSiteId', '1131754764715524828']);
                
        cX.callQueue.push(['sendPageViewEvent']);  
        cX.callQueue.push(['setEventAttributes', { origin: 'bcc-kapook', persistedQueryId: 'b3a826f54065885b8e9030c9b1b032a4545a5451' }]);  
        </script>
        <script type="text/javascript">
        (function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
        e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
        t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
  </script>
<!-- Cxense -->

<!-- Taboola -->
<script type="text/javascript">
        window._taboola = window._taboola || [];
        _taboola.push({article:'auto'});
        !function (e, f, u, i) {
            if (!document.getElementById(i)){
            e.async = 1;
            e.src = u;
            e.id = i;
            f.parentNode.insertBefore(e, f);
            }
        }(document.createElement('script'),
        document.getElementsByTagName('script')[0],
        '//cdn.taboola.com/libtrc/kapook/loader.js',
        'tb_loader_script');
        if(window.performance && typeof window.performance.mark == 'function')
            {window.performance.mark('tbl_ic');}
</script>
<!-- Taboola -->

</head>
<body itemscope itemtype="https://schema.org/NewsArticle" >

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v2.12';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- GA -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-36103152-15', 'kapook.com');
  ga('require', 'GTM-TTM3PW3');
  ga('send', 'pageview');
  // var campaignName = 'hilight-desktop';
</script>

<div itemprop="author" itemscope itemtype="http://schema.org/Organization">
  <meta itemprop="name" content="Kapook.com">
</div>   

<!-- skin -->
  <div id='div-gpt-ad-1536326235215-0' style='height:1px; width:1px;'>
  <script>
  googletag.cmd.push(function() { googletag.display('div-gpt-ad-1536326235215-0'); });
  </script>
  </div>

<div class="head">
  <div class="container">
    <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
      <div class="logo" itemprop="logo" itemscope itemtype="https://schema.org/ImageObject"><a href="//www.kapook.com"><img src="//my.kapook.com/img-portal/logo-kapook.png" alt=""></a>
        <meta itemprop="url" content="https://my.kapook.com/img-portal/logo-kapook.png">
        <meta itemprop="width" content="140">
        <meta itemprop="height" content="36">
      </div>
      <meta itemprop="name" content="Kapook.com">
    </div>
    <div class="portal">
      <h2><a href="/" title="">HILIGHT</a></h2>
      <div class="menu" track="listportal"></div>
      <div class="listportal2">
                      <div class="nav-news">
                 <strong>ข่าว-ความรู้</strong>
                                     <a href="https://news.kapook.com" track="listnav-ข่าว">ข่าว</a>
                                     <a href="https://women.kapook.com/star" track="listnav-ข่าวบันเทิง">ข่าวบันเทิง</a>
                                     <a href="http://football.kapook.com" track="listnav-ฟุตบอล">ฟุตบอล</a>
                                     <a href="https://money.kapook.com" track="listnav-การเงิน">การเงิน</a>
                                     <a href="https://education.kapook.com" track="listnav-การศึกษา">การศึกษา</a>
                              </div>
                      <div class="nav-entertain">
                 <strong>บันเทิง</strong>
                                     <a href="https://world.kapook.com/photo" track="listnav-รูปภาพ">รูปภาพ</a>
                                     <a href="http://movie.kapook.com" track="listnav-ดูหนัง">ดูหนัง</a>
                                     <a href="https://musicstation.kapook.com" track="listnav-Music Station">Music Station</a>
                                     <a href="https://drama.kapook.com" track="listnav-ละคร">ละคร</a>
                                     <a href="https://star.kapook.com/koreanews" track="listnav-บันเทิงเกาหลี">บันเทิงเกาหลี</a>
                              </div>
                      <div class="nav-lifestyle">
                 <strong>ไลฟ์ไตล์</strong>
                                     <a href="https://horoscope.kapook.com" track="listnav-ดูดวง">ดูดวง</a>
                                     <a href="https://women.kapook.com" track="listnav-ผู้หญิง">ผู้หญิง</a>
                                     <a href="https://men.kapook.com" track="listnav-ผู้ชาย">ผู้ชาย</a>
                                     <a href="https://health.kapook.com" track="listnav-สุขภาพ">สุขภาพ</a>
                                     <a href="https://travel.kapook.com" track="listnav-ท่องเที่ยว">ท่องเที่ยว</a>
                                     <a href="https://cooking.kapook.com" track="listnav-สูตรอาหารง่ายๆ">สูตรอาหารง่ายๆ</a>
                              </div>
                      <div class="nav-shopping">
                 <strong>ช้อปปิ้ง</strong>
                                     <a href="https://car.kapook.com" track="listnav-รถยนต์">รถยนต์</a>
                                     <a href="https://home.kapook.com" track="listnav-บ้านและการตกแต่ง">บ้านและการตกแต่ง</a>
                                     <a href="https://mobile.kapook.com" track="listnav-มือถือ">มือถือ</a>
                                     <a href="https://money.kapook.com/gold" track="listnav-ราคาทอง">ราคาทอง</a>
                                     <a href="https://gasprice.kapook.com/gasprice.php" track="listnav-ราคาน้ำมัน">ราคาน้ำมัน</a>
                              </div>
                      <div class="nav-photo">
                 <strong>วาไรตี้</strong>
                                     <a href="https://wedding.kapook.com" track="listnav-แต่งงาน">แต่งงาน</a>
                                     <a href="https://baby.kapook.com" track="listnav-แม่และเด็ก">แม่และเด็ก</a>
                                     <a href="https://pet.kapook.com" track="listnav-สัตว์เลี้ยง">สัตว์เลี้ยง</a>
                                     <a href="https://infographic.kapook.com" track="listnav-Infographic">Infographic</a>
                              </div>
                      <div class="nav-variety">
                 <strong>บริการ</strong>
                                     <a href="http://m.kapook.com/app" track="listnav-แอปฯ กระปุก">แอปฯ กระปุก</a>
                                     <a href="https://www.bunditcenter.com" track="listnav-คอร์สออนไลน์">คอร์สออนไลน์</a>
                                     <a href="https://learning.kapook.com" track="listnav-เรียนเลขออนไลน์">เรียนเลขออนไลน์</a>
                                     <a href="https://www.kapook.com/advert" track="listnav-ติดต่อโฆษณา">ติดต่อโฆษณา</a>
                                     <a href="https://www.kapook.com/notice" track="listnav-แจ้งปัญหา">แจ้งปัญหา</a>
                                     <a href="http://job.kapook.com" track="listnav-ร่วมงานกับเรา">ร่วมงานกับเรา</a>
                              </div>
                </div>
    </div>

    <div class="today">


      <signin-box></signin-box>

      <script type="text/javascript" language="javascript1.1">page='hilight_content';</script>      <div id="truehits_div"></div>
      <script type="text/javascript">
        (function(){var ga1 = document.createElement('script');ga1.type='text/javascript';ga1.async=true;ga1.src="//lvs.truehits.in.th/dataa/a0000034.js";var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(ga1, s);})();
      </script>
    </div>
  </div>
</div>

<div class="nav">
  <div class="container">
    <ul class="list-top-menu">
          <li catc="home"><a href="https://news.kapook.com" track="nav-หน้าแรก"  >หน้าแรก</a></li>
            <li catc="star.php"><a href="https://women.kapook.com/star.php" track="nav-ข่าวดารา"  >ข่าวดารา</a></li>
            <li catc="politic"><a href="https://news.kapook.com/politic" track="nav-ข่าวการเมือง"  >ข่าวการเมือง</a></li>
            <li catc="crime"><a href="https://news.kapook.com/crime" track="nav-ข่าวอาชญากรรม"  >ข่าวอาชญากรรม</a></li>
            <li catc="world"><a href="https://news.kapook.com/world" track="nav-ข่าวต่างประเทศ"  >ข่าวต่างประเทศ</a></li>
            <li catc="social_media"><a href="https://news.kapook.com/social_media" track="nav-ข่าวฮิตสังคมออนไลน์"  >ข่าวฮิตสังคมออนไลน์</a></li>
            <li catc="x-file"><a href="https://news.kapook.com/x-file" track="nav-x-file"  >x-file</a></li>
          </ul>
  </div>
</div>

<script type="text/javascript">
  var current_cat = "http://news.kapook.com/world";
</script>
  

<!-- alert -->
<div id="subpop">
  <div class="hidden show-when-unsubscribed">
    <strong>บริการส่งเรื่องฮอต ข่าวฮิต</strong>
    <button id="button-subscribe" class="btn-subscribe">รับบริการแจ้งเตือน ฟรี!</button>
    <p>ไม่อยากพลาดข่าวร้อน เรื่องฮิต กิจกรรมแจกของรางวัลมากมาย คลิกเลย</p>
    <span class="closepop" onclick="closepop();"></span>      
  </div>
  <script src="https://www.kapook.com/testnoti/javascripts/scale.fix.js"></script>
  <script src="https://www.kapook.com/testnoti/javascripts/iframe-init.js"></script>
</div>




            <div class="fullbanner">
        <div id="div-gpt-ad-1530788081041-3">
          <script>
            cX.callQueue.push(['invoke', function() {
              googletag.cmd.push(function() { googletag.display('div-gpt-ad-1530788081041-3'); });
            }]);    
          </script>
        </div>
      </div>
      
  
  <div id="nav-top">
    <p class="bradecramp" itemscope itemtype="https://schema.org/BreadcrumbList">
      <span class="icon-home"></span>
      <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="https://news.kapook.com"  title="หน้าแรก hilight" itemprop="item"><span itemprop="name">hilight</span></a></span>
      <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="http://news.kapook.com/world" target="_blank"  title="ข่าวต่างประเทศ" itemprop="item"><span itemprop="name">ข่าวต่างประเทศ</span></a></span>
    </p>
  </div>
<meta itemscope itemprop="mainEntityOfPage" itemType="https://schema.org/WebPage" itemid="https://hilight.kapook.com/view/178501">
<meta itemprop="image" content="https://hilight.kapook.com/image_fb/36/178501-new-148859.jpg">
<div id="container">
  <div class="article" itemprop="articleBody">
    <h1 itemprop="headline">สลด คลิปนาทีสึนามิ ถล่มอินโดฯ คนร้องบอกให้วิ่งหนีตาย ก่อนคลื่นยักษ์ซัดเละกับตา</h1>
    <meta itemprop="dateCreated" content="2018-10-01 09:22:16">
    <meta itemprop="datePublished" content="2018-10-01 10:50:05">
    <meta itemprop="dateModified" content="2018-10-01 10:50:05">
          <meta content="October 01, 2018 at 09:22:16">
      <meta content="October 01, 2018 at 09:22:16">
      <meta content="October 01, 2018 at 10:50:05">    <div class="social-share">
              <p class="num-view setview_em">37,789<em>อ่าน</em></p>
        <div itemprop="interactionStatistic" itemscope itemtype="http://schema.org/InteractionCounter">
            <meta itemprop="interactionType" content="http://schema.org/ViewAction" />
            <meta itemprop="userInteractionCount" content="37,789" />
        </div> 
          </div>
    <div class="share-group">
      <div class="tw-share tw-btn" data-url="https://hilight.kapook.com/view/178501"><a href="#" class="btnClick"><img src="//my.kapook.com/review/svg/twitter.svg" alt="" /></a></div>
      <div class="fb-share fb-btn" data-url="https://hilight.kapook.com/view/178501"><a href="#" class="btnClick"><img src="//my.kapook.com/review/svg/facebook.svg" alt="" /><span class="boxCount">0</span></a></div>
      <div itemprop="interactionStatistic" itemscope itemtype="http://schema.org/InteractionCounter">
          <meta itemprop="interactionType" content="http://schema.org/ShareAction" />
          <meta itemprop="userInteractionCount" id="meta_view" content="" />
      </div>
    </div>

    <div class="content">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>เปิดคลิปวินาทีผู้คนบนเกาะสุลาเวสี ประเทศอินโดนีเซีย วิ่งหนีเอาชีวิตรอดก่อนคลื่นยักษ์สึนามิถล่มริมชายฝั่ง เผยเสียงคนถ่ายคลิปร้องตะโกนเตือน ก่อนจบลงด้วยภาพของความสูญเสีย</b><br />      <br>
                        <div id="InRead"></div>
          <div class="banner_inread" id="zb3ca2a4d-3ddc-4098-a4ed-cc8352548077" style='display:none' ></div>
          <script>!function(a,n,e,t,r){tagsync=e;var c=window[a];if(tagsync){var d=document.createElement("script");d.src="https://3326.tm.zedo.com/v1/5bd6d11c-2716-4757-9c8b-e4e5774d25a4/atm.js",d.async=!0;var i=document.getElementById(n);if(null==i||"undefined"==i)return;i.parentNode.appendChild(d,i),d.onload=d.onreadystatechange=function(){var a=new zTagManager(n);a.initTagManager(n,c,this.aync,t,r)}}else document.write("<script src='https://3326.tm.zedo.com/v1/5bd6d11c-2716-4757-9c8b-e4e5774d25a4/tm.js?data="+a+"'><"+"/script>")}("datalayer","zb3ca2a4d-3ddc-4098-a4ed-cc8352548077",true, 1 , 1);</script>     
                            
<div align="center"><img alt="สึนามิอินโดนีเซีย" title="สึนามิอินโดนีเซีย" class="img-mobile" src="//hilight.kapook.com/img_cms2/user/sarinee/2018/D10/w1/004a1.jpg" /><br />ภาพจาก <a target="_blank" href="https://www.facebook.com/memoriesatp/videos/2037719352961776/">เฟซบุ๊ก ราชสีห์ จิตอาสา</a><br /><br /><img alt="สึนามิอินโดนีเซีย" title="สึนามิอินโดนีเซีย" class="img-mobile" src="//hilight.kapook.com/img_cms2/user/sarinee/2018/D10/w1/004a2.jpg" /><br />ภาพจาก <a target="_blank" href="https://www.facebook.com/memoriesatp/videos/2037719352961776/">เฟซบุ๊ก ราชสีห์ จิตอาสา</a> 
</div><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; เมื่อวันที่ 30 กันยายน 2561 <a target="_blank" href="https://www.facebook.com/memoriesatp/videos/2037719352961776/">เพจเฟซบุ๊ก ราชสีห์ จิตอาสา</a> ได้มีการเผยแพร่คลิปวิดีโอเหตุการณ์แผ่นดินไหวที่รุนแรงถึง 7.5 แมกนิจูด ส่งผลทำให้เกิดคลื่นยักษ์สึนามิ เข้าถล่มเมืองปาลู บนเกาะสุลาเวสี ทางตอนกลางของประเทศอินโดนีเซีย เมื่อวันที่ 29 กันยายน ที่ผ่านมา ซึ่งขณะนี้มีรายงานผู้เสียชีวิตล่าสุด 832 ราย และได้รับบาดเจ็บอีกกว่า 540 ราย<br /><br /> 
<div align="center"><img alt="สึนามิอินโดนีเซีย" title="สึนามิอินโดนีเซีย" class="img-mobile" src="//hilight.kapook.com/img_cms2/user/sarinee/2018/D10/w1/004a3.jpg" /><br />ภาพจาก <a target="_blank" href="https://www.facebook.com/memoriesatp/videos/2037719352961776/">เฟซบุ๊ก ราชสีห์ จิตอาสา</a><br /><br /><img alt="สึนามิอินโดนีเซีย" title="สึนามิอินโดนีเซีย" class="img-mobile" src="//hilight.kapook.com/img_cms2/user/sarinee/2018/D10/w1/004a4.jpg" /><br />ภาพจาก <a target="_blank" href="https://www.facebook.com/memoriesatp/videos/2037719352961776/">เฟซบุ๊ก ราชสีห์ จิตอาสา</a><br /> 
</div>      <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="color: rgb(128, 0, 0);">โดยทั้ง 2 คลิป เป็นคลิปที่ผู้อยู่ในเหตุการณ์สามารถบันทึกภาพเอาไว้ได้ ซึ่งคลิปแรกเป็นคลิปที่ผู้ถ่ายซึ่งหนีขึ้นมาอยู่บนอาคารสูง ได้มีการร้องตะโกนเตือนผู้คนที่ยังอยู่ด้านล่างว่ามีคลื่นยักษ์สึนามิกำลังเคลื่อนตัวเข้ามาที่ฝั่ง จึงเห็นได้ว่าผู้คนต่างพากันวิ่งหนีเพื่อเอาชีวิตรอด ก่อนที่คลื่นยักษ์จะซัดเข้ามาในเวลาต่อมาเพียงไม่กี่นาที เจ้าของคลิปถึงกับร้องไห้เมื่อเขาได้เห็นภาพแห่งความสูญเสียครั้งใหญ่ครั้งนี้</span><br /><br /> 
<div align="center"><iframe scrolling="no" allowtransparency="true" allowfullscreen="true" width="261" height="476" frameborder="0" style="border: medium none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fmemoriesatp%2Fvideos%2F2037719352961776%2F&amp;show_text=0&amp;width=261"></iframe><br /> 
</div><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; คลิปที่ 2 เป็นคลิปวิดีโอที่ผู้ประสบเหตุสามารถบันทึกภาพไว้ได้เช่นกัน กับวินาทีการเกิดแผ่นดินไหว จนทำให้พื้นดินสั่นสะเทือน ก่อนจะแตกแยกออกจากกัน<br /><br /> 
<div align="center"><iframe scrolling="no" allowtransparency="true" allowfullscreen="true" width="267" height="476" frameborder="0" style="border: medium none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fmemoriesatp%2Fvideos%2F2037756449624733%2F&amp;show_text=0&amp;width=267"></iframe><br /> 
</div><br /> 
<div align="center"><iframe scrolling="no" allowtransparency="true" allowfullscreen="true" width="380" height="476" frameborder="0" style="border: medium none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F100010258505456%2Fvideos%2F728790877472856%2F&amp;show_text=0&amp;width=380"></iframe><br /> 
</div><br /> 
<div align="center"><img alt="สึนามิอินโดนีเซีย" title="สึนามิอินโดนีเซีย" class="img-mobile" src="//hilight.kapook.com/img_cms2/user/sarinee/2018/D10/w1/004a5.jpg" /><br />ภาพจาก <a target="_blank" href="https://www.facebook.com/100010258505456/videos/728790877472856/">เฟซบุ๊ก ราชสีห์ จิตอาสา</a><br /> 
</div><br />    </div>

    <div class="tag-list-related">
      <div class="compound" style="display:none">
        <h3>ประเด็นที่เกี่ยวข้อง</h3>
        <ul id="ulcompound">
        </ul>
      </div>
      <div class="person" style="display:none">
        <h3>บุคคลที่เกี่ยวข้อง</h3>
        <ul id="ulperson">
        </ul>
      </div>
      <div class="where" style="display:none">
        <h3>สถานที่ที่เกี่ยวข้อง</h3>
        <ul id="ulplace">
        </ul>
      </div>
      <div class="ulsinge" style="display:none">
        <h3>ประเด็นที่เกี่ยวข้อง</h3>
        <ul id="ulsinge">
        </ul>
      </div>
    </div>
  </div>
  <div class="aside">

                  <div class="banner">
          <div id="div-gpt-ad-1530788081041-0">
            <script>
              cX.callQueue.push(['invoke', function() {
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1530788081041-0'); });
              }]);    
            </script>
          </div>
        </div>    
            
        
    <!-- test cxense php -->
          <div style="width:100%; margin:10px auto">
      <strong style="font-family: 'kittithada_roman'; font-weight: normal; font-size:34px; padding: 0 5px 5px;">เรื่องที่คุณอาจสนใจ</strong>    
      <!-- Cxense content widget: Aside : Article page -->
      <div id="cx_ca9d72de82ca2e5ac4ea39c0c87c60f31963bcf7" style="display:none"></div>
      <script type="text/javascript">
          var cX = cX || {}; cX.callQueue = cX.callQueue || [];
            cX.callQueue.push(['insertWidget', {
              widgetId: 'ca9d72de82ca2e5ac4ea39c0c87c60f31963bcf7',
              insertBeforeElementId: 'cx_ca9d72de82ca2e5ac4ea39c0c87c60f31963bcf7',
              width:300, height: 1108, renderTemplateUrl: 'auto'
            }]);
            </script>
      <!-- End of Cxense content widget -->
      </div>
            
      <div ng-controller="relateCtrl" id="relateBox" style="">
      <relate-boxclip></relate-boxclip>
      <relate-boxarticle></relate-boxarticle>          
      <relate-boxpic></relate-boxpic>
      </div>

                        <div class="banner">
            <div id="div-gpt-ad-1530788081041-1" class="float-banner-sidebar">
              <script>
                cX.callQueue.push(['invoke', function() {
                  googletag.cmd.push(function() { googletag.display('div-gpt-ad-1530788081041-1'); });
                }]);    
              </script>
            </div>
          </div>  
                </div>
</div>

<div id="cxense-widget" style="width:1200px; margin:20px auto 15px;">
<strong style="font-family: 'kittithada_roman', Tahoma, sans-serif; font-weight: normal; font-size: 44px; padding: 0 10px 5px;">เรื่องที่คุณอาจสนใจ</strong>    
<!-- Cxense content widget: bottom a/b/c testing -->
<div id="cx_356860333c9bd4bc18c0d8f20c9ec6b1b486d312" style="display:none"></div>
<script type="text/javascript">
    var cX = cX || {}; cX.callQueue = cX.callQueue || [];
    cX.callQueue.push(['insertWidget', {
        widgetId: '356860333c9bd4bc18c0d8f20c9ec6b1b486d312',
        insertBeforeElementId: 'cx_356860333c9bd4bc18c0d8f20c9ec6b1b486d312',
        width:1200, height: 554, renderTemplateUrl: 'auto'
    }]);
</script>
<!-- End of Cxense content widget -->
</div>

<div id="action-quick" data-0="position:absolute; opacity:0; top:-50%;" data-550=" position:fixed; opacity:1; top:-50px;" data-650=" top:0px;">
	<div class="bar">
		<a href="http://www.kapook.com" class="logo"><img src="//my.kapook.com/img-portal/logo-kapook.png" alt=""></a>
		<strong>สลด คลิปนาทีสึนามิ ถล่มอินโดฯ คนร้องบอกให้วิ่งหนีตาย ก่อนคลื่นยักษ์ซัดเละกับตา</strong>

		<div class="share-group2">
			<div class="fb-share fb-btn" data-url="https://hilight.kapook.com/view/178501"><a href="#" class="btnClick"><img src="//my.kapook.com/review/svg/facebook.svg" alt="" /><span class="boxCount"></span></a></div>
			<div class="tw-share tw-btn" data-url="https://hilight.kapook.com/view/178501"><a href="#" class="btnClick"><img src="//my.kapook.com/review/svg/twitter.svg" alt="" /></a></div>
		</div>

		<em>โพสต์เมื่อ 1 ตุลาคม 2561 เวลา 09:22:16</em>
		<em class="setview"> 37,789 อ่าน</em>
	</div>
</div>

      <div class="ads728" style="display: block ; margin: 15px auto; width: 728px;">
      <div id="div-gpt-ad-1530788081041-2">
        <script>
          cX.callQueue.push(['invoke', function() {
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1530788081041-2'); });
          }]);    
        </script>
      </div>
    </div>
      

<!-- taboola -->
<style>
.taboola-widget { width:1200px; margin:0 auto;}
</style>
<div class="taboola-widget">
<div id="taboola-below-article-thumbnails"></div>
</div>
<script type="text/javascript">
            window._taboola = window._taboola || [];
            _taboola.push({
                mode: 'alternating-thumbnails-a',
                container: 'taboola-below-article-thumbnails',
                placement: 'Below Article Thumbnails',
                target_type: 'mix'
            });
</script>
<!-- taboola -->

<div data-0="position:absolute; opacity:0; " data-1200=" position:fixed; opacity:1;" style="cursor: pointer;z-index: 999;">
  <a id="totop" class="icon-arrow-up" onclick="  $('html, body').animate({ scrollTop: 0 }, 'slow');">TOP</a>
</div>



<div class="directory" id="directory-zone">
  <div class="container">
    </div>
</div>

<!--Footer-->
<link rel="stylesheet" type="text/css" media="all" href="https://my.kapook.com/fonts/chatthai/fontface.css">  
<link rel="stylesheet" type="text/css" media="all" href="https://my.kapook.com/css/portal/footer2018.css">
<div class="footer-zone-services">
    <div class="wrapper-sv">

        <div class="services">

                <h2>บริการของเรา (Services & Solutions)</h2>

             <ul class="services-list ">

                    <li><img src="https://my.kapook.com/img-services/ic-sv-01.png"><a href="https://www.kapook.com/services/" target="_blank"><h3>ผลิตเนื้อหา-ลงโฆษณา</h3><span>พื้นที่โฆษณาประชาสัมพันธ์ที่ตอบโจทย์ทางการตลาด เข้าถึงกลุ่มเป้าหมายที่หลากหลาย</span></a></li>

                    <li><img src="https://my.kapook.com/img-services/ic-sv-02.png"><a href="https://www.kapook.com/services/video" target="_blank"><h3>ผลิตวิดีโอโปรโมต</h3><span>รับผลิตรายการโชว์ทางอินเทอร์เน็ตในรูปแบบตอนสั้น ๆ ไปจนถึงซีรีส์ , วิดีโอโปรโมชั่นสินค้าและบริการ</span></a></li>

                    <li><img src="https://my.kapook.com/img-services/ic-sv-03.png"><a href="https://www.kapook.com/services/enterprise-solutions" target="_blank"><h3>พัฒนาระบบองค์กร</h3><span>ครอบคลุมในทุกความต้องการด้านเทคโนโลยี ในยุค Big Data ด้วยทีมงานผู้เชี่ยวชาญ</span></a></li>

                    <li><img src="https://my.kapook.com/img-services/ic-sv-04.png"><a href="https://www.kapook.com/services/socialmedia/" target="_blank"><h3>บริหารโซเชียลมีเดีย</h3><span>บริการรับวางแผนและบริหาร ผลิตเนื้อหาที่หลากหลายบนเครือข่าย Social Media</span></a></li>
              </ul>
              <em><a href="https://www.kapook.com/services/" target="_blank">ดูบริการทั้งหมด คลิกที่นี่<div class="arrow"><span class="ar"></span></div></a></em>
        </div>

               

        <div class="foot-details">
                 <h2>ช่องทางการติดต่อ</h2>

                 <div class="foot-action">  
                    <ul>
                        <li><a href="https://www.kapook.com/services/contact" target="_blank">ติดต่อเรา</a></li>
                        <li><a href="https://www.kapook.com/disclaimer/" target="_blank">Disclaimer</a></li> 
                        <li><a href="http://job.kapook.com/" target="_blank">ร่วมงานกับเรา</a></li>
                        <li><a href="https://www.kapook.com/notice" target="_blank">แจ้งปัญหา</a></li>
                        
                    </ul>   
               </div>

               <div class="official-social2">
                
                     <a href="https://www.facebook.com/kapookdotcom" target="_blank"><img src="https://my.kapook.com/img-services/icon-fb.png"></a>
                     <a href="https://twitter.com/kapookdotcom" target="_blank"><img src="https://my.kapook.com/img-services/icon-tw.png"></a>
                     <a href="https://www.youtube.com/user/kapookdotcom" target="_blank"><img src="https://my.kapook.com/img-services/icon-yt.png"></a>
                     <a href="https://www.instagram.com/kapookdotcom/" target="_blank"><img src="https://my.kapook.com/img-services/icon-ig.png"></a>
                </div>


                 <div class="about">

                          <p>Kapook App ดาวน์โหลดได้เลย</p>
                        <a href="https://itunes.apple.com/th/app/kapook/id562858086?mt=8" target="_blank"><img src="https://www.kapook.com/assets_2017/images/app-store.jpg" class="app-store" alt=""></a>
                       <a href="https://play.google.com/store/apps/details?id=apps.kapook.com&hl=th" target="_blank"><img src="https://www.kapook.com/assets_2017/images/google-play.jpg" class="google-play" alt=""></a>
                </div>


    

        </div>
    </div>
</div>

<!--End : Footer-->


<iframe style="position:absolute; bottom:0; right:0;" width="0" height="0" src="//sp.kapook.com/cms_content/publisher/updateview.php?message=178501||27"></iframe>





<script>
  $(document).ready(function(){
    $(".menu").click(function(){
      $(".listportal2").toggleClass("listopen");
      $(".menu").toggleClass("close");
    });

    $.each($("ul.list-top-menu li"), function(i, v){
      if($(v).attr("catc")==current_cat)
        $(v).addClass("active")
    })

    //----------------------  set view ---------------------------------------------
    // var url_stat  = 'https://cacheportal.kapook.com/haadmin/content/get_info_count/178501/27  '
    // console.log(url_stat)
    // $.ajax({
    //     url: url_stat,
    // }).done(function(data) {
    //     $.each($('.setview'), function(i, v){
    //         $(v).text(data.views.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+' '+'อ่าน')
    //     })
    //     $.each($('.setview_em'), function(i, v){
    //         $(v).text(data.views.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
    //         var em = document.createElement("em")
    //         var t = document.createTextNode("อ่าน")
    //         em.appendChild(t) 
    //         $(v).append(em)
    //     })
    // });
    //------------------------------------------------------------------------------

  });
</script>

<!-- ads for gallery desktop -->


<script src="//my.kapook.com/portal_view/js/swiper.min.js"></script>
<script src="//my.kapook.com/portal_view/js/galleryPlugin-loadmore.js"></script> 

  <script type="text/javascript" src="//my.kapook.com/jquery/head.load.min.js"></script>
  <script type="text/javascript" src="//my.kapook.com/js_emocomment/jquery.easing.1.3.js"></script>
  <script type="text/javascript">
    var CONTENT_ID = 178501;
    var SUBDOMAIN = 27;
    var GET_CATEID = function () {
      return cate = '6';
    }
    var contentType = 1;
    var mytheme = "content";
    var myarr=[];
    myarr.push("//my.kapook.com/jquery/skrollr.min.js");
    myarr.push("//my.kapook.com/jquery/jquery.fbbutton.js");
    myarr.push("//my.kapook.com/jquery/jquery.kp.tracker.js");

    if(mytheme!="review" && mytheme!="clip")
      myarr.push("//my.kapook.com/angular/app/content_relate/js/main_t.js");
      
      myarr.push("//my.kapook.com/jquery/jquery.tagCenter.js");
      head.load(myarr, function(){
        head.load(["//my.kapook.com/jquery/headLoadComplete.js"]);
      })
  </script>

  <script type="text/javascript" src="//my.kapook.com/jquery/jquery.worldjwplayer.js"></script>
  <script type="text/javascript" src="//my.kapook.com/jquery/oembed.js"></script>
  <script type="text/javascript" src="//my.kapook.com/jquery/run_oembed.js"></script>
  <script type="text/javascript" src="https://www.instagram.com/embed.js"></script>

  <!-- Advenue DMP Container - Kapook.com -->
  <script type="text/javascript" charset="UTF-8" src="//my.kapook.com/js_tag/dmp.js"></script>
  <!-- End Advenue DMP Container -->

  <!-- Tag : Show -->
  <script type="text/javascript">
   var url_stag = '//cacheportal.kapook.com/tag/content/tag/178501/hilight/1?jsoncallback=?';
   var url_ctag ='//cacheportal.kapook.com/tag/compound/tag/178501/hilight/1?jsoncallback=?';
 </script>
 <script type="text/javascript" src="//my.kapook.com/jquery/tag.js"></script>


<script>
   //window.addEventListener('load', function () {

        var lastScrollY = 0, ticking = false

            ,floatBanner= {el:document.querySelector(".float-banner-sidebar"), Y: 0,H: 0, flag:false}
            ,contentBOX = document.querySelector(".content")
            ,container = document.querySelector("#container")
            ,contentTricker= {el:document.querySelector(".tag-list-related"), Y: 0,H: 0,FOOTER:0, flag:false}
            ,idiff = contentBOX.getBoundingClientRect().top - container.getBoundingClientRect().top;

            function initData() {
                floatBanner.Y = floatBanner.el.parentNode.getBoundingClientRect().top + window.scrollY;
                floatBanner.H = floatBanner.el.getBoundingClientRect().height;
                contentTricker.H = contentTricker.el.getBoundingClientRect().height;
                contentTricker.Y = contentTricker.el.getBoundingClientRect().top + window.scrollY;
                contentTricker.FOOTER = contentTricker.Y - floatBanner.H;
                floatBanner.flag = false;
                contentTricker.flag = false;
            }
                initData();

            var countTry = 10,
                myInitCheck = setInterval(function(){
                       // console.log(countTry)
                       if(countTry == 0){
                           clearInterval(myInitCheck);
                           return;
                       }
                       initData()
                       countTry --;
                    }, 1000);



        function onScroll() {
            if(!ticking) {
                lastScrollY = window.scrollY;
                requestAnimationFrame(update);
                ticking = true;
            }
        }

        function update() {
            if (lastScrollY +90 >= contentTricker.FOOTER  && contentTricker.flag == false ) {

                floatBanner.el.style.position="absolute";
                //floatBanner.el.style.top = contentTricker.H - floatBanner.H +idiff+"px";
                floatBanner.el.style.top = contentBOX.getBoundingClientRect().height - floatBanner.H +idiff+"px";
                contentTricker.flag = true;

            }else if(lastScrollY +90  < contentTricker.FOOTER   ){

                if(contentTricker.flag == true){
                    floatBanner.el.style.position="";
                    floatBanner.el.style.top="";
                    contentTricker.flag = false;
                }
                if (lastScrollY+80 >= floatBanner.Y  /*&& floatBanner.flag == false */) {
                    //floatBanner.el.parentNode.classList.toggle("wrap-float-banner",true)
                    floatBanner.el.style.position = "fixed";
                    floatBanner.el.style.top = "80px";
                    floatBanner.el.style.zIndex = 9;
                    floatBanner.flag = true;

                }else if(lastScrollY+80 < floatBanner.Y  /*&& floatBanner.flag == true*/){
                    // floatBanner.el.parentNode.classList.toggle("wrap-float-banner",false)
                    floatBanner.el.removeAttribute("style")
                    floatBanner.flag = false;
                }
            }

            ticking = false;
        }

        // Window Scroll
        window.addEventListener("scroll",onScroll, false);
        
        
</script>
      <script type="text/javascript"> // taboola load
        window._taboola = window._taboola || [];
        _taboola.push({flush: true});
    </script>
  
    
</body>
</html>