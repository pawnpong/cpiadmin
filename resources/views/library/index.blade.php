@extends('layouts.master')



@section('content')
<div class="pcoded-content">
        <div class="pcoded-inner-content">   
        
                <div class="main-body">
                        <div class="page-wrapper">
                                @if(session('feedback'))
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong>Success!</strong> {{ session('feedback') }}
                                    </div>
                                @endif
                                @if(session('feedbackdelete'))
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong>Success!</strong> {{ session('feedbackdelete') }}
                                    </div>
                                @endif
                            <!-- Page-header start -->
                            <div class="page-header">
                                <div class="page-header-title">
                                    <h4>library</h4>
                                    <span></span>
                                </div>
                                <div class="page-header-breadcrumb">
                                    <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="{{url('/')}}">
                                                <i class="icofont icofont-home"></i>
                                            </a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="">library</a>
                                       
                                    </ul>
                                </div>
                            </div>


                        <!-- Hover table card start -->
                        <div class="card">
                                <div class="card-header">
                                        <a href="{{route('infographics.create')}}">
                                    <h5><button class="btn btn-info"><i class="ion-plus"></i>library</button></h5>
                                    <span></span></a>
                                    <div class="card-header-right">
                                      
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="dt-responsive table-responsive">
                                        <table id="simpletable" class="table table-striped table-bordered nowrap">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>ลิ้งที่ชื่นชอบ</th>
                                                    <th></th>
                                                
                                                    <th></th>
                                                </tr>
                                            {{-- </thead>
                                            <tbody>
                                                    @php
                                                    $i = 1;
                                                    @endphp
                                                    @foreach($Infographic as $k)
                                                <TR>
                                                <td>{{$i}}</td>
                                                <TD>{{$k->name}}</TD>
                                                <td>{{$k->department}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-inverse waves-effect " data-toggle="modal" data-target="#info{{$k->id}}"><i class="ion-ios-search"></i>View</button>
                                                </td>
                                                <td>{{$k->created_at}}</td>
                                                <td>{{$k->updated_at}}</td>

                                                <td> 
                                                    <a href="{{ route('infographics.edit',$k->id) }}" class="btn   btn-warning waves-effect waves-light"><i class="fa fa-pencil"></i> แก้ไข</a>
                                                    <a href="" class=" btn btn-danger  waves-effect waves-light delBtn" data-id="{{$k->id}}"><i class="fa fa-times" ></i> ลบ</a>
                                                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                                </td> --}}
                                                {{-- <td> 
                                                    {!! Form::open(['id'=>$k->id,'route' => ['infographics.destroy',$k->id],'method'=> 'delete']) !!} {!! Form::close() !!}
                                                   
                                                    <a href="{{ route('infographics.edit',['id' => $k->id]) }}" 
                                                        class="btn  btn-round btn-warning waves-effect waves-light"><i class="ion-edit"></i>แก้ไข
                                                    </a>

                                                    <button type="button" onclick="document.getElementById('{{$k->id}}').submit();"
                                                         class="btn btn-danger btn-round waves-effect waves-light">
                                                         <i class="ion-trash-a"></i>ลบ
                                                    </button>
                                                   
                                                </td> --}}
                                               
                                                
                                                
                                                </TR>
                                        <!-- Modal small-->
                                        {{-- <div class="modal fade" id="info{{$k->id}}" tabindex="-1" role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <img src="{{asset('fileinfographic/'.$k->filename.' ')}}" alt="{{$k->name}}" width="468px" height="368px" class="img img-fluid" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div> --}}
                                        <!-- Modal small-->

                                        

                                                    {{-- @endforeach --}}
                                            </tbody>  
                                        </table>
                                    </div>
                                </div>
                            </div>

                                        <!-- Zero config.table end -->

                            <!-- Hover table card end -->
                            {{-- <img src="{{asset('fileinfographic/'.$k->filename.' ')}}" alt="{{$k->name}}"> --}}

                         
                        
                          



                                
                           









        </div>
</div>



@endsection
@section('jsfooter')
<script>
		// window.setTimeout(function(){
        //     $('.alert').fadeTo(500,0).slideUp(500,function(){
        //         $(this).remove();
        //     })
        // }, 3000);

$(document).on('click', '.delBtn', function (e) {
            
            e.preventDefault();
            var id = $(this).data('id');
            // alert(id);
            swal({
                title: "คุณต้องการลบ?",
                text: "หากคุณทำการลบข้อมูล จะไม่สามารถทำการกู้คืนได้อีก",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    // alert(id);
                    $.ajax({
                            method: "DELETE",
                            url: '{{ url('admin/infographics')}}/'+id,
                            data: {ids:id,_token: $('#_token').val(),},
                            success: function (data) {
                                if(data.success =="1"){
                                    swal("ทำการลบข้อมูลสำเร็จ", {
                                        icon: "success",
                                    }).then(()=>{ location.reload(); });     
                                }else{
                                    swal({
                                        title:    "พบข้อผิดพลาด",
                                        text: "กรุณาติดต่อผู้ดูแลระบบ",
                                        icon: "warning",
                                        dangerMode: true,
        
                                    });
                                }
                            }         
                    });
                } else {
                    swal("ยกเลิกการลบข้อมูล");
                }
            });
        });
    </script>
@endsection