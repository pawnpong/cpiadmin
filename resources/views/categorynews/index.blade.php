@extends('layouts.master')
@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">    
        <div class="main-body">
            <div class="page-wrapper">
                    @if(session('feedback'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong>Success!</strong> {{ session('feedback') }}
                        </div>
                    @endif
                <!-- Page-header start -->
                <div class="page-header">
                    <div class="page-header-title">
                        <h4>หมวดหมู่ข่าวประชาสัมพันธ์</h4>
                        <span></span>
                    </div>
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{url('/admin')}}">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="">หมวดหมู่ข่าวประชาสัมพันธ์</a>
                        </ul>
                    </div>
                </div>
                <!-- Hover table card start -->
                <div class="card">
                    <div class="card-header">
                            <a href="{{route('admin.categorynews.create')}}">
                        <h5><button class="btn btn-info btn-md"><i class="ion-plus"></i>เพิ่มข้อมูล</button></h5>
                        <span></span></a>
                        <div class="card-header-right">
                            
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="dt-responsive table-responsive">
                            <table id="simpletable" class="table table-striped table-bordered nowrap">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th width="75%">ชื่อ</th> 
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i = 1; @endphp
                                    @foreach($categorynews as $row)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$row->name}}</td>
                                        <td> 
                                            <a href="{{ route('admin.categorynews.edit',$row->id) }}" class="btn   btn-warning waves-effect waves-light"><i class="fa fa-pencil"></i> แก้ไข</a>
                                            <a href="" class=" btn btn-danger  waves-effect waves-light delBtn" data-id="{{$row->id}}" data-refrow="{{$row->news->count()}}"><i class="fa fa-times" ></i> ลบ</a>
                                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                        </td>
                                    </tr>
                                    @php $i++; @endphp
                                    @endforeach
                                </tbody>  
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('jsfooter')
<script>
    $(document).ready(function() {
            document.getElementById('main_news').classList.add('active');
            document.getElementById('main_news').classList.add('pcoded-trigger');
            document.getElementById('categorynews_active').classList.add('active');
    });
		window.setTimeout(function(){
            $('.alert').fadeTo(500,0).slideUp(500,function(){
                $(this).remove();
            })
        }, 3000);

        $(document).on('click', '.delBtn', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var refrow = $(this).data('refrow');
            swal({
                title: "คุณต้องการลบ?",
                text: "มีข้อมูลข่าวประชาสัมพันธ์ที่เกี่ยวข้องกับประเภทนี้อยู่ " + refrow + " รายการ หากคุณทำการลบข้อมูล จะไม่สามารถทำการกู้คืนได้อีก",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        method: "DELETE",
                        url: '{{ url('admin/categorynews')}}/'+id,
                        data: {ids:id,_token: $('#_token').val(),},
                        success: function (data) {
                            if(data.success =="1"){
                                swal("ทำการลบข้อมูลสำเร็จ", {
                                    icon: "success",
                                }).then(()=>{ location.reload(); });     
                            }else{
                                swal({
                                    title:"พบข้อผิดพลาด",
                                    text: "กรุณาติดต่อผู้ดูแลระบบ",
                                    icon: "warning",
                                    dangerMode: true,
                                });
                            }
                        }         
                    });
                } else {
                    //swal("ยกเลิกการลบข้อมูล");
                }
            });
        });
    </script>
@endsection