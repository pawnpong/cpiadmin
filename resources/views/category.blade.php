@extends('layouts.frontend')
     @section('content')
<!--breadcrumbs-->
<section id="breadcrumb">
    <div class="row">
        <div class="large-12 columns">
            <nav aria-label="You are here:" role="navigation">
                <ul class="breadcrumbs">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="{{url('/')}}">หน้าหลัก</a>
                    </li>
                    <li>
                        <span class="show-for-sr">Current: </span> วีดีโอ
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</section>
<!--end breadcrumbs-->
<!-- Premium Videos -->
<br>
<!-- End Premium Videos -->
<section class="category-content">
    <div class="row">
        <!-- left side content area -->
        <div class="large-8 columns">
            <section class="content content-with-sidebar">
                <!-- newest video -->
                <div class="main-heading removeMargin">
                    <div class="row secBg padding-14 removeBorderBottom">
                        <div class="medium-8 small-8 columns">
                            <div class="head-title">
                                <i class="fa fa-film"></i>
                                <h4>
                                    @if($id_category == 0) 
                                    {{$category_type}}
                                    @else
                                    {{$category_type->name}} 
                                    @endif
                                </h4>
                            </div>
                        </div>
                        <div class="medium-4 small-4 columns">
                            <ul class="tabs text-right pull-right" data-tabs id="newVideos">
                                {{--
                                <li class="tabs-title is-active"><a href="#new-all">all</a></li>
                                <li class="tabs-title"><a href="#new-hd">HD</a></li> --}}
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row secBg">
                    <div class="large-12 columns">
                        <div class="row column head-text clearfix">
                            <p class="pull-left">
                                @if($id_category == 0)
                                All Videos :
                                <span> {{$youtube->count()}} Videos posted</span></p>
                                @else All Videos :
                                <span> {{$category->count()}} Videos posted</span></p>
                                @endif
                            <div class="grid-system pull-right show-for-large">
                                <a class="secondary-button current grid-medium" href="#"><i class="fa fa-th-large"></i></a>
                                <a class="secondary-button list" href="#"><i class="fa fa-th-list"></i></a>
                            </div>
                        </div>
                        <div class="tabs-content" data-tabs-content="newVideos">
                            <div class="tabs-panel is-active" id="new-all">
                                <div class="row list-group">
                                    @if($id_category == 0)
                                    @foreach($category as $item)
                                    <div class="item large-4 columns end grid-medium">
                                        <div class="post thumb-border">
                                            <div class="post-thumb">
                                                <?php
                                                                $data = explode("=",$item->link);
                                                                $data[1];
                                                                ?>
                                                <embed width="370" height="220" src="https://www.youtube.com/embed/{{$data[1]}}">
                                                <a href="#" onclick="updateview({{$item->id}});" class="hover-posts">
                                                    <span><i class="fa fa-play"></i>Watch Video</span>
                                                </a>
                                                <div class="video-stats clearfix">
                                                    <div class="thumb-stats pull-left">
                                                        <i class="fa fa-heart"></i>
                                                        <span>{{$item->view}}</span>
                                                    </div>
                                                 
                                                </div>
                                            </div>
                                            <div class="post-des">
                                                <h6><a href="single-video-v2.html">{{$item->title}}</a></h6>
                                                <div class="post-stats clearfix">
                                                    <p class="pull-left">
                                                        <i class="fa fa-clock-o"></i>
                                                        <small>{{DateTime::createFromFormat('Y-m-d H:i:s',$item->created_at)->format('d-m-Y')}}</small>
                                                    </p>
                                                    <p class="pull-left">
                                                        <i class="fa fa-eye"></i>
                                                        <small>{{$item->view}}</small>
                                                    </p>
                                                </div>
                                                <div class="post-summary">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach 
                                    @elseif($category->count() == 0) 
                                    @foreach(range(1, 2) as $i)
                                    <div class="item large-4 columns end grid-medium">
                                        <div class="post thumb-border">
                                            <div class="post-thumb">
                                                <img src="http://placehold.it/370x220" alt="landing">
                                                <a href="#" class="hover-posts">
                                                    <span><i class="fa fa-play"></i>NO VIDEO</span>
                                                </a>
                                            </div>
                                            <div class="post-des">
                                                <h6><a href="#">ไม่มีวิดีโอ</a></h6>
                                                <div class="post-stats clearfix">
                                                </div>
                                                <div class="post-summary">
                                                    <p> </p>
                                                </div>
                                                <div class="post-button">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach 
                                    @else
                                    @foreach($category as $item)
                                    <div class="item large-4 columns end grid-medium">
                                        <div class="post thumb-border">
                                            <div class="post-thumb">
                                                <?php
                                                            $data = explode("=",$item->link);
                                                            $data[1];
                                                            ?>
                                                <embed width="370" height="220" src="https://www.youtube.com/embed/{{$data[1]}}">
                                                <a href="#" onclick="updateview({{$item->id}});" class="hover-posts">
                                                    <span><i class="fa fa-play"></i>Watch Video</span>
                                                </a>
                                                <div class="video-stats clearfix">
                                                    <div class="thumb-stats pull-left">
                                                        <i class="fa fa-heart"></i>
                                                        <small>{{$item->view}}</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="post-des">
                                                <h6><a href="single-video-v2.html">{{$item->title}}</a></h6>
                                                <div class="post-stats clearfix">
                                                    <p class="pull-left">
                                                        <i class="fa fa-clock-o"></i>
                                                        <small>{{DateTime::createFromFormat('Y-m-d H:i:s',$item->created_at)->format('d-m-Y')}}</small>
                                                    </p>
                                                    <p class="pull-left">
                                                        <i class="fa fa-eye"></i>
                                                        <small>{{$item->view}}</small>
                                                    </p>
                                                </div>
                                                <div class="post-summary">   
                                                </div>
                                                <div class="post-button">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach 
                                    @endif
                                </div>
                            </div>
                        </div>
                        @include('pagination.default', ['paginator' => $category])
                </div>
            </section>
            <!-- ad Section -->
            <!-- End ad Section -->
        </div><!-- end left side content area -->
        <!-- sidebar -->
        <div class="large-4 columns">
            <aside class="secBg sidebar">
                <div class="row">
                    <!-- categories -->
                    <div class="large-12 medium-7 medium-centered columns">
                        <div class="widgetBox">
                            <div class="widgetTitle">
                                <h5>categories</h5>
                            </div>
                            <div class="widgetContent">
                                <ul class="accordion" data-accordion>
                                    <li class="accordion-item is-active" data-accordion-item>
                                        <a href="#" class="accordion-title">หมวดหมู่วีดีโอ</a>
                                        <div class="accordion-content" data-tab-content>
                                            <ul>
                                                @php $youtube_count = \App\Video::where('is_public', 1)->count(); @endphp
                                                <li
                                                    class="clearfix">
                                                    <i class="fa fa-play-circle-o"></i>
                                                    <a href="{{ route('Category',['id' => 0 ]) }}">ทั้งหมด <span>({{$youtube_count}})</span></a>
                                                </li>
                                                @foreach($category_select as $item) @php $youtube_count = \App\Video::where('category_id','=',$item->id)->where('is_public',
                                                1)->count(); @endphp
                                                <li class="clearfix">
                                                    <i class="fa fa-play-circle-o"></i>
                                                    <a href="{{ route('Category',['id' => $item->id ]) }}">{{$item->name}}
                                                        <span>({{$youtube_count}})</span></a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- social Fans Widget -->
                    @include('layouts.social')
                  <!-- End social Fans Widget -->
                </div>
            </aside>
        </div><!-- end sidebar -->
    </div>
</section><!-- End Category Content-->
<!-- footer -->

</div>
<!--end off canvas content-->
</div>
<!--end off canvas wrapper inner-->
</div>
<!--end off canvas wrapper-->
<!-- script files -->
@endsection

@section('footer')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
    function updateview($id) {
        //alert($id);
        var token = "<?php echo csrf_token(); ?>";
        $.ajax({
            url: '{{ route("ajaxupdateview") }}',
            datatType: 'json',
            type: 'POST',
            data: {
                '_token': token,
                'id': $id,
            },
            success: function (response) {
                //alert(response.id);
                //redirect to Playyoutube
                window.location = response.url

            }
        });//end ajax
    }
</script>
@endsection
