@extends('layouts.master')

@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">                  
        <div class="main-body">
            <div class="page-wrapper">
                <!-- Page-header start -->
                <div class="page-header">
                    <div class="page-header-title">
                        <h4>เพิ่มข่าวประชาสัมพันธ์</h4>
                        <span></span>
                    </div>
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{url('/')}}">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{route('admin.newstopic.index')}}">ข่าวประชาสัมพันธ์</a>
                            <li class="breadcrumb-item"><a href="#">เพิ่มข่าวประชาสัมพันธ์</a>  
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5>เพิ่มข้อมูล</h5>
                    </div>
                    <div class="card-block">
                        {!! Form::open(array('route' => 'admin.newstopic.store' , 'method' => 'post' , 'files' => true )) !!}
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">หมวดหมู่ข่าวประชาสัมพันธ์</label>
                            <div class="col-sm-10">
                                {!! Form::select('CategoryNews_id', App\CategoryNewsTable::where('is_public' , 1)->orderby('name','asc')->pluck('name','id'),
                                null, ['placeholder' => 'หมวดหมู่ข่าวประชาสัมพันธ์','class' => 'form-control']); !!} 
                                @if ($errors->has('CategoryNews_id'))
                                <span class="messages">
                                    <p class="text-danger error">{{ $errors->first('CategoryNews_id')}}</p>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="headlines" class="col-sm-2 col-form-label">รูปพาดหัวข่าว</label>
                            <div class="col-10">
                                {!! Form::file('headlines' , ['id'=>'imgInp','class' => 'form-control','multiple'=>'multiple',
                                'accept'=>'image/jpg, image/jpeg, image/png,image/gif','style'=>'display:none;cursor:pointer;']); !!}
                            
                                <div class="col-lg-3 col-md-6 col-sm-12">
                                        <a onclick="$('#imgInp').click();">      
                                        <img id="blah" style="cursor:pointer;" src="{{asset('images/nopic.png')}}" alt="รูปพาดหัวข่าว"  
                                        class="img-fluid" /></a>
                                </div> 
                                @if ($errors->has('headlines'))
                                    <span class="messages"><p class="text-danger error">{{ $errors->first('headlines')}}</p></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">หัวข้อข่าว</label>
                            <div class="col-sm-10">
                                {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'กรุณากรอกหัวข้อข่าว']);!!}
                        
                                @if ($errors->has('title'))
                                <span class="messages"><p class="text-danger error">{{ $errors->first('title')}}</p></span>
                                @endif
                            </div>  
                        </div>

                        <div class="form-group row">
                            <label for="exampleTextarea" class="col-sm-2 col-form-label">เนื้อหาข่าว</label>
                            <div class="col-10">
                                {!! Form::textarea('detail',null,['class'=>'form-control','placeholder'=>'กรุณากรอกเนื้อหาข่าว','rows'=>'15']);!!}
                                @if ($errors->has('detail'))
                                <span class="messages"><p class="text-danger error">{{ $errors->first('detail')}}</p></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">ผู้ประกาศข่าว</label>
                            <div class="col-sm-10">
                                {!! Form::text('correspondent',null,['class'=>'form-control','placeholder'=>'กรุณากรอกผู้ประกาศข่าว']);!!}
                                @if ($errors->has('correspondent'))
                                <span class="messages"><p class="text-danger error">{{ $errors->first('correspondent')}}</p></span>
                                @endif
                            </div>  
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">ผู้เรียบเรียง</label>
                            <div class="col-sm-10">
                                {!! Form::text('composer',null,['class'=>'form-control','placeholder'=>'กรุณากรอกผู้เรียบเรียง']);!!}
                                @if ($errors->has('composer'))
                                <span class="messages"><p class="text-danger error">{{ $errors->first('composer')}}</p></span>
                                @endif
                            </div>  
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">แหล่งที่มา</label>
                            <div class="col-sm-10">
                                {!! Form::text('source',null,['class'=>'form-control','placeholder'=>'กรุณากรอกแหล่งที่มา']);!!}
                                @if ($errors->has('source'))
                                <span class="messages"><p class="text-danger error">{{ $errors->first('source')}}</p></span>
                                @endif
                            </div>  
                        </div>

                        <div class="form-group row">
                            <label for="exampleTextarea" class="col-sm-2 col-form-label">วันที่เริ่ม</label>
                            <div class="col-10">
                                    {!! Form::date('startdate',null,['class'=>'form-control']);!!}
                                @if ($errors->has('startdate'))
                                <span class="messages"><p class="text-danger error">{{ $errors->first('startdate')}}</p></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="exampleTextarea" class="col-sm-2 col-form-label">วันที่สิ้นสุด</label>
                            <div class="col-10">
                                    {!! Form::date('enddate',null,['class'=>'form-control']);!!}
                                @if ($errors->has('enddate'))
                                <span class="messages"><p class="text-danger error">{{ $errors->first('enddate')}}</p></span>
                                @endif
                            </div>
                        </div>

                        <div class="row form-group{{ $errors->has('filenewsdetail') ? ' has-error' : '' }}">
                            <label for="filenewsdetail" class="col-sm-2 control-label">รูปภาพรายละเอียดข่าว</label>
                            <div class="col-10">
                                {!! Form::file('filenewsdetail[]' , ['class' => 'form-control','multiple'=>'multiple','accept'=>'image/jpg, image/jpeg, image/png ,image/gif']); !!}
                                @if ($errors->has('filenewsdetail'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('filenewsdetail') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2"></label>
                            <div class="col-sm-10 edit-right ">
                                <button data-key="submit" type="submit" class="btn btn-primary" > <i class="ion-archive"></i>บันทึก</button>
                                <a href="{{ route('admin.newstopic.index') }}" class="btn btn-default">
                                    <i class="ion-reply"></i>ยกเลิก
                                </a>
                            </div>
                        </div> 
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('cssheader')
<!-- -->
@endsection

@section('jsfooter')
    <script>
           $(document).ready(function() {
            document.getElementById('main_news').classList.add('active');
            document.getElementById('main_news').classList.add('pcoded-trigger');
            document.getElementById('news_active').classList.add('active');
    });
        //image headline Show
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imgInp").change(function() {readURL(this);});
    </script>
@endsection
