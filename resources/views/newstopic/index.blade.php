@extends('layouts.master')

@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">   
        <div class="main-body">
            <div class="page-wrapper">
                @if(session('feedback'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Success!</strong> {{ session('feedback') }}
                    </div>
                @endif
                @if(session('feedbackdelete'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Success!</strong> {{ session('feedbackdelete') }}
                    </div>
                @endif
                <!-- Page-header start -->
                <div class="page-header">
                    <div class="page-header-title">
                        <h4>ข่าวประชาสัมพันธ์</h4>
                        <span></span>
                    </div>
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{url('/')}}">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="">ข่าวประชาสัมพันธ์</a>
                            
                        </ul>
                    </div>
                </div>
                <!-- Hover table card start -->
                <div class="card">
                    <div class="card-header">
                            <a href="{{route('admin.newstopic.create')}}">
                        <h5><button class="btn btn-info"><i class="ion-plus"></i>เพิ่มข้อมูล</button></h5>
                        <span></span></a>
                        <div class="card-header-right">
                            
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="dt-responsive table-responsive">
                            <table id="simpletable" class="table table-striped table-bordered nowrap">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>หัวข้อข่าว</th>
                                        <th>เนื้อหา</th>
                                        <th>รูปพาดหัวข่าว</th>
                                        <th>ผู้ประกาศข่าว</th>
                                        <th>ผู้เรียบเรียง</th>
                                        <th>แหล่งที่มา</th>
                                        <th>วันที่เริ่ม</th>
                                        <th>วันที่สิ้นสุด</th>
                                        <th>แก้ไข/ลบ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i = 1; @endphp
                                    @foreach($newstopic as $row) 
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td><p style="white-space:pre-wrap; word-wrap:break-word">{{$row->title}}</p></td>
                                        <td>
                                            <button type="button" class="btn btn-inverse waves-effect " data-toggle="modal" data-target="#detail{{$row->id}}"><i class="ion-ios-search"></i></button>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-inverse waves-effect " data-toggle="modal" data-target="#Modal-tab{{$row->id}}"><i class="ion-ios-search"></i></button>
                                        </td>
                                        <td>{{$row->correspondent}}</td>
                                        <td>{{$row->composer}}</td>
                                        <td>{{$row->source}}</td>
                                        <td>{{DateTime::createFromFormat('Y-m-d', $row->startdate)->format('d-m-Y') }}</td>
                                        <td>{{DateTime::createFromFormat('Y-m-d', $row->enddate)->format('d-m-Y') }}</td>
                                        <td> 
                                            <a href="{{ route('admin.newstopic.edit',$row->id) }}" class="btn  btn-warning waves-effect waves-light"><i class="fa fa-pencil"></i> แก้ไข</a>
                                            <a href="" class=" btn btn-danger  waves-effect waves-light delBtn" data-id="{{$row->id}}"><i class="fa fa-times" ></i> ลบ</a>
                                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                        </td>   
                                    </tr>
                                        <!-- Modal headlines-->
                                        <div class="modal fade" id="info{{$row->id}}" tabindex="-1" role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">{{$row->title}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true" style="font-size: 1.50em;cursor: pointer;"><i class="ion-close-circled"></i></span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <img src="{{asset('news/'.$row->headlines.' ')}}" alt="{{$row->title}}" width="468px" height="368px" class="img img-fluid" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Modal headlines-->

                                        <!-- Modal detail-->
                                        <div class="modal fade" id="detail{{$row->id}}" tabindex="-1" role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">{{$row->title}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true" style="font-size: 1.50em;cursor: pointer;"><i class="ion-close-circled"></i></span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                       @php echo $row->detail @endphp
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Modal detail-->

                                        <div class="modal fade modal-flex" id="Modal-tab{{$row->id}}" tabindex="-1" role="dialog">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                            <h5 class="modal-title"></h5>
                                                            <button style="cursor: pointer;" type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <!-- Tabination card start -->
                                                        <div class="col-md-12">
                                                                <div class=" tab-card">
                                                                    <ul class="nav nav-tabs md-tabs" role="tablist">
                                                                        <li class="nav-item">
                                                                            <a class="nav-link active" data-toggle="tab" href="#headlines{{$row->id}}" role="tab">รูปพาดหัวข่าว</a>
                                                                            <div class="slide"></div>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" data-toggle="tab" href="#detail_images{{$row->id}}" role="tab">รูปรายละเอียด</a>
                                                                            <div class="slide"></div>
                                                                        </li>
                                                                    </ul>
                                                                    <div class="tab-content">
                                                                        <div class="tab-pane active" id="headlines{{$row->id}}" role="tabpanel">
                                                                            <div class="card-block">
                                                                                <h6>
                                                                                    <b class="text-info">รูปพาดหัวข่าว</b>
                                                                                </h6>
                                                                                <div class="mail-body-content">
                                                                                        <div class="col-lg-12 col-sm-12">
                                                                                                <div class="thumbnail">
                                                                                                    <div class="thumb">
                                                                                        <img src="{{asset('news/'.$row->headlines.' ')}}" alt="{{$row->title}}" class="img img-fluid" />
                                                                                    </div>
                                                                                                </div>
                                                                                        </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="tab-pane" id="detail_images{{$row->id}}" role="tabpanel">
                                                                            <!-- <div class="email-card p-0"> -->
                                                                            <div class="card-block">
                                                                                <h6>
                                                                                    <b class="text-info">รูปรายละเอียด</b>
                                                                                </h6>
                                                                                <div class="mail-body-content">
                                                                                    <div class="card-block">
                                                                                        <div class="row">
                                                                                            @php
                                                                                                $images_detail = \App\FileNewsDetail::where('news_id','=',$row->id)->get();
                                                                                            @endphp
                                                                                            @foreach($images_detail as $row2)
                                                                                            <div class="col-lg-4 col-sm-6">
                                                                                                <div class="thumbnail">
                                                                                                    <div class="thumb">
                                                                                                        <a href="{{asset('newsdetail/'.$row2->filename.' ')}}" target="_blank" data-lightbox="1" data-title="My caption 1">
                                                                                                            <img src="{{asset('newsdetail/'.$row2->filename.' ')}}" style="height:150px;width:225px;" alt="{{$row->title}}" class="img-fluid img-thumbnail">
                                                                                                        </a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            @endforeach
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            <!-- </div> -->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <!-- Tabination card end -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @php $i++; @endphp
                                    @endforeach
                                </tbody>  
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('jsfooter')
<script>
    $(document).ready(function() {
            document.getElementById('main_news').classList.add('active');
            document.getElementById('main_news').classList.add('pcoded-trigger');
            document.getElementById('news_active').classList.add('active');
    });

		window.setTimeout(function(){
            $('.alert').fadeTo(500,0).slideUp(500,function(){
                $(this).remove();
            })
        }, 3000);

$(document).on('click', '.delBtn', function (e) {
            
            e.preventDefault();
            var id = $(this).data('id');
            swal({
                title: "คุณต้องการลบ?",
                text: "หากคุณทำการลบข้อมูล จะไม่สามารถทำการกู้คืนได้อีก",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        method: "DELETE",
                        url: '{{ url('admin/newstopic')}}/'+id,
                        data: {ids:id,_token: $('#_token').val(),},
                        success: function (data) {
                            if(data.success =="1"){
                                swal("ทำการลบข้อมูลสำเร็จ", {
                                    icon: "success",
                                }).then(()=>{ location.reload(); });     
                            }else{
                                swal({
                                    title:"พบข้อผิดพลาด",
                                    text: "กรุณาติดต่อผู้ดูแลระบบ",
                                    icon: "warning",
                                    dangerMode: true,
                                });
                            }
                        }         
                    });
                } else {
                    swal("ยกเลิกการลบข้อมูล");
                }
            });
        });
    </script>
@endsection







