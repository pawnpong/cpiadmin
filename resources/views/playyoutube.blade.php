@extends('layouts.frontend')

@section('meta')
    <meta property="og:title" content="{{$playyoutube->title}}">
    <meta property="og:image" content="">
    <meta property="og:description" content="{{$playyoutube->title}}">
    <meta property="og:url" content="{{URL::current()}}">
@endsection

    @section('content')
            <!--breadcrumbs-->
            <section id="breadcrumb" class="breadcrumb-video-2">
                <div class="row">
                    <div class="large-12 columns">
                        <nav aria-label="You are here:" role="navigation">
                            <ul class="breadcrumbs">
                            <li><i class="fa fa-home"></i><a href="{{url('/')}}">หน้าหลัก</a></li>
                                <li><a href="">เล่นวีดีโอ</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </section><!--end breadcrumbs-->
            <div class="row">
                <!-- left side content area -->
                <div class="large-8 columns">
                    <!--single inner video-->
                    <section class="inner-video">
                        <div class="row secBg">
                            <div class="large-12 columns inner-flex-video">
                                <div class="flex-video widescreen">
                                   
                                    <?php
                                    $data = explode("=",$playyoutube->link);
                                    $data[1];
                                    ?>  
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/{{$data[1]}}" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- single post stats -->
                    <section class="SinglePostStats">
                        <!-- newest video -->
                        <div class="row secBg">
                            <div class="large-12 columns">
                                <div class="media-object stack-for-small">
                                    <div class="media-object-section object-second">
                                        <div class="author-des clearfix">
                                            <div class="post-title">
                                                <h4>{{$playyoutube->title}}</h4>   
                                               
    
                                                <p>
                                                <span><i class="fa fa-clock-o"></i> <span>{{DateTime::createFromFormat('Y-m-d H:i:s',$playyoutube->created_at)->format('d-m-Y') }}</span></span>
                                                    <span><i class="fa fa-eye"></i>{{$playyoutube->view}}</span>
                                                </p><br>
                                                    
                                                @include('layouts.sharesocial')     
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section><!-- End single post stats -->
                    <!-- single post description -->
                    <section class="singlePostDescription">
                        <div class="row secBg">
                            <div class="large-12 columns">
                                <div class="heading">
                                    <h5>Description</h5>
                                </div>
                                <div class="description">
                               <?php echo $playyoutube->description; ?>
                                </div>
                            </div>
                        </div>
                    </section><!-- End single post description -->
                    <section class="content content-with-sidebar">
                        <!-- newest video -->
                        <div class="main-heading removeMargin">
                            <div class="row secBg padding-14 removeBorderBottom">
                                <div class="medium-8 small-8 columns">
                                    <div class="head-title">
                                        <i class="fa fa-film"></i>
                                        <h4>
                                            ทั้งหมด
                                        </h4>
                                    </div>
                                </div>
                                <div class="medium-4 small-4 columns">
                                    <ul class="tabs text-right pull-right" data-tabs id="newVideos">
                                        {{-- <li class="tabs-title is-active"><a href="#new-all">all</a></li>
                                        <li class="tabs-title"><a href="#new-hd">HD</a></li> --}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row secBg">
                            <div class="large-12 columns">
                                <div class="row column head-text clearfix">
                                    <p class="pull-left">All Videos : <span>{{$youtube->count()}} Videos Posts</span></p>
                                    <div class="grid-system pull-right show-for-large">
                                        {{-- <a class="secondary-button grid-default" href="#"><i class="fa fa-th"></i></a> --}}
                                        <a class="secondary-button current grid-medium" href="#"><i class="fa fa-th-large"></i></a>
                                        <a class="secondary-button list" href="#"><i class="fa fa-th-list"></i></a>
                                    </div>
                                </div>
                                <div class="tabs-content" data-tabs-content="newVideos">
                                    <div class="tabs-panel is-active" id="new-all">
                                        <div class="row list-group">   
                                                @foreach($youtube as $item)
                                            <div class="item large-4 columns end grid-medium">
                                                <div class="post thumb-border">
                                                    <div class="post-thumb">    
                                                            <?php
                                                            $data = explode("=",$item->link);
                                                            $data[1];
                                                            ?>  
                                                            <embed width="370" height="220"
                                                            src="https://www.youtube.com/embed/{{$data[1]}}" allowfullscreen>
                                                         <a href="#" onclick="updateview({{$item->id}});" class="hover-posts">
                                                            <span><i class="fa fa-play"></i>Watch Video</span>
                                                        </a>
                                                        <div class="video-stats clearfix">
                                                            {{-- <div class="thumb-stats pull-left">
                                                                <h6>HD</h6>
                                                            </div> --}}
                                                            <div class="thumb-stats pull-left">
                                                                <i class="fa fa-heart"></i>
                                                                <span>{{$item->view}}</span>
                                                            </div>
                                                            {{-- <div class="thumb-stats pull-right">
                                                                <span>05:56</span>
                                                            </div> --}}
                                                        </div>
                                                    </div>
                                                    <div class="post-des">
                                                        <h6><a href="single-video-v2.html">{{$item->title}}</a></h6>
                                                        <div class="post-stats clearfix">
                                                            <p class="pull-left">
                                                                <i class="fa fa-clock-o"></i>
                                                                <small>{{DateTime::createFromFormat('Y-m-d H:i:s',$item->created_at)->format('d-m-Y') }}</small>
                                                            </p>
                                                            <p class="pull-left">
                                                                <i class="fa fa-eye"></i>
                                                                <small>{{$item->view}}</small>
                                                            </p>
                                                        </div>
                                                        <div class="post-summary">
                                                        </div>
                                                        <div class="post-button">  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                @include('pagination.default', ['paginator' => $youtube])
                        </div>
                    </section>
                </div><!-- end left side content area -->
                <!-- sidebar -->
                <div class="large-4 columns">
                    <aside class="secBg sidebar">
                        <div class="row">
                            <!-- most view Widget -->
                            <div class="large-12 medium-7 medium-centered columns">
                                <div class="widgetBox">
                                    <div class="widgetTitle">
                                        <h5>Most View Videos</h5>
                                    </div>
                                    <div class="widgetContent">
                                    @foreach($mostvideos as $mostvideo)
                                        <div class="video-box thumb-border">
                                            <div class="video-img-thumb">
                                                    <?php
                                                    $data = explode("=",$mostvideo->link);
                                                    $data[1];
                                                    ?>  
                                                    <embed width="300" height="190"
                                                    src="https://www.youtube.com/embed/{{$data[1]}}">
                                                {{-- <a href="#" class="hover-posts"> --}}
                                                <a href="{{ route('PlayYoutube',['id' => $mostvideo->id ]) }}" class="hover-posts">
                                                    <span><i class="fa fa-play"></i>Watch Video</span>
                                                </a>
                                            </div>
                                            <div class="video-box-content">
                                                <h6><a href="#">{{$mostvideo->title}}</a></h6>
                                                <p>
                                                    <span><i class="fa fa-user"></i><a href="#">{{$mostvideo->create_by}}</a></span>
                                                    <span><i class="fa fa-clock-o"></i>{{DateTime::createFromFormat('Y-m-d H:i:s',$mostvideo->created_at)->format('d-m-Y') }}</span>
                                                    <span><i class="fa fa-eye"></i>{{$mostvideo->view}}</span>   
                                                </p>
                                            </div>
                                        </div>
                                    @endforeach
                                </div></div>
                            </div><!-- end most view Widget -->
                            <!-- social Fans Widget -->
                            @include('layouts.social')
                    </aside>
                </div><!-- end sidebar -->
            </div>
          
        </div><!--end off canvas content-->
    </div><!--end off canvas wrapper inner-->
</div><!--end off canvas wrapper-->
<!-- script files -->
@endsection

@section('footer')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
{{-- @section('footer') --}}
    <script>
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
        function updateview($id){
            //alert($id);
            var token="<?php echo csrf_token(); ?>";
            $.ajax({
                url: '{{ route("ajaxupdateview") }}',
                datatType : 'json',
                type: 'POST',
                data: {
                    '_token':token,
                    'id': $id,
                },
                success:function(response) {
                    //alert(response.id);
                    //redirect to Playyoutube
                    window.location = response.url
    
                }
            });//end ajax
        }
    </script>
@endsection