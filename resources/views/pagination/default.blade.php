@if ($paginator->lastPage() > 1)
<div class="pagination">
      @if($paginator->currentPage() == 1)
      <a class="prev page-numbers {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">« Previous</a>
      @else
    <a href="{{ $paginator->url(1) }}"  class="prev page-numbers {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">« Previous</a>
      @endif
    @for ($i = 1; $i <= $paginator->lastPage(); $i++)
        @if ($paginator->currentPage() == $i)

            <span class="page-numbers current">{{ $i }}</span>
            
        @else

            <a href="{{ $paginator->url($i) }}" class="page-numbers">{{ $i }}</a>

        @endif
    @endfor

    <a href="{{ ($paginator->currentPage() == $paginator->lastPage()) ? "#" : $paginator->url($paginator->currentPage()+1) }}" class="next page-numbers">Next »</a></div>


</div>
@endif

