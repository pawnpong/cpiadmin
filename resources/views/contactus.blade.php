@extends('layouts.frontend')
@section('content')
<section id="breadcrumb" class="breadcrumb-video-2">
        <div class="row">
            <div class="large-12 columns">
                <nav aria-label="You are here:" role="navigation">
                    <ul class="breadcrumbs">
                    <li><i class="fa fa-home"></i><a href="{{url('/')}}">หน้าหลัก</a></li>
                        <li><a href="">ติอต่อเรา</a></li>
                        
                    </ul>
                </nav>
            </div>
        </div>
    </section>
    <section class="category-content">
        <div class="row">
            <!-- left side content area -->
            <div class="large-8 columns">
                <section class="content content-with-sidebar">
                    <!-- newest video -->
                    <div class="main-heading removeMargin">
                        <div class="row secBg padding-14 removeBorderBottom">
                            <div class="medium-8 small-8 columns">
                                <div class="head-title">
                                    <i class="fa fa-user"></i>
                                    <h4>ติดต่อเรา </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row secBg">
                        <div class="large-12 columns">
                            <article class="page-content">
                                <center>
                                    <a href="{{asset('frontend/images/map.jpg')}}" data-lightbox="lightboxOverlay" data-title="">
                                        <img src="{{asset('frontend/images/map.jpg')}}" alt="">
                                        <span><i class=""></i></span>
                                    </a>
                                </center>
                                   <br>
                                   <h6>สถานที่ตั้ง : กรมประชาสัมพันธ์ เลขที่9 ซอยอารีย์สัมพันธ์ ถนนพระราม 6 แขวงสามเสนใน เขตพญาไท กรุงเทพฯ 10400 </h6>
                                   <h6>โทร : 02-618-2323 โทรสาร : 02-618-2364, 618-2399</h6> 
                                   <h6> Website : http://www.prd.go.th </h6>
                                   <h6> E-mail : webmaster@prd.go.th </h6>
                                   
                                 @include('layouts.sharesocial')
                            </article>
                        </div>
                    </div>
                </section>  
                   
            </div><!-- end left side content area -->
            <!-- sidebar -->
            <div class="large-4 columns">
                <aside class="secBg sidebar"> 
                    <div class="row">
                        <!-- social Fans Widget -->
                      @include('layouts.social')
                      <!-- End social Fans Widget -->
                    </div>
                </aside>
            </div><!-- end sidebar -->
        </div>
    </section><!-- End Category Content-->
@endsection


