@extends('layouts.frontend')

@section('meta')
    <meta property="og:title" content="{{$news->title}}">
    <meta property="og:image" content="{{asset('news/'.$news->headlines)}}">
    <meta property="og:description" content="{{$news->title}}">
    <meta property="og:url" content="{{URL::current()}}">
@endsection

@section('content')
<!--breadcrumbs-->
<section id="breadcrumb" class="breadcrumb-video-2">
    <div class="row">
        <div class="large-12 columns">
            <nav aria-label="You are here:" role="navigation">
                <ul class="breadcrumbs">
                    <li><i class="fa fa-home"></i><a href="{{url('/')}}">หน้าหลัก</a></li>
                    <li><a href="">ข่าวประชาสัมพันธ์</a></li>
                </ul>
            </nav>
        </div>
    </div>
</section>
<!--end breadcrumbs-->

<div class="row">
    <!-- left side content area -->
    <div class="large-8 columns">
        <!--single inner video-->
        <section class="content content-with-sidebar">
            <section class="inner-video">
                <div class="main-heading removeMargin">
                    <div class="row secBg padding-14 removeBorderBottom">
                        <div class="medium-8 small-8 columns">
                            <div class="head-title">
                                <i class="fa fa-newspaper-o"></i>
                                <h4>ข่าวประชาสัมพันธ์</h4>
                            </div>
                        </div>
                        <div class="medium-4 small-4 columns">
                            <ul class="tabs text-right pull-right" data-tabs id="newVideos">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row secBg">
                    <div class="large-12 columns inner-flex-video ">
                        <div class="flex-video widescreen">
                            <center> <img src="{{asset('news/'.$news->headlines)}}" alt="" class="img-fluid"></center>
                        </div>
                    </div>
                </div>
            </section>
            <!-- single post stats -->
            <section class="SinglePostStats">
                <!-- newest video -->
                <div class="row secBg">
                    <div class="large-12 columns">
                        <div class="media-object stack-for-small">
                            <div class="media-object-section object-second">
                                <div class="author-des clearfix">
                                    <div class="post-title">
                                        <h4>{{$news->title}}</h4>
                                        <p>
                                            <span style="font-size:14px;"><i class="fa fa-clock-o"></i>{{DateTime::createFromFormat('Y-m-d H:i:s',$news->created_at)->format('d-m-Y')}}</span>
                                            <span style="font-size:14px;"><i class="fa fa-eye"></i>{{$news->view}}</span>
                                            <span style="font-size:14px;">ผู้ประกาศข่าว : {{$news->correspondent}}</span>
                                            <span style="font-size:14px;">ผู้เรียบเรียง : {{$news->composer}}</span>
                                            <span style="font-size:14px;">แหล่งที่มา : {{$news->source}}</span>
                                        </p>  
                                        <br>
                                        @include('layouts.sharesocial')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- End single post stats -->
            <!-- single post description -->
            <section class="singlePostDescription">
                <div class="row secBg">
                    <div class="large-12 columns">
                        <div class="heading">
                            <h5>รายละเอียดข่าว</h5>
                        </div>
                        <div class="description">
                          <?php echo $news->detail; ?>
                        </div>
                    </div>
                </div>
            </section><!-- End single post description -->
            @php $newdetail = \App\FileNewsDetail::where('news_id' , $news->id)->get(); @endphp @if($newdetail->count() != 0 )
            <section class="singlePostDescription">
                <div class="row secBg">
                    <div class="large-12 columns">
                        <div class="heading">
                            <h5>รูปรายละเอียดข่าว</h5>
                        </div>
                        <div class="description">
                            <section id="movies">
                                <div class="row secBg">
                                    <div class="large-12 columns">
                                        <div id="owl-demo-movie" class="owl-carousel carousel" data-autoplay="true" data-autoplay-timeout="3000" data-autoplay-hover="true"
                                            data-car-length="5" data-items="6" data-dots="false" data-loop="true" data-auto-width="true"
                                            data-margin="10">
                                            @foreach($newdetail as $item)
                                            <div class="item-movie item thumb-border">
                                                <figure class="premium-img">
                                                    <img src="{{asset('newsdetail/'.$item->filename)}}" alt="carousel">
                                                    <a href="{{asset('newsdetail/'.$item->filename)}}" class="hover-posts" data-lightbox="lightboxOverlay" data-title="">
                                                        <span><i class="fa fa-search"></i></span>
                                                    </a>
                                                </figure>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </section>
            @endif
            <section class="content content-with-sidebar">
                <!-- newest video -->
                <div class="main-heading removeMargin">
                    <div class="row secBg padding-14 removeBorderBottom">
                        <div class="medium-8 small-8 columns">
                            <div class="head-title">
                                <i class="fa fa-film"></i>
                                <h4>
                                    ทั้งหมด
                                </h4>
                            </div>
                        </div>
                        <div class="medium-4 small-4 columns">
                            <ul class="tabs text-right pull-right" data-tabs id="newVideos">
                                {{--
                                <li class="tabs-title is-active"><a href="#new-all">all</a></li>
                                <li class="tabs-title"><a href="#new-hd">HD</a></li> --}}
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row secBg">
                    <div class="large-12 columns">
                        <div class="row column head-text clearfix">
                            <p class="pull-left">All News : <span>{{$viewnews->count()}} News Posts</span></p>
                            <div class="grid-system pull-right show-for-large">
                                {{-- <a class="secondary-button grid-default" href="#"><i class="fa fa-th"></i></a> --}}
                                <a class="secondary-button current grid-medium" href="#"><i class="fa fa-th-large"></i></a>
                                {{-- <a class="secondary-button list" href="#"><i class="fa fa-th-list"></i></a> --}}
                            </div>
                        </div>
                        <div class="tabs-content" data-tabs-content="newVideos">
                            <div class="tabs-panel is-active" id="new-all">
                                <div class="row list-group">
                                    @foreach($viewnewsall as $item)
                                    <div class="item large-4 columns end grid-medium">
                                        <div class="post thumb-border">
                                            <div class="post-thumb">
                                                <img src="{{asset('news/'.$item->headlines)}}" alt="{{$item->title}}">
                                                <a href="#" onclick="updateview({{$item->id}});" class="hover-posts">
                                                    <span><i class="fa fa-play"></i>ดูรายละเอียด</span>
                                                </a>
                                            </div>
                                            <div class="post-des">
                                                <h6>{{ str_limit($item->title, 50,'...') }}</h6>
                                                <div class="post-stats clearfix">
                                                    <p class="pull-left">
                                                        <i class="fa fa-clock-o"></i>
                                                        <span>{{DateTime::createFromFormat('Y-m-d H:i:s',$item->created_at)->format('d-m-Y') }}</span>
                                                    </p>
                                                    <p class="pull-left">
                                                        <i class="fa fa-eye"></i>
                                                        <span>{{$item->view}}</span>
                                                    </p>
                                                    {{--
                                                    <p class="pull-left">
                                                        <i class="fa fa-eye"></i>
                                                        <span>{{$item->title}}</span>
                                                    </p> --}}
                                                </div>
                                                {{--
                                                <div class="post-summary">
                                                    <p>{{$item->detail}}</p>
                                                </div> --}}
                                                <div class="post-button">
                                                    <a href="single-video-v2.html" class="secondary-button"><i class="fa fa-play-circle"></i>watch
                                                        video</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @include('pagination.default', ['paginator' => $viewnewsall])
                    </div>
            </section>
            </div><!-- end left side content area -->
            <!-- sidebar -->
            <div class="large-4 columns">
                <aside class="secBg sidebar">
                    <div class="row">
                        <div class="large-12 medium-7 medium-centered columns">
                            <section class="widgetBox">
                                <div class="row">
                                    <div class="large-12 columns">
                                        <div class="column row">
                                            <div class="heading category-heading clearfix">
                                                <div class="cat-head pull-left">
                                                    <h4>Hot News</h4>
                                                </div>
                                                <div class="sidebar-video-nav">
                                                    {{--
                                                    <div class="navText pull-right">
                                                        <a class="prev secondary-button"><i class="fa fa-angle-left"></i></a>
                                                        <a class="next secondary-button"><i class="fa fa-angle-right"></i></a>
                                                    </div> --}}
                                                </div>
                                            </div>
                                        </div>
                                        <!-- slide Videos-->
                                        <div id="owl-demo-video" class="owl-carousel carousel" data-car-length="1" data-items="1" data-loop="true" data-nav="false"
                                            data-autoplay="true" data-autoplay-timeout="3000" data-dots="false">
                                            @foreach($viewnews as $item) @if($news->id != $item->id )
                                            <div class="item item-video thumb-border">
                                                <figure class="premium-img">
                                                    <img src="{{asset('news/'.$item->headlines)}}" alt="{{$item->title}}">
                                                    <a href="#" onclick="updateview({{$item->id}});" class="hover-posts">
                                                        <h6> <span><i class="fa fa-play">ดูรายละเอียด</i></span></h6>
                                                    </a>
                                                </figure>
                                                <div class="video-des">
                                                    <h6><a href="#"><h6>{{ str_limit($item->title, 50,'...') }}</h6></a></h6>
                                                    <div class="post-stats clearfix">
                                                        <p class="pull-left">
                                                            <i class="fa fa-user"></i>
                                                            <span><a href="#">{{$item->create_by}}</a></span>
                                                        </p>
                                                        <p class="pull-left">
                                                            <i class="fa fa-clock-o"></i>
                                                            <span>{{DateTime::createFromFormat('Y-m-d H:i:s',$item->created_at)->format('d-m-Y')
                                                                }}</span>
                                                        </p>
                                                        <p class="pull-left">
                                                            <i class="fa fa-eye"></i>
                                                            <span>{{$item->view}}</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif @endforeach
                                        </div><!-- end carousel -->
                                    </div>
                                </div>
                            </section><!-- End Category -->
                        </div>
                        <!-- social Fans Widget -->
                        @include('layouts.social')
                       <!-- End social Fans Widget -->
                    </div>
                </aside>
            </div><!-- end sidebar -->

            

    </div>
</div>
<!--end off canvas content-->
</div>
<!--end off canvas wrapper inner-->
</div>
<!--end off canvas wrapper-->
@endsection
<!-- script files -->
@section('footer')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> {{-- @section('footer') --}}
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
    function updateview($id) {
        //alert($id);
        var token = "<?php echo csrf_token(); ?>";
        $.ajax({
            url: '{{ route("ajaxupdateviewnews") }}',
            datatType: 'json',
            type: 'POST',
            data: {
                '_token': token,
                'id': $id,
            },
            success: function (response) {
                //alert(response.id);
                //redirect to Playyoutube
                window.location = response.url

            }
        });//end ajax
    }
</script> 
@endsection
