<!doctype html>
<html class="no-js" lang="en">

<head>
    <title>CPI Channel</title>

    @section('meta')
    @show
    
    <!-- icon web app -->
    <link rel="apple-touch-icon" sizes="16x16" href="{{asset('frontend/images/sliderimages/CPILogo.PNG')}}">
    <link rel="icon" sizes="192×192" href="{{asset('frontend/images/sliderimages/CPILogo.PNG')}}">
    <link rel="icon" sizes="128×128" href="{{asset('frontend/images/sliderimages/CPILogo.PNG')}}">
    <link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('frontend/css/app.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/theme.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/font-awesome.min.css')}}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/layerslider/css/layerslider.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/responsive.css')}}">
    <link href="{{asset('lightbox/css/lightbox.css')}}" rel="stylesheet">
</head>

<body>
    <div class="off-canvas-wrapper">
        <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
            <!--header-->
            <div class="off-canvas position-left light-off-menu" id="offCanvas-responsive" data-off-canvas>
                <div class="off-menu-close">
                    <h3>Menu</h3>
                    <span data-toggle="offCanvas-responsive"><i class="fa fa-times"></i></span>
                </div>
                <ul class="vertical menu off-menu" data-responsive-menu="drilldown">
                    <li class="has-submenu">
                        <a href="{{url('/')}}"><i class="fa fa-home"></i>หน้าหลัก</a>
                    </li>
                    <li class="has-submenu" data-dropdown-menu="example1">
                        <a href="{{route('Category',['id' => 0])}}"><i class="fa fa-toggle-right"></i>วีดีโอ</a>
                        <ul class="submenu menu vertical" data-submenu data-animate="slide-in-down slide-out-up">
                            <li><a href="{{route('Category',['id' => 0])}}"><i class="fa fa-toggle-right"></i>ทั้งหมด</a></li>
                            @php $category = \App\CategoryTable::where('is_public',1)->get(); @endphp 
                            @foreach($category as $item)
                            <li><a href="{{route('Category',['id' => $item->id])}}"><i class="fa fa-toggle-right"></i>{{$item->name}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    
                    <li class="has-submenu" data-dropdown-menu="example1">
                        <a href="{{route('News',['id' => 0])}}"><i class="fa fa-newspaper-o"></i>ข่าวประชาสัมพันธ์</a>
                        <ul class="submenu menu vertical" data-submenu data-animate="slide-in-down slide-out-up">
                            <li><a href="{{route('News',['id' => 0])}}"><i class="fa fa-newspaper-o"></i>ทั้งหมด</a></li>
                            @php $categorynews = \App\CategoryNewsTable::where('is_public',1)->get(); @endphp 
                            @foreach($categorynews as $item)
                            <li><a href="{{route('News',['id' => $item->id])}}"><i class="fa fa-newspaper-o"></i>{{$item->name}}</a></li>
                            @endforeach
                        </ul>
                    </li>

                    <li class="has-submenu" data-dropdown-menu="example1">
                        <a href="{{route('Infographic',['id' => 0])}}"><i class="fa fa-photo"></i>อินโฟกราฟฟิก</a>
                        <ul class="submenu menu vertical" data-submenu data-animate="slide-in-down slide-out-up">
                            <li><a href="{{route('Infographic',['id' => 0])}}"><i class="fa fa-photo"></i>ทั้งหมด</a></li>
                            @php $CategoryInfographic = \App\CategoryInfographicTable::where('is_public',1)->get(); @endphp
                            @foreach($CategoryInfographic as $item)
                            <li><a href="{{route('Infographic',['id' => $item->id])}}"><i class="fa fa-photo"></i>{{$item->name}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    <li><a href="{{url('feeds')}}"><i class="fa fa-industry"></i>สำนักข่าว NNT</a></li>
                    <li><a href="http://1.179.246.149"><i class="fa fa-file-movie-o"></i>NBT2HD</a></li>
                    <li class="has-submenu" data-dropdown-menu="Category">
                        <a href=""><i class="fa fa-institution"></i>หน่วยงานที่เกี่ยวข้อง</a>
                        <ul class="submenu menu vertical" data-submenu data-animate="slide-in-down slide-out-up">
                            <li><a href="{{url('nacc')}}"><i class="fa fa-angle-right"></i>ปปช</a></li>
                            <li><a href=""><i class="fa fa-angle-right"></i>ปปท</a></li>
                        </ul>
                    </li>
                    
                </ul>
            </div>
            <div class="off-canvas-content" data-off-canvas-content>
                <header>
                    <!--Navber-->
                    <section id="navBar">
                        <nav class="sticky-container" data-sticky-container>
                            <div class="sticky topnav" data-sticky data-top-anchor="navBar" data-btm-anchor="footer-bottom:bottom" data-margin-top="0"
                                data-margin-bottom="0" style="width: 100%; background: #fff;" data-sticky-on="large">
                                <div class="row">
                                    <div class="large-12 columns">
                                        <div class="title-bar" data-responsive-toggle="beNav" data-hide-for="large">
                                            <button class="menu-icon" type="button" data-toggle="offCanvas-responsive"></button>
                                            <div class="top-bar-middle">
                                                <img src="{{asset('frontend/images/sliderimages/cpichannel.jpg')}}" width="90px" height="30px" alt="logo">
                                            </div>
                                            <div class="top-bar-right">
                                                @include('layouts.sharesocial')
                                            </div>
                                        </div>
                                        <div class="top-bar show-for-large" id="beNav" style="width: 100%;">
                                            <div class="top-bar-left">
                                                <ul class="menu">
                                                    <li class="menu-text">
                                                        <a href="{{url('/')}}"><img src="{{asset('frontend/images/sliderimages/cpichannel.jpg')}}" width="100" height="30" alt="logo"></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="top-bar-right">
                                                <ul class="menu vertical medium-horizontal" data-responsive-menu="drilldown medium-dropdown">
                                                    <li class="has-submenu active">
                                                        <a href="{{url('/')}}"><i class="fa fa-home"></i>หน้าหลัก</a>
                                                    </li>
                                                    <li class="has-submenu" data-dropdown-menu="Category">
                                                        <a><i class="fa fa-toggle-right"></i>วีดีโอ</a>
                                                        <ul class="submenu menu vertical" data-submenu data-animate="slide-in-down slide-out-up">
                                                            <li><a href="{{route('Category',['id' => 0])}}"><i class="fa fa-angle-right"></i>ทั้งหมด</a>
                                                            @foreach($category as $item)
                                                            <li><a href="{{route('Category',['id' => $item->id])}}"><i class="fa fa-angle-right"></i>{{$item->name}}</a></li>
                                                            @endforeach
                                                        </ul>
                                                        </li>
                                                    <li class="has-submenu" data-dropdown-menu="Category">
                                                        <a href=""><i class="fa fa-newspaper-o"></i>ข่าวประชาสัมพันธ์</a>
                                                            <ul class="submenu menu vertical" data-submenu data-animate="slide-in-down slide-out-up">
                                                                <li><a href="{{route('News',['id' => 0])}}"><i class="fa fa-angle-right"></i>ทั้งหมด</a></li>
                                                                @foreach($categorynews as $item)
                                                                <li><a href="{{route('News',['id' => $item->id])}}"><i class="fa fa-angle-right"></i>{{$item->name}}</a></li>
                                                                @endforeach
                                                            </ul>
                                                    </li>
                                                    <li class="has-submenu" data-dropdown-menu="Category">
                                                        <a href=""><i class="fa fa-photo"></i>กิจกรรม</a>
                                                            <ul class="submenu menu vertical" data-submenu data-animate="slide-in-down slide-out-up">
                                                                <li><a href="{{route('Infographic',['id' => 0])}}"><i class="fa fa-angle-right"></i>ทั้งหมด</a></li>
                                                                @foreach($CategoryInfographic as $item)
                                                                <li><a href="{{route('Infographic',['id' => $item->id])}}"><i class="fa fa-angle-right"></i>{{$item->name}}</a></li>
                                                                @endforeach
                                                            </ul>
                                                    </li>
                                                    <li><a href="{{url('feeds')}}"><i class="fa fa-industry"></i>สำนักข่าว NNT</a></li>
                                                    <li><a href="http://1.179.246.149" target="_blank"><i class="fa fa-file-movie-o"></i>NBT2HD</a></li>
                                                    <li class="has-submenu" data-dropdown-menu="Category">
                                                        <a href=""><i class="fa fa-institution"></i>หน่วยงานที่เกี่ยวข้อง</a>
                                                            <ul class="submenu menu vertical" data-submenu data-animate="slide-in-down slide-out-up">
                                                                <li><a href="{{url('nacc')}}"><i class="fa fa-angle-right"></i>ปปช</a></li>
                                                                <li><a href=""><i class="fa fa-angle-right"></i>ปปท</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="has-submenu">
                                                        <a href="{{url('Contact')}}"><i class="fa fa-phone"></i>ติดต่อเรา</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </nav>
                    </section>
                </header><!-- End Header -->
                 @yield('content')
                <footer>
                    <a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-double-up"></i></a>
                </footer>

                <div id="footer-bottom">
                    <div class="logo text-center">
                        <a href="http://www.pacc.go.th/index.php/greeting"><img src="{{asset('frontend/images/pacc.png')}}" width="125px" height="125px" alt="footer logo"></a>
                        <a href="https://www.nacc.go.th"><img src="{{asset('frontend/images/nacc.gif')}}" width="125px" height="125px" alt="footer logo"></a>
                        <a href="http://www.prd.go.th/main.php?filename=intro_mother"><img src="{{asset('frontend/images/logoprd.png')}}" width="125px" height="125px" alt="footer logo"></a>
                    </div>
                    <div class="btm-footer-text text-center">
                        <p>© 2018. All rights reserved.</p> <br>
                        <div align="center"><a href="http://www.amazingcounters.com"><img border="0" src="http://cc.amazingcounters.com/counter.php?i=3227120&c=9681673" alt="AmazingCounters.com"></a></div>
                    </div>
                </div>
            </div>
            <!--end off canvas content-->
        </div>
        <!--end off canvas wrapper inner-->
    </div>
    <!--end off canvas wrapper-->

    <!-- script files -->
    <script src="{{asset('frontend/bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('frontend/bower_components/what-input/what-input.js')}}"></script>
    <script src="{{asset('frontend/bower_components/foundation-sites/dist/foundation.js')}}"></script>
    <script src="{{asset('frontend/js/jquery.showmore.src.js')}}" type="text/javascript"></script>
    <script src="{{asset('frontend/js/app.js')}}"></script>
    <script src="{{asset('frontend/layerslider/js/greensock.js')}}" type="text/javascript"></script>
    <!-- LayerSlider script files -->
    <script src="{{asset('frontend/layerslider/js/layerslider.transitions.js')}}" type="text/javascript"></script>
    <script src="{{asset('frontend/layerslider/js/layerslider.kreaturamedia.jquery.js')}}" type="text/javascript"></script>
    <script src="{{asset('frontend/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('frontend/js/inewsticker.js')}}" type="text/javascript"></script>
    <script src="{{asset('frontend/js/jquery.kyco.easyshare.js')}}" type="text/javascript"></script>
    <script src="{{asset('lightbox/js/lightbox.js')}}"></script>
    <script>
        lightbox.option({
            //'resizeDuration': 200,
            'wrapAround': false,
            'albumLabel': ""
        })
    </script>
</body>

</html>
@yield('footer')