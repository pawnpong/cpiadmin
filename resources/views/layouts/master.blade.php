
<!DOCTYPE html>
<html lang="en">

<head>
    <title>CPI CHANNEL</title>
    <!-- HTML5 Shim and Respond.js IE9 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Phoenixcoded">
    <meta name="keywords" content=", Flat ui, Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="Phoenixcoded">
    <!-- Favicon icon -->
    <link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components/bootstrap/css/bootstrap.min.css')}}">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/icon/themify-icons/themify-icons.css')}}">
    <!-- animation css -->
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components/animate.css/css/animate.css')}}">

    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/icon/icofont/css/icofont.css')}}">
    <!-- flag icon framework css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/pages/flag-icon/flag-icon.min.css')}}">
    <!-- Menu-Search css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/pages/menu-search/css/component.css')}}">
    <!-- Date-time picker css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/pages/advance-elements/css/bootstrap-datetimepicker.css')}}">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components/bootstrap-daterangepicker/css/daterangepicker.css')}}" />
    <!-- Date-Dropper css -->
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components/datedropper/css/datedropper.min.css')}}" />
    <!-- Color Picker css -->
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components/spectrum/css/spectrum.css')}}" />

    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <!-- color .css -->

    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/linearicons.css')}}" >
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/simple-line-icons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/ionicons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.mCustomScrollbar.css')}}">
    <!-- Data Table -->
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/pages/data-table/css/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">
    
    <!-- hover-effect.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/pages/hover-effect/normalize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/pages/hover-effect/set2.css')}}">
    @yield('cssheader')

</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div></div>
        </div>
    </div>
    <!-- Pre-loader end -->

    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">

            <nav class="navbar header-navbar pcoded-header" header-theme="theme4">
                <div class="navbar-wrapper">
                    <div class="navbar-logo">
                        <a class="mobile-menu" id="mobile-collapse" href="#!">
                            <i class="ti-menu"></i>
                        </a>
                        <a class="mobile-search morphsearch-search" href="#">
                            <i class="ti-search"></i>
                        </a>
                        <a href="{{url('/admin')}}">
                           <h5> <img class="img-fluid" style="heigth:50px;width:50px;" src="{{asset('frontend\images\logoprd.png')}}" alt="Theme-Logo" />
                            CPI CHANNEL
                           </h5>
                        </a>
                        <a class="mobile-options">
                            <i class="ti-more"></i>
                        </a>
                    </div>
                    <div class="navbar-container container-fluid">
                        <div>
                            <ul class="nav-left"> 
                            </ul>
                            <ul class="nav-right">
                                <li class="user-profile header-notification">
                                    <a href="#!">
                                        <img src="{{asset('assets/images/user.png')}}" alt="User-Profile-Image">
                                        <span>{{ Auth::user()->name }}</span>
                                        <i class="ti-angle-down"></i>
                                    </a>
                                    <ul class="show-notification profile-notification">
                                       
                    
                                        <li>
                                            <a href="{{ url('/logout') }}">
                                                <i class="ti-layout-sidebar-left"></i> ออกจากระบบ
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <!-- Sidebar inner chat end-->
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <nav class="pcoded-navbar" pcoded-header-position="relative">
                        <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="">
                                <div class="main-menu-header">
                                    <img class="img-40" src="{{asset('assets/images/user.png')}}" alt="User-Profile-Image">
                                    <div class="user-details">
                                        <span>Admin</span>
                                        {{-- <span id="more-details">{{ Auth::user()->name }}<i class="ti-angle-down"></i></span> --}}
                                    </div>
                                </div>

                                {{-- <div class="main-menu-content">
                                    <ul>
                                        <li class="more-details">
                                           
                                            <a href="{{ url('/logout') }}"><i class="ti-layout-sidebar-left"></i>ออกจากระบบ</a>
                                        </li>
                                    </ul>
                                </div> --}}
                            </div>
                            <div class="pcoded-navigatio-lavel" data-i18n="nav.category.navigation" menu-title-theme="theme5">Navigation</div>
                            <ul class="pcoded-item pcoded-left-item ">
                                <li class="{{\Request::route()->getName() == "adminindex"  ? 'active' :''  }} ">
                                <a href="{{url ('/admin') }}">
                                        <span class="pcoded-micon"><i class="ti-home"></i></span>
                                <span class="pcoded-mtext" data-i18n="nav.dash.main">หน้าหลัก</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                <li class="pcoded-hasmenu" id="main_news">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="ti-layout"></i></span>
                                        <span class="pcoded-mtext" data-i18n="nav.page_layout.main">จัดการข่าวประชาสัมพันธ์</span>
                                        {{--  <span class="pcoded-badge label label-warning">NEW</span>  --}}
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li id="categorynews_active" class="{{ (substr(\Request::route()->getName(),0,18) == "admin.categorynews"   ? 'active' :'' ) }}">
                                            <a href="{{route('admin.categorynews.index')}}">
                                                <span class="pcoded-micon"><i class="ti-layout"></i></span>
                                                <span class="pcoded-mtext" data-i18n="nav.widget.main">หมวดหมู่ประชาสัมพันธ์</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li id="news_active" class="{{ (substr(\Request::route()->getName(),0,15) == "admin.newstopic"   ? 'active' :'' ) }}">
                                            <a href="{{route('admin.newstopic.index')}}">
                                                <span class="pcoded-micon"><i class="ti-layout"></i></span>
                                                <span class="pcoded-mtext" data-i18n="nav.page_layout.main">ข่าวประชาสัมพันธ์</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>    
                                        </li>      
                                    </ul>
                                </li>
                                <li class="pcoded-hasmenu" id="main_infographic">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="ion-images"></i></span>
                                        <span class="pcoded-mtext" data-i18n="nav.page_layout.main">จัดการกิจกรรม</span>
                                        {{--  <span class="pcoded-badge label label-warning">NEW</span>  --}}
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li id="categoryinfographic_active" class="{{ (substr(\Request::route()->getName(),0,26) == "admin.categoryinfographics"   ? 'active' :'' ) }}">
                                                <a href="{{route('admin.categoryinfographics.index')}}">
                                                    <span class="pcoded-micon"><i class="ion-images"></i></span>
                                                    <span class="pcoded-mtext" data-i18n="nav.widget.main">หมวดหมู่กิจกรรม</span>
                                                    <span class="pcoded-mcaret"></span>
                                                </a>
                                        </li>
                                        <li id="infographic_active" class="{{ (substr(\Request::route()->getName(),0,18) == "admin.infographics"   ? 'active' :'' ) }}">
                                            <a href="{{route('admin.infographics.index')}}">
                                                <span class="pcoded-micon"><i class="ion-images"></i></span>
                                                <span class="pcoded-mtext" data-i18n="nav.widget.main">กิจกรรม</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="pcoded-hasmenu" id="main_youtube">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="ion-social-youtube"></i></span>
                                        <span class="pcoded-mtext" data-i18n="nav.page_layout.main">จัดการวีดีโอ</span>
                                        {{--  <span class="pcoded-badge label label-warning">NEW</span>  --}}
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li id="category_active" class="{{ (substr(\Request::route()->getName(),0,14) == "admin.category" && strlen(\Request::route()->getName())  ==20  ? 'active' :'' ) }}">
                                                <a href="{{route('admin.category.index')}}">
                                                    <span class="pcoded-micon"><i class="ion-social-youtube"></i></span>
                                                    <span class="pcoded-mtext" data-i18n="nav.widget.main">หมวดหมู่วีดีโอ</span>
                                                    <span class="pcoded-mcaret"></span>
                                                </a>
                                        </li>
                                        <li id="youtube" class="{{ (substr(\Request::route()->getName(),0,13) == "admin.youtube"   ? 'active' :'' ) }}">
                                                <a href="{{route('admin.youtube.index')}}">
                                                    <span class="pcoded-micon"><i class="ion-social-youtube"></i></span>
                                                    <span class="pcoded-mtext" data-i18n="nav.widget.main">วีดีโอ</span>
                                                    <span class="pcoded-mcaret"></span>
                                                </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="{{ (substr(\Request::route()->getName(),0,11) == "admin.users"   ? 'active' :'' ) }}">
                                    <a href="{{route('admin.users.index')}}">
                                        <span class="pcoded-micon"><i class="ti-user"></i></span>
                                        <span class="pcoded-mtext" data-i18n="nav.dash.main">จัดการผู้ใช้</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="{{url('/')}}" target="_blank">
                                        <span class="pcoded-micon"><i class="ion-ios-world-outline"></i></span>
                                        <span class="pcoded-mtext" data-i18n="nav.widget.main">ไปยังหน้าเว็ปไซต์</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    @yield('content')
                </div>
            </div>
        </div>
    </div> 

    <!-- Required Jquery -->
    <script type="text/javascript" src="{{asset('bower_components/jquery/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('bower_components/popper.js/js/popper.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('bower_components/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="{{asset('bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="{{asset('bower_components/modernizr/js/modernizr.js')}}"></script>
    <script type="text/javascript" src="{{asset('bower_components/modernizr/js/css-scrollbars.js')}}"></script>

    <!-- classie js -->
    <script type="text/javascript" src="{{asset('bower_components/classie/js/classie.js')}}"></script>

     <!-- Bootstrap date-time-picker js -->
     <script type="text/javascript" src="{{asset('assets/pages/advance-elements/moment-with-locales.min.js')}}"></script>
     <script type="text/javascript" src="{{asset('bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
     <script type="text/javascript" src="{{asset('assets/pages/advance-elements/bootstrap-datetimepicker.min.js')}}"></script>
     <!-- Date-range picker js -->
     <script type="text/javascript" src="{{asset('bower_components/bootstrap-daterangepicker/js/daterangepicker.js')}}"></script>
     <!-- Date-dropper js -->
     <script type="text/javascript" src="{{asset('bower_components/datedropper/js/datedropper.min.js')}}"></script>
     <!-- Color picker js -->
     <script type="text/javascript" src="{{asset('bower_components/spectrum/js/spectrum.js')}}"></script>
     <script type="text/javascript" src="{{asset('bower_components/jscolor/js/jscolor.js')}}"></script>
     <!-- tinymce js -->
     <script src="{{asset('assets/pages/wysiwyg-editor/js/tinymce.min.js')}}"></script>
     

    <!-- i18next.min.js -->
    <script type="text/javascript" src="{{asset('bower_components/i18next/js/i18next.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('bower_components/jquery-i18next/js/jquery-i18next.min.js')}}"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="{{asset('assets/js/script.js')}}"></script>
    <script src="{{asset('assets/pages/wysiwyg-editor/wysiwyg-editor.js')}}"></script><!-- wysiwyg-editor -->

    <script src="{{asset('assets/js/pcoded.min.js')}}"></script>
    <script src="{{asset('assets/js/demo-12.js')}}"></script>
    <script src="{{asset('assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.mousewheel.min.js')}}"></script>

    <!-- data table -->
    <!-- data-table js -->
    <script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/pages/data-table/js/jszip.min.js')}}"></script>
    <script src="{{asset('assets/pages/data-table/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/pages/data-table/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
    
    <script src="{{asset('assets/pages/data-table/js/data-table-custom.js')}}"></script>
    <script src=" https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    @yield('jsfooter')
</body>

</html>


