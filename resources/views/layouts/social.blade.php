<div class="large-12 medium-7 medium-centered columns">
    <div class="widgetBox">
        <div class="widgetTitle">
            <h5>ติดตามเรา</h5>
        </div>
        <div class="widgetContent">
            <div class="social-links">
               <a class="socialButton" href="https://www.facebook.com/prdofficial/?ref=br_rs">
                    <i class="fa fa-facebook"></i>
                    {{-- <span>P'</span> --}}
                    {{-- <span>Fans</span> --}}
                </a>
                <a class="socialButton" href="https://twitter.com/prd_official">
                    <i class="fa fa-twitter"></i>
                    {{-- <span>Dow</span> --}}
                    {{-- <span>Fans</span> --}}
                </a>
                <a class="socialButton" href="https://www.youtube.com/user/prdnewmedia">
                    <i class="fa fa-youtube"></i>
                    {{-- <span>Kub</span> --}}
                    {{-- <span>Fans</span> --}}
                </a>
                <a class="socialButton" href="https://www.instagram.com/explore/locations/253646370/">
                    <i class="fa fa-instagram"></i>
                    {{-- <span>!</span> --}}
                    {{-- <span>Fans</span> --}}
                </a>
               <a class="socialButton" href="https://www.instagram.com/explore/locations/253646370/">
                    <i class="fa fa-rss"></i>
                    {{-- <span>RSS</span> --}}
                    {{-- <span>Fans</span> --}}
                </a>
            
            </div>
        </div>
    </div>
</div><!-- End social Fans Widget -->




@yield('social')