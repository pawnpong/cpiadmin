@extends('layouts.master')


@section('content')

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <!-- Page-header start -->
                <div class="page-header">
                    <div class="page-header-title">
                        <h4>แก้ไขหมวดหมู่กิจกรรม</h4>
                        <span></span>
                    </div>
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{url('/admin')}}">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{route('admin.categoryinfographics.index')}}">แก้ไขหมวดหมู่กิจกรรม</a>
                            <li class="breadcrumb-item"><a href="#">แก้ไข</a>
                        
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5>แก้ไข </h5>
                    </div>
                    <div class="card-block">
                    {!! Form::model($categoryinfographics,['route' => ['admin.categoryinfographics.update',$categoryinfographics->id],'method'=> 'put','files'=>true,'name'=>'form','id'=>'formCreate'])
                    !!} {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">หมวดหมู่กิจกรรม</label>
                                <div class="col-sm-10">
                                <?= Form::text('name',null,['class'=>'form-control','placeholder'=>'ชื่อประเภท Infographics']);?>
                                <span class="messages"></span>
                                    @if ($errors->has('name'))
                                <span class="messages"><p class="text-danger error">{{ $errors->first('name')}}</p></span>
                                    @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2"></label>
                            <div class="col-sm-10">
                                <button data-key="submit" type="submit" class="btn btn-primary" > <i class="ion-archive"></i>บันทึก</button>
                                <a href="{{ route('admin.categoryinfographics.index') }}" class="btn btn-default">
                                <i class="ion-reply"></i>ยกเลิก</a>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('jsfooter')
<script>
      $(document).ready(function() {
            document.getElementById('main_infographic').classList.add('active');
            document.getElementById('main_infographic').classList.add('pcoded-trigger');
            document.getElementById('categoryinfographic_active').classList.add('active');
    });
    //image headline Show
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#blah').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
        $("#imgInp").change(function() {readURL(this);});

</script>
@endsection
