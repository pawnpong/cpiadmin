@extends('layouts.frontend') 
    @section('content')
<!--breadcrumbs-->
            <section id="breadcrumb">
                <div class="row">
                    <div class="large-12 columns">
                        <nav aria-label="You are here:" role="navigation">
                            <ul class="breadcrumbs">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="{{url('/')}}">หน้าหลัก</a>
                                </li>
                                <li>
                                    <span class="show-for-sr">Current: </span> สำนักข่าว NNT
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </section>
            <!--end breadcrumbs-->
            <br>

            <section class="category-content">
                <div class="row">
                    <!-- left side content area -->
                    <div class="large-8 columns">
                        <section class="content content-with-sidebar">
                            <!-- newest video -->
                            <div class="main-heading removeMargin">
                                <div class="row secBg padding-14 removeBorderBottom">
                                    <div class="medium-8 small-8 columns">
                                        <div class="head-title">
                                            <i class="fa fa-newspaper-o"></i>
                                            สำนักข่าว NNT
                                        </div>
                                    </div>
                                    <div class="medium-4 small-4 columns">
                                        <ul class="tabs text-right pull-right" data-tabs id="newVideos">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row secBg">
                                <div class="large-12 columns">
                                    <div class="row column head-text clearfix">
                                        <p class="pull-left">All News :
                                            <span> 1 News posted</span>
                                        </p>
                                        </p>
                                        <div class="grid-system pull-right show-for-large">
                                            <a class="secondary-button current grid-medium" href="#">
                                                <i class="fa fa-th-large"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="tabs-content" data-tabs-content="newVideos">
                                        <div class="tabs-panel is-active" id="new-all">
                                            <div class="row list-group">
                                                @foreach ($data->items as $item)
                                                <div class="item large-4 end columns grid-medium">
                                                    <div class="post thumb-border">
                                                        <div class="post-thumb">
                                                            <img alt="Image" src="{{ $item->get_enclosure()->link }}">
                                                            <a href="{{ $item->get_permalink() }}" class="hover-posts">
                                                                <span><i class="fa fa-play"></i>ดูรายละเอียด</span>
                                                            </a>
                                                        </div>
                                                        <div class="post-des">
                                                            {{--
                                                            <h6>{{ str_limit($item->title, 50,'...') }}</h6> --}}
                                                            <h5>{{ str_limit($item->get_title(), 70, '...' ) }}</a></h5>
                                                            <div class="post-stats clearfix">
                                                                <p class="pull-left">
                                                                    <small>Posted on {{ $item->get_date('j F Y | g:i a') }}</small>
                                                                </p>
                                                            </div>
                                                            <div class="post-summary">
                                                                {{--
                                                                <p>{{$item->detail}}</p> --}}
                                                            </div>
                                                            <div class="post-button">
                                                                <a href="single-video-v2.html" class="secondary-button">
                                                                    <i class="fa fa-play-circle"></i>watch video</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </section>
                        </div>
                        <!-- end left side content area -->
                        <!-- sidebar -->
                        <div class="large-4 columns">
                            <aside class="secBg sidebar">
                                <div class="row">
                                    @include('layouts.social')
                                </div>
                            </aside>
                        </div>
                        <!-- end sidebar -->
                    </div>
                </div>
            </section>
<!-- End Category Content-->
<!-- footer -->


</div>
</div>
</div>
@endsection