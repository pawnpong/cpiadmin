@extends('layouts.frontend') 
@section('content')
<!-- layerslider -->
<section id="slider">
    <div id="full-slider-wrapper">
        <div id="layerslider" style="width:100%;height:500px;">
            <div class="ls-slide" data-ls="transition2d:1;timeshift:-1000;">
                <img src="frontend/images/sliderimages/bg.png" class="ls-bg" alt="Slide background" /> {{-- <h3 class="ls-l"
                    style="left:550px; top:135px; padding: 15px; color: #444444;font-size: 24px;font-family: 'Open Sans'; font-weight: bold; text-transform: uppercase;"
                    data-ls="offsetxin:0;durationin:2500;delayin:500;durationout:750;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotateout:-90;transformoriginout:left bottom 0;">กรมประชาสัมพันธ์</h3>
                --}}
                <h1 class="ls-l" style="left: 50%; top:185px;background: #e96969;padding:0 10px; opacity: 1; color: #ffffff; font-size: 36px; font-family: 'Open Sans'; text-transform: uppercase; font-weight: bold;" data-ls="offsetxin:left;durationin:3000; delayin:800;durationout:850;rotatexin:90;rotatexout:-90;">CPI = Couruption Perception Index</h1><br>
                <p class="ls-l" style="font-weight:600;left:520px; top:250px; opacity: 1;width: 450px; color: #444; font-size: 14px; font-family: 'Open Sans';" data-ls="offsetyin:top;durationin:4000;rotateyin:90;rotateyout:-90;durationout:1050;">ค่า  CPI สูง หมายถึงมีการ คอร์รัปชั่นต่ำ</p><br>
                <img class="ls-l" src="frontend/images/sliderimages/logoprd.PNG" alt="layer 1" style="top:55px; left:500px;" data-ls="offsetxin:right;durationin:3000; delayin:600;durationout:850;rotatexin:-90;rotatexout:90;">
                <img class="ls-l ls-linkto-2" style="top:400px;left:50%;white-space: nowrap;" data-ls="offsetxin:-50;delayin:1000;rotatein:-40;offsetxout:-50;rotateout:-40;" src="frontend/images/sliderimages/left.png" alt="">
                <img class="ls-l ls-linkto-2" style="top:400px;left:52%;white-space: nowrap;" data-ls="offsetxin:50;delayin:1000;offsetxout:50;" src="frontend/images/sliderimages/right.png" alt="">
            </div>
            <div class="ls-slide" data-ls="transition2d:1;timeshift:-1000;">
                <img src="frontend/images/sliderimages/bg2.png" class="ls-bg" alt="Slide background" />
                <h3 class="ls-l" style="left:50%; top:150px; padding: 15px; color: #444444;font-size: 24px;font-family: 'Open Sans'; font-weight: bold; text-transform: uppercase;" data-ls="offsetxin:0;durationin:2500;delayin:500;durationout:750;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotateout:-90;transformoriginout:left bottom 0;">จัดอันดับโดยองค์กรเพื่อความโปร่งใสนานาชาติ</h3>
                <h1 class="ls-l" style="left: 50%; top:200px;background: #e96969;padding:0 10px; opacity: 1; color: #ffffff; font-size: 36px; font-family: 'Open Sans'; text-transform: uppercase; font-weight: bold;" data-ls="offsetxin:left;durationin:3000; delayin:800;durationout:850;rotatexin:90;rotatexout:-90;">ดัชนีภาพลักษณ์คอร์รับชันหรือดัชนีการรับรู้การทุจริต </h1>
                <p class="ls-l" style="text-align:center; font-weight:600;left:50%; top:265px; opacity: 1;width: 450px; color: #444; font-size: 14px; font-family: 'Open Sans';"   data-ls="offsetyin:top;durationin:4000;rotateyin:90;rotateyout:-90;durationout:1050;">(CPI:Couruption Perception Index)</p>
                <img class="ls-l ls-linkto-1" style="top:400px;left:50%;white-space: nowrap;" data-ls="offsetxin:-50;delayin:1000;rotatein:-40;offsetxout:-50;rotateout:-40;" src="frontend/images/sliderimages/left.png" alt="">
                <img class="ls-l ls-linkto-1" style="top:400px;left:52%;white-space: nowrap;" data-ls="offsetxin:50;delayin:1000;offsetxout:50;" src="frontend/images/sliderimages/right.png" alt="">
            </div>
        </div>
    </div>
</section>
<!--end slider-->
<section id="premium">
    <div class="row">
        <div class="heading clearfix">
            <div class="large-11 columns">
                <h4><i class="fa fa-play-circle-o"></i>วีดีโอ</h4>
            </div>
            <div class="large-1 columns">
                <div class="navText show-for-large">
                    <a class="prev secondary-button"><i class="fa fa-angle-left"></i></a>
                    <a class="next secondary-button"><i class="fa fa-angle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div id="owl-demo" class="owl-carousel carousel" data-car-length="4" data-items="4" data-loop="true" data-nav="false" data-autoplay="true"
        data-autoplay-timeout="3000" data-dots="false" data-auto-width="false" data-responsive-small="1" data-responsive-medium="2"
        data-responsive-xlarge="5">
        @php $ytcount = 4 - $youtube->count() ; @endphp @foreach($youtube as $item)
        <div class="item">
            <figure class="premium-img">
                @php $data = explode("=",$item->link); $data[1]; @endphp
                <img src="https://img.youtube.com/vi/{{$data[1]}}/0.jpg" alt="carousel">
                <figcaption>
                    <h5>{{ str_limit($item->title , 50 , '...')}}</h5>
                    <p>
                        <span><i class="fa fa-clock-o"></i>{{DateTime::createFromFormat('Y-m-d H:i:s',$item->created_at)->format('d-m-Y')}}</span>
                        <span><i class="fa fa-eye"></i>{{$item->view}}</span>
                    </p>
                </figcaption>
            </figure>
            <a href="#" onclick="updateview1({{$item->id}});" class="hover-posts">
                <span><i class="fa fa-play"></i>Watch Video</span>
            </a>
        </div>
        @endforeach @if($ytcount > 0 ) @foreach(range(1, $ytcount) as $i)
        <div class="item">
            <figure class="premium-img">
                <img src="http://placehold.it/400x300" alt="carousel">
                <figcaption>
                    <h5>ไม่มีวีดีโอ</h5>
                    {{--
                    <p>Movies Trailer</p> --}}
                </figcaption>
            </figure>
            <a href="" class="hover-posts">
                <span><i class="fa fa-play"></i>Watch Video</span>
            </a>
        </div>
        @endforeach @endif
    </div>
</section>

<div class="row">
    <!-- left side content area -->
    <div class="large-8 columns">
        <section class="content content-with-sidebar">
            <!-- newest video -->
            <div class="main-heading">
                <div class="row secBg padding-14">
                    <div class="medium-8 small-8 columns">
                        <div class="head-title">
                            <i class="fa fa-newspaper-o"></i>
                            <h4>ข่าวกรมประชาสัมพันธ์</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row secBg">
                <div class="large-12 columns">
                    <div class="row column head-text clearfix">
                        <p class="pull-left">All News : <span>{{$news->count()}} News posted</span></p>
                        <div class="grid-system pull-right show-for-large">
                            <a class="secondary-button current grid-medium" href="#"><i class="fa fa-th-large"></i></a>
                        </div>
                    </div>
                    <div class="tabs-content" data-tabs-content="newVideos">
                        <div class="tabs-panel is-active" id="new-all">
                            <div class="row list-group">
                                @php $ytcount = 4 - $news->count(); @endphp @foreach($news as $item)
                                <div class="item large-4 columns end grid-medium">
                                    <div class="post thumb-border">
                                        <div class="post-thumb">
                                            <img src="{{asset('news/'.$item->headlines)}}" alt="{{$item->title}}">
                                            <a href="#" onclick="updateview({{$item->id}});" class="hover-posts">
                                                <span><i class="fa fa-play"></i>ดูรายละเอียด</span>
                                            </a>
                                        </div>
                                        <div class="post-des">
                                            <h5>{{ str_limit($item->title, 85,'...') }}</h5>
                                            
                                            <div class="post-stats clearfix">
                                                <p class="pull-left">
                                                    <i class="fa fa-clock-o"></i>
                                                    <small>{{DateTime::createFromFormat('Y-m-d H:i:s',$item->created_at)->format('d-m-Y')}}</small>
                                                </p>
                                                <p class="pull-left">
                                                    <i class="fa fa-eye"></i>
                                                    <small>{{$item->view}}</small>
                                                </p>
                                            </div>
                                            <div class="post-button">
                                                <a href="single-video-v2.html" class="secondary-button"><i class="fa fa-play-circle"></i>watch
                                                    video
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach @if($news->count() == 0 ) @foreach(range(1, $ytcount) as $i)
                                <div class="item large-4 columns end grid-medium">
                                    <figure class="premium-img">
                                        <img src="http://placehold.it/370x220" alt="new video">
                                        <figcaption>
                                            {{--
                                            <h5>ICE Age 5 upcoming Movie</h5>
                                            <p>Movies Trailer</p> --}}
                                        </figcaption>
                                    </figure>
                                    <a href="" class="hover-posts">
                                        <span><i class="fa fa-play"></i>Watch Video</span>
                                    </a>
                                </div>
                                @endforeach @endif
                            </div>
                        </div>
                    </div>
                    @if($news->count() != 0 )
                    <div class="text-center row-btn">
                        <a class="button radius" href="{{route('News',['id' => 0])}}">ข่าวทั้งหมด</a>
                    </div>
                    @endif
                </div>
            </div>
        </section>
    </div><!-- end left side content area -->
    <!-- sidebar -->
    <div class="large-4 columns padding-right-remove">
        <aside class="secBg sidebar">
            <div class="row">
                @if($viewnews->count() != 0)
                <!-- most view Widget -->
                <div class="large-12 medium-7 medium-centered columns">
                    <section class="widgetBox">
                        <div class="row">
                            <div class="large-12 columns">
                                <div class="column row">
                                    <div class="heading category-heading clearfix">
                                        <div class="cat-head pull-left">
                                            <h4>Hot News</h4>
                                        </div>
                                        <div class="sidebar-video-nav">
                                        </div>
                                    </div>
                                </div>
                                <!-- slide Videos-->
                                <div id="owl-demo-video" class="owl-carousel carousel" data-car-length="1" data-items="1" data-loop="true" data-nav="false"
                                    data-autoplay="true" data-autoplay-timeout="3000" data-dots="false">
                                    @foreach($viewnews as $item)
                                    <div class="item item-video thumb-border">
                                        <figure class="post-thumb">
                                            <img src="{{asset('news/'.$item->headlines)}}" alt="{{$item->title}}">
                                            <a href="#" onclick="updateview({{$item->id}});" class="hover-posts">
                                                <span><i class="">ดูรายละเอียด</i></span>
                                            </a>
                                        </figure>
                                        <div class="video-des">
                                             <h5> {{$item->title}}</h5>
                                            <div class="post-stats clearfix">
                                                <p class="pull-left">
                                                    <i class="fa fa-clock-o"></i>
                                                    <small>{{DateTime::createFromFormat('Y-m-d H:i:s',$item->created_at)->format('d-m-Y')}}</small>
                                                </p>
                                                <p class="pull-left">
                                                    <i class="fa fa-eye"></i>
                                                    <small>{{$item->view}}</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div><!-- end carousel -->
                            </div>
                        </div>
                    </section><!-- End Category -->
                </div>
                @endif @if($youtube->count() != 0)
                <div class="large-12 medium-7 medium-centered columns">
                    <section class="widgetBox">
                        <div class="row">
                            <div class="large-12 columns">
                                <div class="column row">
                                    <div class="heading category-heading clearfix">
                                        <div class="cat-head pull-left">
                                            <h4>Hot Videos</h4>
                                        </div>
                                        <div class="sidebar-video-nav">
                                        </div>
                                    </div>
                                </div>
                                <!-- slide Videos-->
                                <div id="owl-demo-video" class="owl-carousel carousel" data-car-length="1" data-items="1" data-loop="true" data-nav="false"
                                    data-autoplay="true" data-autoplay-timeout="3000" data-dots="false">
                                    @foreach($youtube as $item)
                                    <div class="item item-video thumb-border">
                                        <figure class="premium-img">
                                            <?php
                                            $data = explode("=",$item->link);
                                            $data[1];
                                            ?>
                                            <iframe width="325" height="190" src="https://www.youtube.com/embed/{{$data[1]}}" allowfullscreen></iframe>
                                            <a href="#" onclick="updateview1({{$item->id}});" class="hover-posts">
                                                <span><i class="">ดูรายละเอียด</i></span>
                                            </a>
                                        </figure>
                                        <div class="video-des">
                                          <h5> {{$item->title}} </h5>
                                            <div class="post-stats clearfix">
                                                <p class="pull-left">
                                                    <i class="fa fa-clock-o"></i>
                                                    <small>{{DateTime::createFromFormat('Y-m-d H:i:s',$item->created_at)->format('d-m-Y')}}</small>
                                                </p>
                                                <p class="pull-left">
                                                     <i class="fa fa-eye"></i>
                                                     <small>{{$item->view}}</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div><!-- end carousel -->
                            </div>
                        </div>
                    </section><!-- End Category -->
                </div>
                @endif @include('layouts.social')

            </div>
        </aside>
    </div><!-- end sidebar -->

</div>
<section id="movies">
    <div class="row secBg">
        <div class="large-12 columns">
            <div class="column row">
                <div class="heading category-heading clearfix">
                    <div class="cat-head pull-left">
                        <i class="fa fa-picture-o"></i>
                        <h4>กิจกรรม</h4>
                    </div>
                    <div>
                        <div class="navText pull-right show-for-large">
                            <a class="prev secondary-button"><i class="fa fa-angle-left"></i></a>
                            <a class="next secondary-button"><i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- movie carousel -->
            <div id="owl-demo-movie" class="owl-carousel carousel" data-autoplay="true" data-autoplay-timeout="3000" data-autoplay-hover="true"
                data-car-length="5" data-items="6" data-dots="false" data-loop="true" data-auto-width="true" data-margin="10">
                @if($Infographic->count()!=0) @foreach($Infographic as $item)
                <div class="item-movie item thumb-border">
                    <figure class="premium-img">
                        @if($item->filename == '' || $item->filename == null)
                        <img src="http://placehold.it/185x275" alt="carousel"> @else
                        <img src="{{asset('fileinfographic/'.$item->filename)}}" alt="carousel"> @endif
                        <a href="fileinfographic/{{$item->filename}}" onclick="updateview2({{$item->id}});" class="hover-posts" data-lightbox="lightboxOverlay"
                            data-title="{{$item->department}}"></i>
                            <span><i class="">{{$item->name}}</i></span>
                        </a>

                    </figure>
                </div>
                @endforeach @else @foreach(range(1, 6) as $i)
                <div class="item-movie item thumb-border">
                    <figure class="premium-img">
                        <img src="http://placehold.it/185x275" alt="carousel">
                        <a href="#" class="hover-posts">
                            <span><i class="fa fa-search"></i></span>
                        </a>
                    </figure>
                </div>
                @endforeach @endif
            </div><!-- end carousel -->
            @if($Infographic->count() != 0)
            <div class="row collapse">
                <div class="large-12 columns text-center row-btn">
                    <a href="{{ route('Infographic',['id' => 0 ]) }}" class="button radius">กิจกรรมทั้งหมด</a>
                </div>
            </div>
            @endif
        </div>
    </div>
</section>
<br> 
@endsection 
@section('footer')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
    function updateview1($id) {
        var token = "<?php echo csrf_token(); ?>";
        $.ajax({
            url: '{{ route("ajaxupdateview") }}',
            datatType: 'json',
            type: 'POST',
            data: {
                '_token': token,
                'id': $id,
            },
            success: function (response) {
                window.location = response.url

            }
        });//end ajax
    }
</script>


<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
    function updateview($id) {
        var token = "<?php echo csrf_token(); ?>";
        $.ajax({
            url: '{{ route("ajaxupdateviewnews") }}',
            datatType: 'json',
            type: 'POST',
            data: {
                '_token': token,
                'id': $id,
            },
            success: function (response) {
                window.location = response.url

            }
        });//end ajax
    }
</script>

<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
    function updateview2($id) {
        //alert($id);
        var token = "<?php echo csrf_token(); ?>";
        $.ajax({
            url: '{{ route("ajaxupdateinfographic") }}',
            datatType: 'json',
            type: 'POST',
            data: {
                '_token': token,
                'id': $id,
            },
            success: function (response) {
                //alert(response.id);
                //redirect to Playyoutube
                //window.location = response.url

            }
        });//end ajax
    }
</script>
 @endsection