<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Category = new \App\CategoryTable();
        $Category->insert([
            [ 'name' => 'ต่อต้านการทุจริต','is_public' => '1' ],
            [ 'name' => 'สปอตประชาสัมพันธ์','is_public' => '1' ],
            [ 'name' => 'รายการทีวี','is_public' => '1' ],
        ]);

        $Categorynews = new \App\CategoryNewsTable();
        $Categorynews->insert([
            [ 'name' => 'ข่าวอาชญากรรม','is_public' => '1' ],
            [ 'name' => 'ข่าวการเมือง','is_public' => '1' ],
            [ 'name' => 'ข่าวสังคม','is_public' => '1' ],
            [ 'name' => 'ข่าวเศรษฐกิจ','is_public' => '1' ],
            [ 'name' => 'ข่าวกีฬา','is_public' => '1' ],
            [ 'name' => 'ข่าวไอที','is_public' => '1' ],
        ]);
        $categoryinfographic = new \App\CategoryInfographicTable();
        $categoryinfographic->insert([
            [ 'name' => 'กรมประชาสัมพันธ์','is_public' => '1' ],
            [ 'name' => 'ปปช','is_public' => '1' ],
            [ 'name' => 'ปปท','is_public' => '1' ],
        ]);
    }
}
