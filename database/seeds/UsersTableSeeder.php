<?php

use Illuminate\Database\Seeder;
//use Carbon\Carbon;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $U = new \App\UserRole();
        $U->insert([
            [ 'name' => 'admin' ],
            [ 'name' => 'member' ],
        ]);
        $user = new \App\User();
        $user->insert([
            [
                'name' => 'yochigangTest',
                'email' => 'mrbuctico@gmail.com',
                'userrole_id' => 1,
                'password' => bcrypt('123456'),
                'remember_token' => str_random(10), 
                'is_public' => 1,
                //'created_at' => Carbon::now()->format('Y-m-d H:i:s'),   
            ],[
                'name' => "admin",
                'email' => "admin_cpi@gmail.com",
                'userrole_id' => 1,
                'password' => bcrypt('123456'),
                'remember_token' => str_random(10),
                'is_public' => 1,
            ]
        ]);
    }
}
