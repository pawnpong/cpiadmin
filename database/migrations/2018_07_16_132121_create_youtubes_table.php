<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYoutubesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            //$table->string('image');
            $table->string('create_by')->nullable();;
            $table->string('is_public');
            $table->timestamps();
        });

        Schema::create('youtubes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Category_id')->unsigned();
            $table->foreign('Category_id')->references('id')->on('category'); 
            $table->string('title');

            // $table->integer('type_vdo');
            $table->text('link')->nullable(); //เอา ลิ้งค์จากยูทูป
           // $table->text('vdo')->nullable();// อัพจากเคืื่อง

            $table->text('description')->nullable();
            $table->integer('view')->nullable();
            $table->string('create_by')->nullable();
            $table->date('startdate');
            $table->date('enddate');
            $table->string('is_public');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('youtubes');
        Schema::drop('category');
    }
}
