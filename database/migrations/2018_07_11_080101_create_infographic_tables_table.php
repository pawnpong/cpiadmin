<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfographicTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categoryinfographic', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            // $table->string('image');
            $table->string('create_by')->nullable();;
            $table->string('is_public');
            $table->timestamps();
        });
        Schema::create('infographic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('CategoryInfographic_id')->unsigned();
            $table->foreign('CategoryInfographic_id')->references('id')->on('categoryinfographic');
            $table->string('name');
            $table->string('department');
            $table->string('filename')->nullable();
            $table->string('create_by')->nullable();
            $table->string('is_public');
            $table->date('startdate');
            $table->date('enddate');
            $table->integer('view');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categoryinfographic');
        Schema::drop('infographic');
    }
}
