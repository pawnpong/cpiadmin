<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorynews', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            // $table->string('image');
            $table->string('create_by')->nullable();;
            $table->string('is_public');
            $table->timestamps();
        });
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('CategoryNews_id')->unsigned();
            $table->foreign('CategoryNews_id')->references('id')->on('categorynews');
            $table->string('title');
            $table->text('detail');
            $table->string('headlines');
            $table->string('correspondent')->nullable();
            $table->string('composer')->nullable();
            $table->string('source')->nullable();
            $table->date('startdate');
            $table->date('enddate');
            $table->string('view')->nullable();
            $table->string('create_by')->nullable();
            $table->string('is_public');
            $table->timestamps();
        });

        Schema::create('filenewsdetail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('news_id')->unsigned();
            $table->foreign('news_id')->references('id')->on('news');
            $table->text('filename');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categorynews');
        Schema::drop('news');
        Schema::drop('filenewsdetail');
    }
}
