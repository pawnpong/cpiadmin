<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryNewsTable extends Model
{
    protected $table = 'CategoryNews';

    public function news(){
        return $this->hasMany(News::class,'CategoryNews_id');
    }
}