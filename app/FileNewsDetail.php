<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileNewsDetail extends Model
{
    //
    protected $table = 'FileNewsDetail';
    public function news(){
        return $this->belongsTo(News::class, 'news_id');
    }
}
