<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'News';

    public function categorynews(){
        return $this->belongsTo(CategoryNewsTable::class, 'CategoryNews_id');
    }  
    public function filenewsdetail(){
        return $this->hasMany(FileNewsDetail::class);
    }
}
