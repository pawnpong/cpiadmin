<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\CategoryNewsTable;
use Image;
use File;
use Illuminate\Support\Facades\Auth; //เอาออเทนมาใช้

class CategoryNewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorynews = CategoryNewsTable::where('is_public',1)->get(); 
        return view('categorynews.index',compact('categorynews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categorynews.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ],[
            'name.required'=> 'กรุณากรอกชื่อประเภทข่าวประชาสัมพันธ์'
        ]);

         $categorynews = new CategoryNewsTable(); 
         $categorynews->name = $request->name;
         $categorynews->create_by = Auth::user()->name;
         $categorynews->is_public = 1;
         $categorynews->save();
         return redirect()->route('admin.categorynews.index')->with('feedback','บันทึกข้อมูลเรียบร้อยแล้ว');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $categorynews = CategoryNewsTable::findOrFail($id); 
      return view('categorynews.edit',compact('categorynews'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'name' => 'required',
        ],[
            'name.required'=> 'กรุณากรอกชื่อประเภทข่าวประชาสัมพันธ์',
        ]);
        $categorynews = CategoryNewsTable::find($id); 
        $categorynews->name = $request->name;
        $categorynews->save();
        return redirect()->route('admin.categorynews.index')->with('feedback','แก้ไขข้อมูลเรียบร้อยแล้ว');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categorynews = CategoryNewsTable::find($id);
        $categorynews->is_public = 0;
        $categorynews->save();

        if($categorynews){
            return response()->json(['success' => '1']);                    
        }else{
            return response()->json(['success' => '0']);                    
        }
    }
}
