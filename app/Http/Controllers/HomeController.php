<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\News;
use App\Video;
use App\InfographicTable;
use App\CategoryTable;
use App\CategoryInfographicTable;
use App\CategoryNewsTable;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $news_count = News::where('is_public' , 1)->count();
        $youtube_count = Video::where('is_public' , 1)->count();
        $infographic_count = InfographicTable::where('is_public' , 1)->count();
        $category_count = CategoryTable::where('is_public' , 1)->count();
        $categorynews_count = CategoryNewsTable::where('is_public' , 1)->count();
        $categoryinfographic_count = CategoryInfographicTable::where('is_public' , 1)->count();
        return view('home' ,compact('news_count','youtube_count','infographic_count','category_count','categoryinfographic_count','categorynews_count'));
    }
}
