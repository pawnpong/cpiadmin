<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\CategoryTable;
use Image;
use App\Video;
use File;
use Illuminate\Support\Facades\Auth; //เอาออเทนมาใช้

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = CategoryTable::where('is_public',1)->get(); 
        return view('category.index',compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
           
        ],[
            'name.required'=> 'กรุณากรอกชื่อ',
           
        ]);

         $category = new CategoryTable(); 
         $category->name = $request->name;
         $category->create_by = Auth::user()->name;
         $category->is_public = 1;
        //  if ($request->hasFile('image')) { 
        //     $filename = str_random(10).'.'.$request->file('image')->getClientOriginalExtension();
        //     $request->file('image')->move(base_path() . '/public/categoryimages/', $filename);
        //     $category->image = $filename;    
        // } 
         $category->save();
         return redirect()->route('admin.category.index')->with('feedback','บันทึกข้อมูลเรียบร้อยแล้ว');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $category = CategoryTable::findOrFail($id); 
      return view('category.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'name' => 'required',
        ],[
            'name.required'=> 'กรุณากรอกชื่อ',
        ]);
        $category = CategoryTable::find($id); 
        $category->name = $request->name;
     
        // if ($request->hasFile('image')){
        //     File::delete(base_path().'\\public\\categoryimages\\'.$category->image);
        //     $filename = str_random(10).'.'.$request->file('image')->getClientOriginalExtension();
        //     $request->file('image')->move(base_path().'/public/categoryimages/',$filename);
        //     $category->image = $filename;
        // }
        // elseif($category->image !=null){
        //     $category->image = $category->image;
        // }
        // else{
        //     $category->image = 'nopic.jpg';
        // }
        $category->save();
        return redirect()->route('admin.category.index')->with('feedback','แก้ไขข้อมูลเรียบร้อยแล้ว');
      
        
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = CategoryTable::find($id);
        $category->is_public = 0;
        $category->save();

        if($category){
            return response()->json(['success' => '1']);                    
        }else{
            return response()->json(['success' => '0']);                    
        }
    }
}
