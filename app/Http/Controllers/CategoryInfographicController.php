<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\CategoryInfographicTable;
use Image;
use File;
use Illuminate\Support\Facades\Auth; //เอาออเทนมาใช้

class CategoryInfographicController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoryinfographics = CategoryInfographicTable::where('is_public',1)->get(); 
        return view('categoryinfographics.index',compact('categoryinfographics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categoryinfographics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ],[
            'name.required'=> 'กรุณากรอกชื่อ'
        ]);

         $categoryinfographics = new CategoryInfographicTable(); 
         $categoryinfographics->name = $request->name;
         $categoryinfographics->create_by = Auth::user()->name;
         $categoryinfographics->is_public = 1;
         $categoryinfographics->save();
         return redirect()->route('admin.categoryinfographics.index')->with('feedback','บันทึกข้อมูลเรียบร้อยแล้ว');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $categoryinfographics = CategoryInfographicTable::findOrFail($id); 
      return view('categoryinfographics.edit',compact('categoryinfographics'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'name' => 'required',
        ],[
            'name.required'=> 'กรุณากรอกชื่อ',
        ]);
        $categoryinfographics = CategoryInfographicTable::find($id); 
        $categoryinfographics->name = $request->name;
        $categoryinfographics->save();
        return redirect()->route('admin.categoryinfographics.index')->with('feedback','แก้ไขข้อมูลเรียบร้อยแล้ว');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categoryinfographic = CategoryInfographicTable::find($id);
        $categoryinfographic->is_public = 0;
        $categoryinfographic->save();

        if($categoryinfographic){
            return response()->json(['success' => '1']);                    
        }else{
            return response()->json(['success' => '0']);                    
        }
    }
}
