<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\News;
use App\FileNewsDetail;
use Image;
use File;
use Illuminate\Support\Facades\Auth; //เอาออเทนมาใช้

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $newstopic = News::where('is_public',1)->get(); 
        return view('newstopic.index',compact('newstopic'));
     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('newstopic.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $this->validate($request, [
            'CategoryNews_id'=>'required',
            'title' => 'required',
            'detail' => 'required',
            'headlines' => 'required|mimes:jpg,jpeg,png,gif',
            'startdate'=>'required'
        ],[
            'CategoryNews_id.required'=> 'กรุณาเลือกประเภทข่าวประชาสัมพันธ์',
            'title.required'=> 'กรุณากรอกหัวข้อข่าว',
            'detail.required'=> 'กรุณากรอกเนื้อหาข่าว',
            'headlines.required' => 'กรุณาใส่รูปพาดหัวข่าว' ,
            'headlines.mimes' => 'นามสกุลไฟล์รูปภาพพาดหัวข่าวไม่ถูกต้อง',
            'startdate.required' => 'กรุณาเลือกวันที่เริ่ม'
        ]);
            $newstopic = new News(); 
            $newstopic->CategoryNews_id = $request->CategoryNews_id;
            $newstopic->title = $request->title;
            $newstopic->detail = $request->detail;
            $newstopic->correspondent = $request->correspondent;
            $newstopic->composer = $request->composer;
            $newstopic->source= $request->source;
            $newstopic->startdate = $request->startdate;
            $newstopic->view = 0;
            $newstopic->create_by = Auth::user()->name;;
            $newstopic->is_public = 1;
            $newstopic->startdate = $request->startdate;
            $newstopic->enddate = $request->enddate;
            
            if ($request->hasFile('headlines')) { 
                $headlines =str_random(10).'.'.$request->file('headlines')->getClientOriginalExtension();
                $request->file('headlines')->move(base_path() . '/public/news/', $headlines);
                $newstopic->headlines = $headlines;    
            } 

            $newstopic->save();

            if($request->hasFile('filenewsdetail')){
                foreach($request->file('filenewsdetail') as $files)
                {
                    $filename = str_random(10).'.'.$files->getClientOriginalExtension();
                    $files->move(public_path().'/newsdetail', $filename);
                    $FileNewsDetail = new FileNewsDetail();
                    $FileNewsDetail->news_id = $newstopic->id;
                    $FileNewsDetail->filename = $filename;
                    $FileNewsDetail->save();
                }
            }
         return redirect()->route('admin.newstopic.index')->with('feedback','บันทึกข้อมูลเรียบร้อยแล้ว');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news_obj = News::findOrFail($id); 
        return view('newstopic.edit',compact('news_obj'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'CategoryNews_id'=>'required',
            'title' => 'required',
            'detail' => 'required',
            'startdate'=>'required'
          
        ],[
            'CategoryNews_id.required'=> 'กรุณาเลือกประเภทข่าวประชาสัมพันธ์',
            'title.required'=> 'กรุณากรอกหัวข้อข่าว',
            'detail.required'=> 'กรุณากรอกเนื้อหาข่าว',
            'headlines.mimes' => 'นามสกุลไฟล์รูปภาพพาดหัวข่าวไม่ถูกต้อง',
            'startdate.required' => 'กรุณาเลือกวันที่เริ่ม',
        ]);
            $newstopic = News::find($id);
            $newstopic->CategoryNews_id = $request->CategoryNews_id;
            $newstopic->title = $request->title;
            $newstopic->detail = $request->detail;
            $newstopic->correspondent = $request->correspondent;
            $newstopic->composer = $request->composer;
            $newstopic->source= $request->source;
            $newstopic->startdate = $request->startdate;
            $newstopic->enddate = $request->enddate;

            if ($request->hasFile('headlines')){
                File::delete(base_path().'\\public\\news\\'.$newstopic->headlines);
                $headlines = str_random(10).'.'.$request->file('headlines')->getClientOriginalExtension();
                $request->file('headlines')->move(base_path().'/public/news/',$headlines);
                $newstopic->headlines = $headlines;
            }
            elseif($newstopic->headlines !=null){
                $newstopic->headlines = $newstopic->headlines;
            }
            else{
                $newstopic->headlines = 'nopic.jpg';
            }

            if($request->hasFile('filenewsdetail')){
                foreach($request->file('filenewsdetail') as $files)
                {
                    $filename = str_random(10).'.'.$files->getClientOriginalExtension();
                    $files->move(public_path().'/newsdetail', $filename);
                    $FileNewsDetail = new FileNewsDetail();
                    $FileNewsDetail->news_id = $newstopic->id;
                    $FileNewsDetail->filename = $filename;
                    $FileNewsDetail->save();     
                }
            }
            $newstopic->save();
            return redirect()->route('admin.newstopic.index')->with('feedback','แก้ไขข้อมูลเรียบร้อยแล้ว');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $newstopic = News::find($id);
        $newstopic->is_public = 0;
        $newstopic->save();

        if($newstopic){
            return response()->json(['success' => '1']);                    
        }else{
            return response()->json(['success' => '0']);                    
        }
    }

    public function ajaxdeletenewsdetail($id)
    {
       $FileNewsDetail = FileNewsDetail::findOrFail($id);
       File::delete(base_path().'\\public\\newsdetail\\'.$FileNewsDetail->filename);
       $FileNewsDetail->delete();

       if($FileNewsDetail){
            return response()->json(['success' => '1']);                    
       }else{
            return response()->json(['success' => '0']);                    
       }


    }
}
