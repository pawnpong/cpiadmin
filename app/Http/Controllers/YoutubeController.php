<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Video;
use App\CategoryTable;
use Image;
use File;
use Illuminate\Support\Facades\Auth; //เอาออเทนมาใช้

class YoutubeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $youtube = Video::with('category')->where('is_public',1)->get(); 
        return view('youtube.index',compact('youtube'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('youtube.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'Category_id' => 'required',
            'title' => 'required',
            'link' => 'required',
            'description' => 'required'
        ],[
            'Category_id.required' => 'กรุณาเลือกประเภท',
            'title.required'=> 'กรุณากรอกชื่อ',
            'link.required' => 'กรุณากรอก Link',
            'description.required'=> 'กรุณากรอกรายละเอียด',
        ]);
        $youtube = new Video(); 
        $youtube->Category_id = $request->Category_id;
        $youtube->title = $request->title;
        $youtube->link = $request->link;
        $youtube->description = $request->description;
        $youtube->view = 0;
        $youtube->is_public = 1;
        $youtube->create_by = Auth::user()->name;
        $youtube->startdate = $request->startdate;
        $youtube->enddate = $request->enddate;
        $youtube->save();
        return redirect()->route('admin.youtube.index')->with('feedback','บันทึกข้อมูลเรียบร้อยแล้ว');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $youtube_obj = Video::findOrFail($id); 
        return view('youtube.edit',compact('youtube_obj'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'Category_id' => 'required',
            'title' => 'required',
            'link' => 'required',
            'description' => 'required',
            'startdate' => 'required',
            
        ],[
            'Category_id.required' => 'กรุณาเลือกประเภท',
            'title.required'=> 'กรุณากรอกชื่อ',
            'link.required' => 'กรุณากรอก Link',
            'description.required'=> 'กรุณากรอกรายละเอียด',
            'startdate.required' => 'กรุณาใส่วันเริ่ม',
            
           
        ]);
        $youtube = Video::find($id);
        $youtube->Category_id = $request->Category_id;
        $youtube->title = $request->title;
        $youtube->link = $request->link;
        $youtube->description = $request->description;
        $youtube->startdate = $request->startdate;
        $youtube->enddate = $request->enddate;
         
         $youtube->save();
         return redirect()->route('admin.youtube.index')->with('feedback','แก้ไขข้อมูลเรียบร้อยแล้ว');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $youtube = Video::find($id);
        $youtube->is_public = 0;
        $youtube->save();

        if($youtube){
            return response()->json(['success' => '1']);                    
        }else{
            return response()->json(['success' => '0']);                    
        }
    }
}
