<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\InfographicTable;
use Image;
use File;
use Illuminate\Support\Facades\Auth; //เอาออเทนมาใช้

class InfographicController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Infographic = InfographicTable::where('is_public',1)->get(); 
        return view('infographics.index',compact('Infographic'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        return view('infographics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'CategoryInfographic_id'=> 'required',
            'name' => 'required',
            'department' => 'required',
            'file' => 'required|mimes:jpg,jpeg,png,gif',
        ],[
            'CategoryInfographic_id.required' => 'กรุณาเลือกประเภท',
            'name.required'=> 'กรุณากรอกชื่อโครงการ',
            'department.required'=> 'กรุณากรอกหน่วยงาน',
            'file.required' => 'กรุณาใส่รูป' ,
            'file.mimes' => 'นามสกุลไม่ถูกต้อง',
        ]);
         $Infographic = new InfographicTable();
         $Infographic->CategoryInfographic_id = $request->CategoryInfographic_id;
         $Infographic->name = $request->name;
         $Infographic->department = $request->department;
         $Infographic->create_by = Auth::user()->name;
         $Infographic->is_public = 1;
         $Infographic->startdate = $request->startdate;
         $Infographic->enddate = $request->enddate;
         if ($request->hasFile('file')) { 
            $filename = str_random(10).'.'.$request->file('file')->getClientOriginalExtension();
            $request->file('file')->move(base_path() . '/public/fileinfographic/', $filename);
            $Infographic->filename = $filename;    
        } 
         $Infographic->save();
         return redirect()->route('admin.infographics.index')->with('feedback','บันทึกข้อมูลเรียบร้อยแล้ว');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $Infographic_obj = InfographicTable::findOrFail($id); 
      return view('infographics.edit',compact('Infographic_obj'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'CategoryInfographic_id'=> 'required',
            'name' => 'required',
            'department' => 'required',
         
        ],[
            'CategoryInfographic_id.required' => 'กรุณาเลือกประเภท',
            'name.required'=> 'กรุณากรอกชื่อโครงการ',
            'department.required'=> 'กรุณากรอกหน่วยงาน',
       
            
        ]);
            
       
        $Infographic = InfographicTable::find($id);
        $Infographic->CategoryInfographic_id = $request->CategoryInfographic_id;
        $Infographic->name = $request->name;
        $Infographic->department = $request->department;
        $Infographic->enddate = $request->enddate;
        if ($request->hasFile('file')){
            File::delete(base_path().'\\public\\fileinfographic\\'.$Infographic->filename);
            $filename = str_random(10).'.'.$request->file('file')->getClientOriginalExtension();
            $request->file('file')->move(base_path().'/public/fileinfographic/',$filename);
            $Infographic->filename = $filename;
        }
        elseif($Infographic->filename !=null){
            $Infographic->filename = $Infographic->filename;
        }
        else{
            $Infographic->filename = 'nopic.jpg';
        }

        $Infographic->save();
        return redirect()->route('admin.infographics.index')->with('feedback','แก้ไขข้อมูลเรียบร้อยแล้ว');
      
        
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Infographic = InfographicTable::find($id);
        $Infographic->is_public = 0;
        $Infographic->save();

        if($Infographic){
            return response()->json(['success' => '1']);                    
        }else{
            return response()->json(['success' => '0']);                    
        }
    }
}
