<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\InfographicTable;
use App\Video;
use Youtube;
use App\CategoryTable;
use App\News;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\CategoryNewsTable;
use App\CategoryInfographicTable;
use Feeds;
use stdClass;

class ChannelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function index()
    {
        $Infographic = InfographicTable::where('is_public', 1)->get(); 
        $youtube = Video::where('is_public', 1)->orderby('id' , 'DESC')->get();
        $news = News::where('is_public', 1)->orderBy('id','DESC')->limit(4)->get();      
        $viewnews = News::where('is_public' ,1)->orderby('view','desc')->limit(4)->get();

        return view('index',compact('Infographic','youtube','news','viewnews'));
    }
    public function Infographic($id)
    {
        $Infographic = InfographicTable::where('is_public', 1)->paginate(9);
        $category_select = CategoryInfographicTable::where('is_public',1)->get();
       
        $id_category = $id;
        if($id == 0){
            $category = InfographicTable::where('is_public', 1)->orderby('id' ,'desc')->get();
            $category_type = 'ทั้งหมด';
         }else{
           $category_type = CategoryInfographicTable::where('id','=',$id)->where('is_public',1)->first();
            $category = InfographicTable::with('categoryinfographic')->where('CategoryInfographic_id','=',$id)->where('is_public',1)->orderby('id','desc')->get();
         }

        return view('viewinfographic',compact('Infographic','category_select','id_category','category','category_type'));
    }

    public function PlayYoutube($id)
    {
        $playyoutube = Video::where('id', $id)->first();
        $youtube = Video::where('is_public', 1)->orderby('id','desc')->paginate(6);

        if($youtube->count() > 1){
            $mostvideos = Video::where('is_public', 1)->orderBy('view', 'DESC')->limit(2)->get();
        }else{
            $mostvideos = Video::where('is_public', 1)->orderBy('view', 'DESC')->limit(1)->get();
        }
        $category = CategoryTable::where('is_public',1)->get();

        return view('playyoutube',compact('playyoutube', 'mostvideos','youtube','category'));
        
    }
    public function Detailnews($id)
    {
        $category = CategoryTable::where('is_public',1)->get();
        $news = News::where('id', $id)->first();
        $viewnews = News::where('is_public' ,1)->orderby('view','desc')->get();
        $viewnewsall = News::where('is_public' ,1)->orderby('id','desc')->paginate(4);

        return view('viewdetailnews',compact('news','category','viewnews','viewnewsall'));
    }

    public function News($id)
    {
      
        $news = News::where('is_public', 1)->orderBy('id','DESC')->paginate(10);
        $viewnews = News::where('is_public', 1)->orderby('view','DESC')->get();

        $category_select = CategoryNewsTable::where('is_public',1)->get();
        $id_category = $id;
        if($id == 0){
            $category = News::where('is_public', 1)->orderby('id' ,'desc')->get();
            $category_type = 'ทั้งหมด';
        }else{
            $category_type = CategoryNewsTable::where('id','=',$id)->where('is_public',1)->first();
            $category = News::with('categorynews')->where('CategoryNews_id','=',$id)->where('is_public',1)->orderby('id','desc')->get();
        }
            return view('viewnews',compact('news','category','viewnews','category_select','id_category','category_type'));
        
    }

    public function Nacc()
    {
        
        $playlists = Youtube::getPlaylistsByChannelId('UCpsNIurwNH3smvTB6fL7JxA');
        $videoList = Youtube::listChannelVideos('UCpsNIurwNH3smvTB6fL7JxA', 50);
        
        return view('nacc',[ 
            'playlists' => $playlists ,
            'videoList' => $videoList
        ]);
    }

    public function Pacc()
    {
        $videoList = Youtube::listChannelVideos('UCpsNIurwNH3smvTB6fL7JxA', 50);
        
        return view('pacc',[ 
            'videoList' => $videoList
        ]);
      
    }

    public function NewsFeed()
    {
        
        $feed = Feeds::make('http://122.155.92.3/website_th/rss/rss_feed/ZeroTolerance');
        $data = new stdClass();
        $data->description = $feed->get_description();
        $data->permalink = $feed->get_permalink();
        $data->items= $feed->get_items();
        //return dd('data');
        return view('feeds', [  
            'data' => $data,
        ]);
    }   

    public function Test()
    {
        return view('test');
    }   

    public function Category($id)
    {
      
       $youtube = Video::where('is_public', 1)->orderby('id','DESC')->get();
   
       $mostvideos = Video::orderBy('view', 'DESC')->limit(2)->get();
       $category_select = CategoryTable::where('is_public',1)->get();
       
        $id_category = $id;
        if($id == 0){
            $category = Video::where('is_public', 1)->orderby('id' ,'desc')->paginate(6);
            $category_type = 'ทั้งหมด';
        }else{
            $category_type = CategoryTable::where('id','=',$id)->where('is_public',1)->first();
            $category = Video::with('category')->where('category_id','=',$id)->where('is_public',1)->orderby('id','desc')->paginate(6);
        }


        return view('category',compact('category','category_type','id_category','youtube','mostvideos','category_select'));
    }

    public function ajaxupdateview(Request $request){
        $oldview = Video::select('view')->where('id', $request->id)->first()->view; 

        if(isset($oldview)){ // have oldview 
            $updateview = Video::find($request->id); 
            $updateview->view = $oldview + 1;
            $updateview->save();
        }else{ // don't have oldview set defualt = 1
            $updateview = Video::find($request->id); 
            $updateview->view = 1;
            $updateview->save();
        }

        return response()->json(['id' => $request->id,'url'=> route('PlayYoutube',['id' => $request->id])]);
    }

    public function ajaxupdateviewnews(Request $request){
        $oldview = News::select('view')->where('id', $request->id)->first()->view; 

        if(isset($oldview)){ // have oldview 
            $updateview = News::find($request->id); 
            $updateview->view = $oldview + 1;
            $updateview->save();
        }else{ // don't have oldview set defualt = 1
            $updateview = News::find($request->id); 
            $updateview->view = 1;
            $updateview->save();
        }

        return response()->json(['id' => $request->id,'url'=> route('Detailnews',['id' => $request->id])]);
    }

    public function ajaxupdateinfographic(Request $request){
        $oldview = InfographicTable::select('view')->where('id', $request->id)->first()->view; 

        if(isset($oldview)){ // have oldview 
            $updateview = InfographicTable::find($request->id); 
            $updateview->view = $oldview + 1;
            $updateview->save();
        }else{ // don't have oldview set defualt = 1
            $updateview = InfographicTable::find($request->id); 
            $updateview->view = 1;
            $updateview->save();
        }

        return response()->json(['id' => $request->id,]);
    } 

    public function Contact(){
        return view('contactus');
    }
}
