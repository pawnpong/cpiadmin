<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'ChannelController@index');
Route::get('/index', 'ChannelController@index');
Route::auth();

Route::get('/admin', 'HomeController@index')->name('adminindex');

Route::get('/Contact', 'ChannelController@Contact');
Route::get('/Livestream', 'ChannelController@Livestream');
Route::get('/History', 'ChannelController@History');
Route::get('/feeds' , 'ChannelController@NewsFeed');
Route::get('/nacc' , 'ChannelController@Nacc');
Route::get('/pacc' , 'ChannelController@Pacc');
Route::get('/test' , 'ChannelController@Test');

Route::resource('/admin/infographics','InfoGraphicController');
Route::resource('/admin/youtube','YoutubeController');
//Route::resource('/library','LibraryController');
Route::resource('/admin/newstopic','NewsController');
Route::resource('/admin/category','CategoryController');
Route::resource('/admin/categorynews','CategoryNewsController');
Route::resource('/admin/categoryinfographics','CategoryInfographicController');
//Route::resource('/playyoutube','PlayYoutubeController');
// manage user 
Route::resource('/admin/users','UsersController');

Route::delete('/ajaxdeletenewsdetail/{id}', 'NewsController@ajaxdeletenewsdetail')->name('ajaxdeletenewsdetail');
Route::post('/ajaxupdateview', 'ChannelController@ajaxupdateview')->name('ajaxupdateview'); 
Route::post('/ajaxupdateviewinfographic', 'ChannelController@ajaxupdateinfographic')->name('ajaxupdateinfographic'); 
Route::post('/ajaxupdateviewnews', 'ChannelController@ajaxupdateviewnews')->name('ajaxupdateviewnews');//Ajax Route to function ajaxupdateview
Route::get('PlayYoutube/{id}', [
    'as' => 'PlayYoutube',
    'uses' => 'ChannelController@PlayYoutube'
]);
Route::resource('playyoutube', 'ChannelController', ['except' => 'PlayYoutube']);

Route::get('Category/{id}', [
    'as' => 'Category',
    'uses' => 'ChannelController@Category'
]);
Route::resource('Category', 'ChannelController', ['except' => 'Category']);

Route::get('Infographic/{id}', [
    'as' => 'Infographic',
    'uses' => 'ChannelController@Infographic'
]);
Route::resource('viewinfographicc', 'ChannelController', ['except' => 'Infographic']);

Route::get('News/{id}', [
    'as' => 'News',
    'uses' => 'ChannelController@News'
]);
Route::resource('viewnews', 'ChannelController', ['except' => 'News']);

Route::get('Detailnews/{id}', [
    'as' => 'Detailnews',
    'uses' => 'ChannelController@Detailnews'
]);
Route::resource('viewdetailnew', 'ChannelController', ['except' => 'Detailnews']);









