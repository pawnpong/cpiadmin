<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryInfographicTable extends Model
{
    protected $table = 'CategoryInfographic';

    public function infographic(){
        return $this->hasMany(InfographicTable::class,'CategoryInfographic_id');
    }
}