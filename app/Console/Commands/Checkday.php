<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\News; 
use App\Video;
use App\InfographicTable;
class Checkday extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:day';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
            // $endpromise = tbpromises::where('status','เข้าทำงาน')->get();
            $CheckInfographic = InfographicTable::where('is_public' , 1)->get();
            $CheckNews = News::where('is_public' , 1)->get();
            $CheckYoutube = Video::where('is_public' , 1)->get();
            foreach($CheckInfographic as $item){
                $activityendDate = strtotime($item->enddate); //2018-12-28
                $today = strtotime(date("Y-m-d"));
        
                if($today > $activityendDate AND $item->enddate != 0000-00-00) {
                    $Infographic = InfographicTable::findOrFail($item->id);
                    $Infographic->is_public = 0;
                    $Infographic->save();

                }
            }
            foreach($CheckNews as $item){
                $activityendDate = strtotime($item->enddate); //2018-12-28
                $today = strtotime(date("Y-m-d"));
        
                if($today > $activityendDate AND $item->enddate != 0000-00-00) {
                    $News = News::findOrFail($item->id);
                    $News->is_public = 0;
                    $News->save();

                }
            }

            foreach($CheckYoutube as $item){
                $activityendDate = strtotime($item->enddate); //2018-12-28
                $today = strtotime(date("Y-m-d"));
        
                if($today > $activityendDate AND $item->enddate != 0000-00-00) {
                    $youtube = Video::findOrFail($item->id);
                    $youtube->is_public = 0;
                    $youtube->save();

                }
            }
}
}
