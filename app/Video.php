<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = 'youtubes';

    public function category(){
        return $this->belongsTo(CategoryTable::class, 'Category_id');
    }  
}
