<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfographicTable extends Model
{
    //
    protected $table = 'Infographic';
    
    public function categoryinfographic(){
        return $this->belongsTo(CategoryInfographicTable::class, 'CategoryInfographic_id');
    }  
}
