<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table = 'userrole';

    public function users(){
        return $this->hasMany(User::class);
    }
}
