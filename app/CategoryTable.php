<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryTable extends Model
{
    protected $table = 'Category';

    public function youtubes(){
        return $this->hasMany(Video::class,'Category_id');
    }
}